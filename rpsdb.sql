-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jun 02, 2019 at 02:05 PM
-- Server version: 5.7.19
-- PHP Version: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rpsdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `application`
--

DROP TABLE IF EXISTS `application`;
CREATE TABLE IF NOT EXISTS `application` (
  `apid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `staffRole` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `surname` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `forenames` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `preferredName` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dob` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` enum('Male','Female') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `religionDenomination` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nationality` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `homeAddress` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `homePhone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `firstLanguage` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `proposedYear` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `accommodation` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `connection` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `currentSchool` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attendanceDate` timestamp NULL DEFAULT NULL,
  `headTeacher` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `headTeacherEmail` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `schoolPhone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `father` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fatherTitle` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fatherProfession` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fatherIndustry` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fatherEmployerName` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fatherPhone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fatherEmail` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mother` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `motherTitle` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `motherProfession` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `motherIndustry` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `motherEmployerName` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `motherPhone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `motherEmail` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentsName` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentsAddress` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hearOfHendon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `advertisement` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `others` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fname` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sname` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `relationshipToChild` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `medicalConditions` varchar(5000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `section` enum('1','2','3','4') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('admitted','pending') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`apid`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `application`
--

INSERT INTO `application` (`apid`, `uid`, `staffRole`, `surname`, `forenames`, `preferredName`, `dob`, `gender`, `religionDenomination`, `nationality`, `homeAddress`, `homePhone`, `firstLanguage`, `proposedYear`, `accommodation`, `connection`, `currentSchool`, `attendanceDate`, `headTeacher`, `headTeacherEmail`, `schoolPhone`, `father`, `fatherTitle`, `fatherProfession`, `fatherIndustry`, `fatherEmployerName`, `fatherPhone`, `fatherEmail`, `mother`, `motherTitle`, `motherProfession`, `motherIndustry`, `motherEmployerName`, `motherPhone`, `motherEmail`, `parentsName`, `parentsAddress`, `hearOfHendon`, `advertisement`, `others`, `title`, `fname`, `sname`, `address`, `email`, `phone`, `relationshipToChild`, `medicalConditions`, `section`, `status`, `created_at`, `updated_at`) VALUES
(1, NULL, NULL, 'rimamchirika', 'jonah', 'jonah', '1990-01-01', 'Male', 'Christianity', 'Nigeria', 'no.31 samora machel street asokoro abuja, fct', '08167989160', 'ENGLISH', '2007', NULL, NULL, NULL, NULL, 'jonah rimamchirika', 'dankifasi@gmail.com', '07033333619', 'danladi kifasi', 'Mr.', 'ENGINEER', 'ICT', 'MICROSOFT', '8065544992', 'dankifasi@gmail.com', 'Naomi', NULL, 'TRADER', 'PROVISIONS', 'MELCOM', '08171624968', 'amostimothy7@gmail.com', NULL, NULL, 'Others', NULL, NULL, NULL, 'musa', 'Jacson', 'no.31 samora machel street asokoro abuja, fct', 'micahande@gmail.com', NULL, 'UNCLE', NULL, NULL, 'admitted', '2019-03-20 19:35:04', '2019-03-20 19:35:29'),
(2, NULL, NULL, 'Michael', 'Bashar', 'Tortiv', '02-04-1992', 'Male', 'Christianity', 'Nigeria', 'Behind Regent University', '08023456954', 'Tiv', '2018', NULL, NULL, 'Regent Secondary School', NULL, 'Mr Chris Quist', 'samuelquist@gmail.com', '09087930940', 'Bashaa Michael', 'Mr.', 'Politician', 'Politics', 'Nigerian Federation', '08028193945', 'bashaamichael@gmail.com', 'Merry Bashaa', NULL, 'Lecturer', 'Education', 'Harvard University', '080331556654', 'marryb@gmail.com', NULL, NULL, 'Advertisement', NULL, NULL, NULL, 'Jude', 'Jackson', 'Behind Government House Mkd', 'judej@gmail.com', NULL, 'Uncle', NULL, NULL, 'pending', '2019-03-20 23:49:34', '2019-03-21 00:04:09');

-- --------------------------------------------------------

--
-- Table structure for table `assignments`
--

DROP TABLE IF EXISTS `assignments`;
CREATE TABLE IF NOT EXISTS `assignments` (
  `aid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `stid` int(11) NOT NULL,
  `subid` int(11) NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `due` timestamp NOT NULL,
  `cid` int(11) NOT NULL,
  `total` decimal(8,2) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`aid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `assignmentsubmissions`
--

DROP TABLE IF EXISTS `assignmentsubmissions`;
CREATE TABLE IF NOT EXISTS `assignmentsubmissions` (
  `asid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `aid` int(11) NOT NULL,
  `sid` int(11) NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `grade` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`asid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `attempts`
--

DROP TABLE IF EXISTS `attempts`;
CREATE TABLE IF NOT EXISTS `attempts` (
  `atid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `sid` int(11) NOT NULL,
  `testid` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`atid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `billclubs`
--

DROP TABLE IF EXISTS `billclubs`;
CREATE TABLE IF NOT EXISTS `billclubs` (
  `blcid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `bid` int(11) NOT NULL,
  `clid` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`blcid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `billdetails`
--

DROP TABLE IF EXISTS `billdetails`;
CREATE TABLE IF NOT EXISTS `billdetails` (
  `bdid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `bid` int(11) NOT NULL,
  `item` varchar(5000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `unitprice` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantity` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `total` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`bdid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `billinventories`
--

DROP TABLE IF EXISTS `billinventories`;
CREATE TABLE IF NOT EXISTS `billinventories` (
  `biid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `bid` int(11) NOT NULL,
  `inid` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`biid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `billitemcollections`
--

DROP TABLE IF EXISTS `billitemcollections`;
CREATE TABLE IF NOT EXISTS `billitemcollections` (
  `bicid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `bid` int(11) NOT NULL,
  `inid` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`bicid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `bills`
--

DROP TABLE IF EXISTS `bills`;
CREATE TABLE IF NOT EXISTS `bills` (
  `bid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `due` timestamp NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` decimal(8,2) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`bid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `billstudents`
--

DROP TABLE IF EXISTS `billstudents`;
CREATE TABLE IF NOT EXISTS `billstudents` (
  `bsid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `sid` int(11) NOT NULL,
  `status` enum('paid','unpaid','partiallypaid') COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`bsid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
CREATE TABLE IF NOT EXISTS `categories` (
  `catid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `stid` int(11) NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`catid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `classes`
--

DROP TABLE IF EXISTS `classes`;
CREATE TABLE IF NOT EXISTS `classes` (
  `cid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `stid` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`cid`),
  UNIQUE KEY `classes_name_unique` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
CREATE TABLE IF NOT EXISTS `comments` (
  `cmid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `stid` int(11) NOT NULL,
  `sid` int(11) NOT NULL,
  `pid` int(11) DEFAULT NULL,
  `fromparent` tinyint(1) DEFAULT NULL,
  `isstudentpermitted` tinyint(1) DEFAULT NULL,
  `comment` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`cmid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `complaints`
--

DROP TABLE IF EXISTS `complaints`;
CREATE TABLE IF NOT EXISTS `complaints` (
  `comid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `complaint` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pid` int(11) NOT NULL,
  `stid` int(11) DEFAULT NULL,
  `sid` int(11) DEFAULT NULL,
  `subject` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` varchar(5000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('pending','resolved') COLLATE utf8mb4_unicode_ci NOT NULL,
  `isPrincipalViewed` tinyint(1) NOT NULL,
  `isAdminViewed` tinyint(1) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`comid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `confirmations`
--

DROP TABLE IF EXISTS `confirmations`;
CREATE TABLE IF NOT EXISTS `confirmations` (
  `confid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`confid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `designations`
--

DROP TABLE IF EXISTS `designations`;
CREATE TABLE IF NOT EXISTS `designations` (
  `did` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `category` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `arm` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`did`),
  UNIQUE KEY `designations_name_unique` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `files`
--

DROP TABLE IF EXISTS `files`;
CREATE TABLE IF NOT EXISTS `files` (
  `flid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `stid` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(2000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`flid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `focus`
--

DROP TABLE IF EXISTS `focus`;
CREATE TABLE IF NOT EXISTS `focus` (
  `fid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cid` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`fid`),
  UNIQUE KEY `focus_name_unique` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `focussubjects`
--

DROP TABLE IF EXISTS `focussubjects`;
CREATE TABLE IF NOT EXISTS `focussubjects` (
  `fsid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `fid` int(11) NOT NULL,
  `subid` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`fsid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `guarantors`
--

DROP TABLE IF EXISTS `guarantors`;
CREATE TABLE IF NOT EXISTS `guarantors` (
  `gid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `stid` int(11) NOT NULL,
  `gName` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dob` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gProfession` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gGender` enum('male','female') COLLATE utf8mb4_unicode_ci NOT NULL,
  `gOfficeAddress` varchar(2000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gStateOfOrigin` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gPhone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gLocation` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`gid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `inventories`
--

DROP TABLE IF EXISTS `inventories`;
CREATE TABLE IF NOT EXISTS `inventories` (
  `inid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantity` int(11) NOT NULL,
  `costPrice` decimal(8,2) NOT NULL,
  `sellingPrice` decimal(8,2) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`inid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2018_07_03_122054_initial_db', 1);

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

DROP TABLE IF EXISTS `news`;
CREATE TABLE IF NOT EXISTS `news` (
  `newsid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `stid` int(11) NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` varchar(5000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`newsid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `news_images`
--

DROP TABLE IF EXISTS `news_images`;
CREATE TABLE IF NOT EXISTS `news_images` (
  `niid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `newsid` int(11) NOT NULL,
  `url` varchar(2000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`niid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

DROP TABLE IF EXISTS `notifications`;
CREATE TABLE IF NOT EXISTS `notifications` (
  `nid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` varchar(5000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`nid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `parents`
--

DROP TABLE IF EXISTS `parents`;
CREATE TABLE IF NOT EXISTS `parents` (
  `pid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `fname` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sname` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `relationship` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(2000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(2000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`pid`),
  UNIQUE KEY `parents_phone_unique` (`phone`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `parents`
--

INSERT INTO `parents` (`pid`, `fname`, `sname`, `phone`, `email`, `relationship`, `image`, `password`, `address`, `deleted_at`, `created_at`, `updated_at`) VALUES
(2, 'Bashaa Michael', NULL, '08028193945', 'bashaamichael@gmail.com', 'father', NULL, '$2y$10$kPEIjMP1Qtk2auD5S3oKMOfSGC1KLPJCz8Yk5j6IFN4dqz.kSXpVC', 'Behind Regent University', NULL, '2019-03-21 00:04:09', '2019-03-21 00:04:09'),
(3, 'Merry Bashaa', NULL, '080331556654', 'marryb@gmail.com', 'mother', NULL, '$2y$10$V4fR/4EeK6YfNCw38YTVyuSXa1eBWF97KX42rrpc5wfbKVGwBZK2u', 'Behind Regent University', NULL, '2019-03-21 00:04:09', '2019-03-21 00:04:09');

-- --------------------------------------------------------

--
-- Table structure for table `parentstudents`
--

DROP TABLE IF EXISTS `parentstudents`;
CREATE TABLE IF NOT EXISTS `parentstudents` (
  `psid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL,
  `sid` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`psid`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `parentstudents`
--

INSERT INTO `parentstudents` (`psid`, `pid`, `sid`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 2, NULL, NULL, '2019-03-21 00:04:09', '2019-03-21 00:04:09'),
(2, 3, NULL, NULL, '2019-03-21 00:04:09', '2019-03-21 00:04:09');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

DROP TABLE IF EXISTS `payments`;
CREATE TABLE IF NOT EXISTS `payments` (
  `payid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `bid` int(11) NOT NULL,
  `amount` decimal(8,2) NOT NULL,
  `reference` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sid` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`payid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

DROP TABLE IF EXISTS `questions`;
CREATE TABLE IF NOT EXISTS `questions` (
  `qid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `testid` int(11) NOT NULL,
  `stid` int(11) NOT NULL,
  `question` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `option1` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `option2` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `option3` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `option4` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `option5` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `correctAnswer` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`qid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `requisitiondetails`
--

DROP TABLE IF EXISTS `requisitiondetails`;
CREATE TABLE IF NOT EXISTS `requisitiondetails` (
  `rdid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `inid` int(11) NOT NULL,
  `rqid` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `price` decimal(8,2) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`rdid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `requisitions`
--

DROP TABLE IF EXISTS `requisitions`;
CREATE TABLE IF NOT EXISTS `requisitions` (
  `rqid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`rqid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `responses`
--

DROP TABLE IF EXISTS `responses`;
CREATE TABLE IF NOT EXISTS `responses` (
  `resid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `sid` int(11) NOT NULL,
  `qid` int(11) NOT NULL,
  `option` int(11) NOT NULL,
  `attempt` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`resid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `resultcomments`
--

DROP TABLE IF EXISTS `resultcomments`;
CREATE TABLE IF NOT EXISTS `resultcomments` (
  `rcid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `comment` varchar(2000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `number` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`rcid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `results`
--

DROP TABLE IF EXISTS `results`;
CREATE TABLE IF NOT EXISTS `results` (
  `rid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `sid` int(11) NOT NULL,
  `attendance` decimal(8,2) DEFAULT NULL,
  `average` decimal(8,2) DEFAULT NULL,
  `tComments` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hComments` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total` decimal(8,2) DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`rid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
CREATE TABLE IF NOT EXISTS `roles` (
  `rlid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`rlid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
CREATE TABLE IF NOT EXISTS `settings` (
  `setid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` decimal(8,2) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`setid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sickbays`
--

DROP TABLE IF EXISTS `sickbays`;
CREATE TABLE IF NOT EXISTS `sickbays` (
  `sbid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `diagnosis` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `treatment` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `stid` int(11) DEFAULT NULL,
  `sid` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`sbid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `staff`
--

DROP TABLE IF EXISTS `staff`;
CREATE TABLE IF NOT EXISTS `staff` (
  `stid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `staffid` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dob` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(2000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` enum('male','female') COLLATE utf8mb4_unicode_ci NOT NULL,
  `residentialAddress` varchar(2000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `did` int(11) NOT NULL,
  `bio` varchar(5000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `stateOfOrigin` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nationality` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`stid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `studenthealth`
--

DROP TABLE IF EXISTS `studenthealth`;
CREATE TABLE IF NOT EXISTS `studenthealth` (
  `shid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `sid` int(11) NOT NULL,
  `comments` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `stid` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`shid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

DROP TABLE IF EXISTS `students`;
CREATE TABLE IF NOT EXISTS `students` (
  `sid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `fname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dob` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(2000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(2000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nationality` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `religion` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `studentid` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `session` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `language` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `serialNumber` int(11) DEFAULT NULL,
  `roomNumber` int(11) DEFAULT NULL,
  `bedNumber` int(11) DEFAULT NULL,
  `house` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fid` int(11) DEFAULT NULL,
  `cid` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`sid`),
  UNIQUE KEY `students_email_unique` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`sid`, `fname`, `sname`, `gender`, `dob`, `image`, `address`, `email`, `phone`, `nationality`, `religion`, `studentid`, `session`, `language`, `serialNumber`, `roomNumber`, `bedNumber`, `house`, `password`, `fid`, `cid`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'jonah', 'rimamchirika', 'Male', '1990-01-01', NULL, 'no.31 samora machel street asokoro abuja, fct', 'j.rimamchirika@hendoncollege.org', '08167989160', 'Nigeria', 'Christianity', NULL, NULL, 'ENGLISH', NULL, NULL, NULL, NULL, '$2y$10$QyzxyuSh/WhJbA7WIhnL.eeutYrowO1E3yJgxEYX6DWdqcSRfH8Py', NULL, NULL, NULL, '2019-03-20 19:35:29', '2019-03-20 19:35:29');

-- --------------------------------------------------------

--
-- Table structure for table `subjectfiles`
--

DROP TABLE IF EXISTS `subjectfiles`;
CREATE TABLE IF NOT EXISTS `subjectfiles` (
  `sfid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `subid` int(11) NOT NULL,
  `stid` int(11) NOT NULL,
  `description` varchar(5000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(2000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`sfid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `subjectresults`
--

DROP TABLE IF EXISTS `subjectresults`;
CREATE TABLE IF NOT EXISTS `subjectresults` (
  `srid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `sid` int(11) NOT NULL,
  `subid` int(11) NOT NULL,
  `ce1` decimal(8,2) DEFAULT NULL,
  `ce2` decimal(8,2) DEFAULT NULL,
  `as1` decimal(8,2) DEFAULT NULL,
  `cat1` decimal(8,2) DEFAULT NULL,
  `ce3` decimal(8,2) DEFAULT NULL,
  `ce4` decimal(8,2) DEFAULT NULL,
  `as2` decimal(8,2) DEFAULT NULL,
  `cat2` decimal(8,2) DEFAULT NULL,
  `pr` decimal(8,2) DEFAULT NULL,
  `exam` decimal(8,2) DEFAULT NULL,
  `total` decimal(8,2) DEFAULT NULL,
  `comments` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`srid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `subjects`
--

DROP TABLE IF EXISTS `subjects`;
CREATE TABLE IF NOT EXISTS `subjects` (
  `subid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `stid` int(11) NOT NULL,
  `cids` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`subid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `subjectsstudents`
--

DROP TABLE IF EXISTS `subjectsstudents`;
CREATE TABLE IF NOT EXISTS `subjectsstudents` (
  `ssid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `sid` int(11) NOT NULL,
  `subid` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`ssid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `subjectteachers`
--

DROP TABLE IF EXISTS `subjectteachers`;
CREATE TABLE IF NOT EXISTS `subjectteachers` (
  `subtid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `subid` int(11) NOT NULL,
  `stid` int(11) NOT NULL,
  `cids` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`subtid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `subjecttests`
--

DROP TABLE IF EXISTS `subjecttests`;
CREATE TABLE IF NOT EXISTS `subjecttests` (
  `subtid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `srid` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `score` decimal(8,2) NOT NULL,
  `total` decimal(8,2) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`subtid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `subjectupdates`
--

DROP TABLE IF EXISTS `subjectupdates`;
CREATE TABLE IF NOT EXISTS `subjectupdates` (
  `suid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `subid` int(11) NOT NULL,
  `stid` int(11) NOT NULL,
  `update` varchar(5000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`suid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `testclasses`
--

DROP TABLE IF EXISTS `testclasses`;
CREATE TABLE IF NOT EXISTS `testclasses` (
  `tcid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `testid` int(11) NOT NULL,
  `cid` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`tcid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `testresults`
--

DROP TABLE IF EXISTS `testresults`;
CREATE TABLE IF NOT EXISTS `testresults` (
  `trid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `testid` int(11) NOT NULL,
  `sid` int(11) NOT NULL,
  `score` int(11) NOT NULL,
  `attempt` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`trid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tests`
--

DROP TABLE IF EXISTS `tests`;
CREATE TABLE IF NOT EXISTS `tests` (
  `testid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `duration` int(11) NOT NULL,
  `startTime` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `endTime` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `stid` int(11) NOT NULL,
  `subid` int(11) NOT NULL,
  `attempts` int(11) NOT NULL DEFAULT '1',
  `deadline` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`testid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `transactiondetails`
--

DROP TABLE IF EXISTS `transactiondetails`;
CREATE TABLE IF NOT EXISTS `transactiondetails` (
  `tdid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `inid` int(11) NOT NULL,
  `tid` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`tdid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

DROP TABLE IF EXISTS `transactions`;
CREATE TABLE IF NOT EXISTS `transactions` (
  `tid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `stid` int(11) DEFAULT NULL,
  `total` decimal(8,2) NOT NULL,
  `paymentStatus` enum('paid','unpaid','partiallypaid') COLLATE utf8mb4_unicode_ci NOT NULL,
  `paymentMethod` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `isCollected` tinyint(1) NOT NULL,
  `createdBy` tinyint(1) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`tid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `uid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`uid`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`uid`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Jonah Boyi', 'jonahrchirika@gmail.com', '$2y$10$mWCtnovVj5FF77IrTRnaq.9FqWC/qydLCcNUjRJvsGGmTMgGOv3vu', 'dOEmmBcRF71Rw3uS364DDhFKKnzBmzigzXXNzYVizn2yVh22HnzjswjV21WH', '2019-03-20 19:15:13', '2019-03-20 19:15:13');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
