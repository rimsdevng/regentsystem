<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Nes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::create('news', function(Blueprint $table){
		    $table->increments('newsid');
		    $table->string('title');
		    $table->string('content',5000);
		    $table->timestamps();
	    });

	    Schema::create('news_images', function(Blueprint $table){
		    $table->increments('niid');
		    $table->integer('newsid');
		    $table->string('url',2000);
		    $table->timestamps();
	    });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
