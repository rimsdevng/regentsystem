<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class New2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        
	

		Schema::create('tests', function (Blueprint $table){
			$table->increments('testid');
			$table->string('name');
			$table->string('description');
			$table->integer('duration');
			$table->string('startTime');
			$table->string('endTime');
			$table->integer('stid');
			$table->integer('subid');
			$table->integer('attempts')->default(1);
			$table->string('deadline')->nullable();
			$table->timestamps();
		});

		Schema::create('testclasses', function(Blueprint $table){
			$table->increments('tcid');
			$table->integer('testid');
			$table->integer('cid');
			$table->timestamps();
		});

		Schema::create('testresults', function(Blueprint $table){
		    $table->increments('trid');
		    $table->integer('testid');
		    $table->integer('sid');
		    $table->integer('score');
		    $table->integer('attempt');
		    $table->integer('total');
		    $table->timestamps();
		});

		Schema::create('questions', function (Blueprint $table){
			$table->increments('qid');
			$table->integer('testid');
			$table->integer('stid');
			$table->string('question');
			$table->string('option1');
			$table->string('option2');
			$table->string('option3');
			$table->string('option4');
			$table->string('option5')->nullable();
			$table->string('image',1000)->nullable();
			$table->integer('correctAnswer');
			$table->timestamps();
		});


		Schema::create('responses', function(Blueprint $table){
			$table->increments('resid');
			$table->integer('sid');
			$table->integer('qid');
			$table->integer('option'); // the number
			$table->integer('attempt');
			$table->timestamps();
		});

		Schema::create('attempts', function(Blueprint $table){
			$table->increments('atid');
			$table->integer('sid');
			$table->integer('testid');
			$table->timestamps();
		});

		Schema::create('news', function(Blueprint $table){
		    $table->increments('newsid');
		    $table->integer('stid');
		    $table->string('title');
		    $table->string('content',5000);
		    $table->timestamps();
		});

		Schema::create('news_images', function(Blueprint $table){
			$table->increments('niid');
			$table->integer('newsid');
			$table->string('url',2000);
			$table->timestamps();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        
	    Schema::dropIfExists('tests');
	    Schema::dropIfExists('testclasses');
	    Schema::dropIfExists('questions');
	    Schema::dropIfExists('responses');
	    Schema::dropIfExists('attempts');
	    Schema::dropIfExists('news');
	    Schema::dropIfExists('news_images');
    }
}
