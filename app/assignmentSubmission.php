<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class assignmentSubmission extends Model
{
    protected $primaryKey = 'asid';
    protected $table = 'assignmentsubmissions';
    protected $guarded = [ ];


	public function Assignment() {
		return $this->belongsTo(assignment::class,'aid','aid');
    }

	public function Student() {
		return $this->belongsTo(student::class,'sid','sid');
    }

}
