<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class subjectResult extends Model
{
    protected $table ='subjectresults';
    protected  $primaryKey = 'srid';

	public function Student() {
		return $this->belongsTo(student::class,'sid','sid');
    }

	public function Subject() {
		return $this->belongsTo(subjects::class,'subid','subid');
    }



}
