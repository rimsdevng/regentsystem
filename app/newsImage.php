<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class newsImage extends Model
{
    protected $primaryKey = 'niid';
    protected $table = 'news_images';

	public function News() {
		return $this->belongsTo(news::class,'newsid','newsid');
    }
}
