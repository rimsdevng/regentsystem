<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class news extends Model
{
	protected $primaryKey = 'newsid';
    protected $table = 'news';

	public function Images() {
		return $this->hasMany(newsImage::class,'newsid','newsid');
    }
}
