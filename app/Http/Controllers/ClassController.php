<?php

namespace App\Http\Controllers;

use App\classes;
use App\focus;
use App\focusSubject;
use App\staff;
use App\subjects;
use Illuminate\Http\Request;

class ClassController extends Controller
{


	public function focus(  ) {
		$subjects = subjects::all();
		return view('focus.add',[
			'subjects' => $subjects
		]);
	}

	public function postFocus(Request $request) {
		$focus = new focus();
		$focus->name = $request->input('title');
//		$focus->description = $request->input('description');
		$status = $focus->save();


		foreach ($request->subid as $id):
			$focus_subject = new focusSubject();
			$focus_subject->fid = $focus->fid;
			$focus_subject->subid = $id;
			$focus_subject->save();
		endforeach;


		if ($status){
			session()->flash('success','Faculty Added.');
		}else{
			session()->flash('error','Sorry, Something Is Wrong.');
		}
		return redirect()->back();
	}


	public function getFocus(  ) {
		$focus = focus::all();
		return view('focus.manage',[
			'focus' => $focus
		]);
	}

	public function focusDetails($fid) {

		$focus = focus::find($fid);
		$subjects = subjects::all();

		return view('focus.details',[
			'focus' => $focus,
			'subjects' => $subjects
		]);

	}

	public function removeFocusSubject( $fid, $subid ) {

		$focusSubject = focusSubject::where('fid',$fid)->where('subid',$subid)->first();
		$focusSubject->delete();

		session()->flash('success','Project Removed');

		return redirect()->back();
	}

	public function postAddFocusSubject( Request $request ) {

		$fid = $request->input('fid');
		$subid = $request->input('subid');
		$focusSubject = new focusSubject();
		$focusSubject->fid = $fid;
		$focusSubject->subid = $subid;
		$focusSubject->save();

		session()->flash('success','Project Added.');

		return redirect()->back();

	}


	public function addClass(  ) {

		$staff = staff::all();
		return view('class.add',[
			'staffs' =>$staff
		]);
	}

	public function postClass( Request $request  ) {


		$class = new classes();
		$class->name = $request->input('name');
		$class->stid = $request->input('stid');
		$class->save();

		session()->flash('success','Department Added Successfully');
		return redirect()->back();
	}

	public function classes(  ) {
		$classes = classes::all();
		return view('class.manage',[
			'classes' => $classes
		]);
	}

	public function editClass( $cid ) {
		$class = classes::find($cid);
		$staff = staff::where('category','Academic')->get();
		return view('class.edit',[
			'class' => $class,
			'staffs' => $staff
		]);
	}

	public function postEditClass(Request $request, $cid) {
		$class = classes::find($cid);
		$class->name = $request->input('name');
		$class->stid = $request->input('stid');
		$class->save();

		session()->flash('success','Department Updated.');
		return redirect()->back();
	}


	public function classDetail($clid) {

		$class = classes::findorfail($clid);
		return view('class.detail',[
			'class' => $class
		]);

	}














}
