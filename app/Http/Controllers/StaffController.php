<?php

namespace App\Http\Controllers;

use App\assignment;
use App\assignmentSubmission;
use App\bills;
use App\classes;
use App\confirmation;
use App\designation;
use App\file;
use App\focus;
use App\Mail\forgotPasswordMail;
use App\Mail\NewsEmail;
use App\news;
use App\newsImage;
use App\question;
use App\result;
use App\resultComment;
use App\sickbay;
use App\staff;
use App\student;
use App\subjectFile;
use App\subjectResult;
use App\subjects;
use App\subjectStudent;
use App\subjectTeacher;
use App\subjectUpdate;
use App\test;
use App\testClass;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Dompdf\Dompdf;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use PHPExcel_IOFactory;
use Illuminate\Http\Request;
use function PhpParser\filesInDir;
use RuntimeException;

class StaffController extends Controller
{


	public function __construct() {
		$this->middleware('staff');
	}

	public function dashboard() {

		if (session()->has('staff')){
			$stid = session()->get('staff')->stid;
			$this->staffId = $stid;
			$staff = staff::findorfail($stid);
			$subjects = $this->mySubjects();

			$class = $this->myClasses();

		}

		return view('dashboard.staff.dashboard',[
			'classes' => $class,
			'staff' => $staff,
			'subjects' => $subjects
		]);

	}

	public function files() {
		$stid = $this->stid();

		if(Input::has('term')){
			$term = Input::get('term');
			if(Input::has('type')) $type = Input::get('type');
			else
				$type = 'private';

			if($type == "all")
				$files = file::where('stid',$stid)->where('name','like',"%$term%")->paginate(50);
			else
			$files = file::where('stid',$stid)->where('name','like',"%$term%")->where('type', $type)->paginate(50);

			return view('dashboard.staff.files.manage',[
				'files' => $files,
				'type' => $type
			]);

		} else{

			$files = file::where('stid',$stid)->orWhere('type', "public")->orWhere('type','staff')->paginate(50);
			return view('dashboard.staff.files.manage',[
				'files' => $files
			]);
		}

	}

	public function addFile() {
		return view('dashboard.staff.files.add');
	}

	public function uploadFiles() {
		return view('dashboard.staff.files.add');
	}

	public function postUploadFiles( Request $request ) {

		$stid = $this->stid();

		if($request->hasFile('files')){
			foreach($request->file('files') as $file){
				$fileName = Carbon::now()->timestamp . $stid . $file->getClientOriginalName();
				$file->move('uploads/files/cloud/' . $stid .'/',$fileName);
				$fileUrl = url('uploads/files/cloud/' . $stid .'/' . $fileName);

				$cloudFile = new file();
				$cloudFile->name = $file->getClientOriginalName();
				$cloudFile->type = $request->input('type');
				$cloudFile->stid = $stid;
				$cloudFile->url = $fileUrl;
				$cloudFile->save();

			}
		}

		session()->flash('success','Files Uploaded.');
		return redirect()->back();

	}

	public function getResultUpload(  ) {
		$subjects = subjects::all();
		$classes = classes::all();
		return view('results.add',[
			'subjects' => $subjects,
			'classes' => $classes

		]);
	}


	public function postUploadQuestions( Request $request ) {

		$testid  = $request->input('testid');

		try {
			$inputFileName = $request->file('file')->getClientOriginalName();
			$request->file('file')->move("uploads/tests",$inputFileName);

			$downloadUrl = url("/uploads/tests/".$inputFileName);

			/* Identify file, create reader and load file  */
			$inputFileType = PHPExcel_IOFactory::identify( getcwd()."/" . "uploads/tests/".$inputFileName);
			$objReader = PHPExcel_IOFactory::createReader($inputFileType);
			$objPHPExcel = PHPExcel_IOFactory::load(getcwd()."/" . "uploads/tests/".$inputFileName);
			$sheet = $objPHPExcel->getActiveSheet();
			$highestRow = $sheet->getHighestRow();
			$highestColumn = $sheet->getHighestColumn();
			$time_pre = microtime(true);

			//  Read a row of data into an array
			$rowData = $sheet->rangeToArray('A2:' . $highestColumn . $highestRow,
				NULL, TRUE, FALSE);

			$count = 0;
			$failedImports = array();

			DB::beginTransaction();
			// add results to data base from file
			foreach($rowData as $cell) {
				try {

					if(!empty($cell[0])){ // ensure there's a question
						$count++;

						$question = new question();
						$question->testid = $testid;
						$question->question = $cell[0];

						if(!empty($cell[1]))$question->image = $cell[1];

						$question->option1 = $cell[2];
						$question->option2 = $cell[3];
						$question->option3 = $cell[4];
						$question->option4 = $cell[5];
						$question->option5 = $cell[6];
						$question->correctAnswer = $cell[7];
						$question->stid = session()->get('staff')->stid;
						$question->save();

					}


				}

				catch(\Exception $e){
					array_push( $failedImports, $count );
				}

			}

			DB::commit();

			if ( count( $failedImports ) > 0 ) {
				$error = "";
				foreach ( $failedImports as $item ) {
					$error .= ",$item";
				}

				$failed = count( $failedImports );
				session()->flash( 'error', "$failed did not import. [$error] " );
			}

			$number = $count - count( $failedImports );
			$request->session()->flash( 'success', "$count records in total. $number uploaded." );


			$request->session()->flash('success',"$count questions uploaded.");
			return redirect('test/' . $testid);

		}
		catch (\Exception $e) {
			die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME)
			    . '": ' . $e->getMessage());
		}


	}

	public function  uploadResults(Request $request) {
//			return $request->all();
			$cid = $request->input('cid');
			$subid  = $request->input('subid');

		try {
			$inputFileName = $request->file('file')->getClientOriginalName();
			$request->file('file')->move("uploads/result",$inputFileName);

			$downloadUrl = url("/uploads/result/".$inputFileName);

			/* Identify file, create reader and load file  */
			$inputFileType = PHPExcel_IOFactory::identify( getcwd()."/" . "uploads/result/".$inputFileName);
			$objReader = PHPExcel_IOFactory::createReader($inputFileType);
			$objPHPExcel = PHPExcel_IOFactory::load(getcwd()."/" . "uploads/result/".$inputFileName);
			$sheet = $objPHPExcel->getActiveSheet();
			$highestRow = $sheet->getHighestRow();
			$highestColumn = $sheet->getHighestColumn();
			$time_pre = microtime(true);

			//  Read a row of data into an array
			$rowData = $sheet->rangeToArray('A2:' . $highestColumn . $highestRow,
				NULL, TRUE, FALSE);

			$count = 0;
// add results to data base from file
			foreach($rowData as $cell) {
				$count++;
				try {

					$result         = new subjectResult();
					$result->subid    = $subid;
					$result->sid    = 5;
					$result->ca1  = $cell[0];
					$result->ca2  = $cell[1];
					$result->ca3  = $cell[2];
					$result->ca  = $cell[3];
					$result->exam  = $cell[4];
					$result->total  = $cell[5];

					$result->save();

				}

				catch(\Exception $e){}

			}

			$request->session()->flash('success',"$count result Uploaded.");
			return redirect()->back();

		}
		catch (\Exception $e) {
			die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME)
			    . '": ' . $e->getMessage());
		}


	}


	public function addResult(Request $request) {

		if (session()->has('staff')){
			$id = session()->get('staff')->stid;
			$staff = staff::findorfail($id);
			$classes = classes::all();

			$subIDs = array();

			$subjectTeachers = subjectTeacher::where('stid',$id)->get();

			foreach($subjectTeachers as $item){
				array_push($subIDs,$item->subid);
			}

			$subjects = subjects::where('stid',$id)->orWhereIn('subid',$subIDs)->get();

		}

		return view('dashboard.staff.results.addSingle',[
			'subjects' => $subjects,
			'classes' => $classes,
			'staff' => $staff
		]);
	}


	public function setupMidtermResult( $cid ) {

		$class = classes::find($cid);

		if(Input::has('population'))
		$population = Input::get('population');
		else $population = 0;

		if(Input::has('schoolOpened'))
		$schoolOpened = Input::get('schoolOpened');
		else $schoolOpened = 0;

		$students = student::where('cid',$cid)->get();
		return view('dashboard.staff.results.midterm.setup',[
			'students' => $students,
			'population' => $population,
			'schoolOpened' => $schoolOpened,
			'class' => $class
		]);
	}

	public function postSetupMidtermResult( Request $request ) {
		$cid = $request->input('cid');
		$sids = $request->input('sids');
		$fnames = $request->input('fnames');
		$snames = $request->input('snames');
		$gender = $request->input('gender');
		$house = $request->input('house');
		$population = $request->input('population');
		$attendance = $request->input('attendance');
		$schoolOpened = $request->input('schoolOpened');
		$comments = $request->input('comments');

		if(result::where('cid',$cid)->count() > 0){
			$batch = result::where('cid',$cid)->get()->last()->batch + 1;
			if(!isset($batch) || empty($batch)) $batch = 0;
		} else $batch = 0;



		DB::beginTransaction();
		for($i = 0; $i < count($sids); $i++){

			$tComments = resultComment::where('number',$comments[$i])->get()->random();


			$hComments = resultComment::where('number',$comments[$i])->get()->random();


			if($hComments->rcid == $tComments->rcid ) $hComments = resultComment::where('number',$comments[$i])->get()->random();

			$student = student::find($sids[$i]);
			$student->fname = $fnames[$i];
			$student->sname = $snames[$i];
			$student->house = $house[$i];
			$student->gender = $gender[$i];
			$student->save();

			$result = new result();
			$result->sid = $sids[$i];
			$result->cid = $cid;
			$result->attendance = $attendance[$i];
			$result->population = $population[$i];
			$result->schoolOpened = $schoolOpened[$i];
			$result->tComments = $tComments->comment;
			$result->hComments = $hComments->comment;
			$result->batch = $batch;
			$result->save();
		}
		DB::commit();

		session()->flash('success','Submitted.');
		return redirect('verify-midterm-result?cid=' . $cid);
	}


	public function verifyMidtermResult( Request $request) {

		$cid = Input::get('cid');

		if(result::where('cid',$cid)->where('population','<>',null)->count() <= 0) {
			session()->flash('error',"Result hasn't been generated.");
			return redirect()->back();
		}

		$lastResult = result::where('cid',$cid)->where('population','<>',null)->get()->last();

		$population = $lastResult->population;
		$schoolOpened = $lastResult->schoolOpened;

		$students = student::where('cid',$cid)->get();

		foreach($students as $student){
			$subids = array();

			$subjectResults = subjectResult::where('sid',$student->sid)->get();

			foreach($subjectResults as $subjectResult){
				array_push($subids,$subjectResult->subid);
			}
			$subjects = subjects::whereIn("subid",$subids)->get();


			$subjectScores = array();

			$emptyResultCount = 0;

			foreach($subjects as $subject){
				$result = subjectResult::where('sid',$student->sid)->where('subid',$subject->subid)->get()->last();

				$totalScore = $result->ce1 + $result->ce2 + $result->as1 + $result->cat1;
				$total = 5 + 5 + 5 + 10;
				$totalOver100 =  ($totalScore / $total ) * 100;
				$remark = '';
				$target = 60;
				$targetStatus = $totalOver100 - $target;


				if(!empty($result->ce1) &&
				   !empty($result->ce2) &&
				   !empty($result->as1 &&
				    !empty($result->cat1))
				){

					$resultItem = array(
						'ce1' => $result->ce1,
						'ce2' => $result->ce2,
						'as1' => $result->as1,
						'cat1' => $result->cat1,
						'total' => $totalScore,
						'totalOver100' => $totalOver100,
						'remarks' => $remark,
						'targetStatus' => $targetStatus

					);


					$subject['result'] = $resultItem;
					array_push($subjectScores,$totalOver100);

				} else{
					$emptyResultCount++;
				} // ensure the result is not empty


			}

			$numberOfSubjects = (count($subjects) - $emptyResultCount);
			$totalPossibleSubjectScore = $numberOfSubjects * 100;


			$totalSubjectScores = array_sum($subjectScores);
			$average = ($totalSubjectScores / $totalPossibleSubjectScore) * 100;

			$midTermResult = result::where('sid',$student->sid)->get()->last();
			$midTermResult->average = $average;
			$midTermResult->total = $totalSubjectScores;
			$midTermResult->save();


		}


		//get batch number
		if(result::where('cid',$cid)->count() > 0){
			$batch = result::where('cid',$cid)->get()->last()->batch;
			if(!isset($batch) || empty($batch)) $batch = 0;
		} else $batch = 0;


		$highestClassAverage = result::where('cid',$cid)->where('batch',$batch)->orderBy('average','desc')->get()->first()->average;


		$allResults = result::where('cid',$cid)->where('batch',$batch)->orderBy('average','desc')->get();

		$averages = array();

		$count = 1;

		foreach($allResults as $result){
			array_push($averages,$result->average);
			$result->position = $count;
			$result->save();
			$count++;
		}

		$students = student::where('cid',$cid)->get();



		return view('dashboard.staff.results.midterm.verify',[
			'students' => $students,
			'population' => $population,
			'schoolOpened' => $schoolOpened,
			'highestClassAverage' => $highestClassAverage
		]);
	}

	public function verifyMidtermResultDetails( $sid ) {
		$student = student::find($sid);

		$subids = array();

		$subjectResults = subjectResult::where('sid',$student->sid)->get();

		foreach($subjectResults as $subjectResult){
			array_push($subids,$subjectResult->subid);
		}
		$subjects = subjects::whereIn("subid",$subids)->get();

		$highestClassAverage = result::where('cid',$student->cid)->orderBy('average','desc')->get()->first()->average;
		$target = 60;

		foreach($subjects as $subject){
			$result = subjectResult::where('sid',$student->sid)->where('subid',$subject->subid)->get()->last();

			$totalScore = $result->ce1 + $result->ce2 + $result->as1 + $result->cat1;
			$total = 5 + 5 + 5 + 10;
			$totalOver100 =  ($totalScore / $total ) * 100;
			$letterGrade = '';
			$remark = '';
			$targetStatus = $totalOver100 - $target;

			switch ($totalOver100){
				case $totalOver100 < 50:
					$letterGrade = 'F';
					$remark = 'FAIL';
					break;
				case $totalOver100 >= 50 && $totalOver100 < 60:
					$letterGrade = 'E';
					$remark = 'PROBATION';
					break;
				case $totalOver100 >= 60 && $totalOver100 < 70:
					$letterGrade = 'C';
					$remark = 'CREDIT';
					break;
				case $totalOver100 >= 70 && $totalOver100 < 80:
					$letterGrade = 'B';
					$remark = 'GOOD';
					break;
				case $totalOver100 >= 80 && $totalOver100 < 90:
					$letterGrade = 'B+';
					$remark = 'VERY GOOD';
					break;
				case $totalOver100 >= 90 && $totalOver100 <= 100:
					$letterGrade = 'A';
					$remark = 'EXCELLENT';
					break;
			}

			$resultItem = array(
				'ce1' => $result->ce1,
				'ce2' => $result->ce2,
				'as1' => $result->as1,
			    'cat1' => $result->cat1,
				'total' => $totalScore,
				'totalOver100' => $totalOver100,
				'letterGrade' => $letterGrade,
				'remarks' => $remark,
				'targetStatus' => $targetStatus

		);


			$subject['result'] = $resultItem;

		}


		return view("dashboard.staff.results.midterm.verifyDetails",[
			'student' => $student,
			'subjects' => $subjects,
			'target' => $target,
			'highestClassAverage' => $highestClassAverage

		]);
	}

	public function submitMidtermResult( Request $request ) {

	}

	public function midtermResultPDF( $sid ) {

		$student = student::find($sid);

		$subids = array();

		$subjectResults = subjectResult::where('sid',$student->sid)->get();

		foreach($subjectResults as $subjectResult){
			array_push($subids,$subjectResult->subid);
		}
		$subjects = subjects::whereIn("subid",$subids)->get();

		$highestClassAverage = result::where('cid',$student->cid)->orderBy('average','desc')->get()->first()->average;
		$target = 60;

		foreach($subjects as $subject){
			$result = subjectResult::where('sid',$student->sid)->where('subid',$subject->subid)->get()->last();

			$totalScore = $result->ce1 + $result->ce2 + $result->as1 + $result->cat1;
			$total = 5 + 5 + 5 + 10;
			$totalOver100 =  ($totalScore / $total ) * 100;
			$letterGrade = '';
			$remark = '';
			$targetStatus = $totalOver100 - $target;

			switch ($totalOver100){
				case $totalOver100 < 50:
					$letterGrade = 'F';
					$remark = 'FAIL';
					break;
				case $totalOver100 >= 50 && $totalOver100 < 60:
					$letterGrade = 'E';
					$remark = 'PROBATION';
					break;
				case $totalOver100 >= 60 && $totalOver100 < 70:
					$letterGrade = 'C';
					$remark = 'CREDIT';
					break;
				case $totalOver100 >= 70 && $totalOver100 < 80:
					$letterGrade = 'B';
					$remark = 'GOOD';
					break;
				case $totalOver100 >= 80 && $totalOver100 < 90:
					$letterGrade = 'B+';
					$remark = 'VERY GOOD';
					break;
				case $totalOver100 >= 90 && $totalOver100 <= 100:
					$letterGrade = 'A';
					$remark = 'EXCELLENT';

			}

			$resultItem = array(
				'ce1' => $result->ce1,
				'ce2' => $result->ce2,
				'as1' => $result->as1,
				'cat1' => $result->cat1,
				'total' => $totalScore,
				'totalOver100' => $totalOver100,
				'letterGrade' => $letterGrade,
				'remarks' => $remark,
				'targetStatus' => $targetStatus

			);


			$subject['result'] = $resultItem;

		}


		return view("dashboard.staff.results.midterm.template",[
			'student' => $student,
			'subjects' => $subjects,
			'target' => $target,
			'highestClassAverage' => $highestClassAverage

		]);


	}


	public function generateMidtermResultPDF( $sid) {


		return $this->midtermResultPDF($sid);

		// instantiate and use the dompdf class
		$dompdf = new Dompdf();
		$dompdf->loadHtml($this->midtermResultPDF($sid));


		// (Optional) Setup the paper size and orientation
//		$dompdf->setPaper('A4', 'landscape');


		// Render the HTML as PDF
		$dompdf->render();

		// Output the generated PDF to Browser
		$dompdf->stream();
	}

	public function printClassResult( $cid ) {
		$students = student::where('cid',$cid)->get();
		$subjects = array();

		foreach($students as $student) {


			$subids = array();

			$subjectResults = subjectResult::where( 'sid', $student->sid )->get();

			foreach ( $subjectResults as $subjectResult ) {
				array_push( $subids, $subjectResult->subid );
			}
			$subjects = subjects::whereIn( "subid", $subids )->get();

			$highestClassAverage = result::where( 'cid', $student->cid )->orderBy( 'average', 'desc' )->get()->first()->average;
			$target              = 60;

			foreach ( $subjects as $subject ) {
				$result = subjectResult::where( 'sid', $student->sid )->where( 'subid', $subject->subid )->get()->last();

				$totalScore   = $result->ce1 + $result->ce2 + $result->as1 + $result->cat1;
				$total        = 5 + 5 + 5 + 10;
				$totalOver100 = ( $totalScore / $total ) * 100;
				$letterGrade  = '';
				$remark       = '';
				$targetStatus = $totalOver100 - $target;

				switch ( $totalOver100 ) {
					case $totalOver100 < 50:
						$letterGrade = 'F';
						$remark      = 'FAIL';
						break;
					case $totalOver100 >= 50 && $totalOver100 < 60:
						$letterGrade = 'E';
						$remark      = 'PROBATION';
						break;
					case $totalOver100 >= 60 && $totalOver100 < 70:
						$letterGrade = 'C';
						$remark      = 'CREDIT';
						break;
					case $totalOver100 >= 70 && $totalOver100 < 80:
						$letterGrade = 'B';
						$remark      = 'GOOD';
						break;
					case $totalOver100 >= 80 && $totalOver100 < 90:
						$letterGrade = 'B+';
						$remark      = 'VERY GOOD';
						break;
					case $totalOver100 >= 90 && $totalOver100 <= 100:
						$letterGrade = 'A';
						$remark      = 'EXCELLENT';

				}

				$resultItem = array(
					'ce1'          => $result->ce1,
					'ce2'          => $result->ce2,
					'as1'          => $result->as1,
					'cat1'         => $result->cat1,
					'total'        => $totalScore,
					'totalOver100' => $totalOver100,
					'letterGrade'  => $letterGrade,
					'remarks'      => $remark,
					'targetStatus' => $targetStatus

				);


				$subject['result'] = $resultItem;

				array_push($subjects,$subject);
			}

		}



		return view("dashboard.staff.results.midterm.printClassResultTemplate",[
			'students' => $students,
			'subjects' => $subjects,
			'target' => $target,
			'highestClassAverage' => $highestClassAverage

		]);


	}

	public function printClassMidtermResult($cid) {

		$students = student::where('cid',$cid)->get();
		$results = "";
		foreach($students as $student){
			$midterm = $this->midtermResultPDF($student->sid);
			$results .= $midterm;
//			array_push($results,$midterm);
		}

		return $results;
	}


	// end of term result


	public function setupEndOfTermResult( $cid ) {

		$class = classes::find($cid);

		if(Input::has('population'))
			$population = Input::get('population');
		else $population = 0;

		if(Input::has('schoolOpened'))
			$schoolOpened = Input::get('schoolOpened');
		else $schoolOpened = 0;

		$students = student::where('cid',$cid)->get();
		return view('dashboard.staff.results.endofterm.setup',[
			'students' => $students,
			'population' => $population,
			'schoolOpened' => $schoolOpened,
			'class' => $class
		]);
	}

	public function postSetupEndOfTermResult( Request $request ) {
		$cid = $request->input('cid');
		$sids = $request->input('sids');
		$fnames = $request->input('fnames');
		$snames = $request->input('snames');
		$gender = $request->input('gender');
		$house = $request->input('house');
		$population = $request->input('population');
		$attendance = $request->input('attendance');
		$schoolOpened = $request->input('schoolOpened');
		$comments = $request->input('comments');

		if(result::where('cid',$cid)->count() > 0){
			$batch = result::where('cid',$cid)->get()->last()->batch + 1;
			if(!isset($batch) || empty($batch)) $batch = 0;
		} else $batch = 0;



		DB::beginTransaction();
		for($i = 0; $i < count($sids); $i++){

			$tComments = resultComment::where('number',$comments[$i])->get()->random();


			$hComments = resultComment::where('number',$comments[$i])->get()->random();


			if($hComments->rcid == $tComments->rcid ) $hComments = resultComment::where('number',$comments[$i])->get()->random();

			$student = student::find($sids[$i]);
			$student->fname = $fnames[$i];
			$student->sname = $snames[$i];
			$student->house = $house[$i];
			$student->gender = $gender[$i];
			$student->save();

			$result = new result();
			$result->sid = $sids[$i];
			$result->cid = $cid;
			$result->attendance = $attendance[$i];
			$result->population = $population[$i];
			$result->schoolOpened = $schoolOpened[$i];
			$result->tComments = $tComments->comment;
			$result->hComments = $hComments->comment;
			$result->batch = $batch;
			$result->save();
		}
		DB::commit();

		session()->flash('success','Submitted.');
		return redirect('verify-endofterm-result?cid=' . $cid);
	}




	public function verifyEndOfTermResult( Request $request) {

		$cid = Input::get('cid');

		if(result::where('cid',$cid)->where('population','<>',null)->count() <= 0) {
			session()->flash('error',"Result hasn't been generated.");
			return redirect()->back();
		}

		$lastResult = result::where('cid',$cid)->where('population','<>',null)->get()->last();

		$population = $lastResult->population;
		$schoolOpened = $lastResult->schoolOpened;

		$students = student::where('cid',$cid)->get();

		foreach($students as $student){
			$subids = array();

			$subjectResults = subjectResult::where('sid',$student->sid)->get();

			foreach($subjectResults as $subjectResult){
				array_push($subids,$subjectResult->subid);
			}
			$subjects = subjects::whereIn("subid",$subids)->get();


			$subjectScores = array();

			$emptyResultCount = 0;

			foreach($subjects as $subject){
				$result = subjectResult::where('sid',$student->sid)->where('subid',$subject->subid)->get()->last();

				$cat1 = $result->ce1 + $result->ce2 + $result->as1 + $result->cat1;
				$cat2 = $result->ce3 + $result->ce4 + $result->as2 + $result->cat2;
				$exam = $result->exam;
				$project = $result->pr;

				$totalOver100 = $result->ce1 + $result->ce2 + $result->as1 + $result->cat1 + $result->ce3 + $result->ce4 + $result->as2 + $result->cat2 + $result->pr + $result->exam;
				$remark = '';
				$target = 60;
				$targetStatus = $totalOver100 - $target;


				if(!empty($result->ce1) &&
				   !empty($result->ce2) &&
				   !empty($result->as1) &&
				   !empty($result->ce3) &&
				   !empty($result->ce4) &&
				   !empty($result->as2) &&
				   !empty($result->cat2) &&
				   !empty($result->pr) &&
				   !empty($result->exam) &&
				   $totalOver100 > 0
				){

					$resultItem = array(
						'ca1' => $cat1,
						'ca2' => $cat2,
						'pr' => $project,
						'exam' => $exam,

						'totalOver100' => $totalOver100,
						'remarks' => $remark,
						'targetStatus' => $targetStatus

					);


					$subject['result'] = $resultItem;
					array_push($subjectScores,$totalOver100);

				} else{
					$emptyResultCount++;
				} // ensure the result is not empty


			}


			$numberOfSubjects = (count($subjects) - $emptyResultCount);
			$totalPossibleSubjectScore = $numberOfSubjects * 100;
			$totalSubjectScores = array_sum( $subjectScores );

			if($totalSubjectScores > 0 && $totalPossibleSubjectScore > 0) {

				$average            = ( $totalSubjectScores / $totalPossibleSubjectScore ) * 100;

				$endOfTermResult          = result::where( 'sid', $student->sid )->get()->last();
				$endOfTermResult->average = $average;
				$endOfTermResult->total   = $totalSubjectScores;
				$endOfTermResult->save();
			}

		}


		//get batch number
		if(result::where('cid',$cid)->count() > 0){
			$batch = result::where('cid',$cid)->get()->last()->batch;
			if(!isset($batch) || empty($batch)) $batch = 0;
		} else $batch = 0;


		$highestClassAverage = result::where('cid',$cid)->where('batch',$batch)->orderBy('average','desc')->get()->first()->average;


		$allResults = result::where('cid',$cid)->where('batch',$batch)->orderBy('average','desc')->get();

		$averages = array();

		$count = 1;

		foreach($allResults as $result){
			array_push($averages,$result->average);
			$result->position = $count;
			$result->save();
			$count++;
		}

		$students = student::where('cid',$cid)->get();



		return view('dashboard.staff.results.endofterm.verify',[
			'students' => $students,
			'population' => $population,
			'schoolOpened' => $schoolOpened,
			'highestClassAverage' => $highestClassAverage
		]);
	}

	public function verifyEndOfTermResultDetails( $sid ) {
		$student = student::find($sid);

		$subids = array();

		$subjectResults = subjectResult::where('sid',$student->sid)->get();

		foreach($subjectResults as $subjectResult){
			array_push($subids,$subjectResult->subid);
		}
		$subjects = subjects::whereIn("subid",$subids)->get();

		//get batch number
		if(result::where('cid',$student->cid)->count() > 0){
			$batch = result::where('cid',$student->cid)->get()->last()->batch;
			if(!isset($batch) || empty($batch)) $batch = 0;
		} else $batch = 0;


		$highestClassAverage = result::where('cid',$student->cid)->where('batch',$batch)->orderBy('average','desc')->get()->first()->average;

//		$highestClassAverage = result::where('cid',$student->cid)->orderBy('average','desc')->get()->first()->average;
		$target = 60;

		foreach($subjects as $subject){
			$result = subjectResult::where('sid',$student->sid)->where('subid',$subject->subid)->get()->last();

			$cat1 = $result->ce1 + $result->ce2 + $result->as1 + $result->cat1;
			$cat2 = $result->ce3 + $result->ce4 + $result->as2 + $result->cat2;
			$exam = $result->exam;
			$project = $result->pr;

			$totalOver100 = $result->ce1 + $result->ce2 + $result->as1 + $result->cat1 + $result->ce3 + $result->ce4 + $result->as2 + $result->cat2 + $result->pr + $result->exam;
			$target = 60;



			$letterGrade = '';
			$remark = '';
			$targetStatus = $totalOver100 - $target;

			switch ($totalOver100){
				case $totalOver100 < 50:
					$letterGrade = 'F';
					$remark = 'FAIL';
					break;
				case $totalOver100 >= 50 && $totalOver100 < 60:
					$letterGrade = 'E';
					$remark = 'PROBATION';
					break;
				case $totalOver100 >= 60 && $totalOver100 < 70:
					$letterGrade = 'C';
					$remark = 'CREDIT';
					break;
				case $totalOver100 >= 70 && $totalOver100 < 80:
					$letterGrade = 'B';
					$remark = 'GOOD';
					break;
				case $totalOver100 >= 80 && $totalOver100 < 90:
					$letterGrade = 'B+';
					$remark = 'VERY GOOD';
					break;
				case $totalOver100 >= 90 && $totalOver100 <= 100:
					$letterGrade = 'A';
					$remark = 'EXCELLENT';
					break;
			}

			$resultItem = array(
				'ca1' => $cat1,
				'ca2' => $cat2,
				'exam' => $exam,
				'pr' => $project,
				'totalOver100' => $totalOver100,
				'letterGrade' => $letterGrade,
				'remarks' => $remark,
				'targetStatus' => $targetStatus

			);


			$subject['result'] = $resultItem;

		}


		return view("dashboard.staff.results.endofterm.verifyDetails",[
			'student' => $student,
			'subjects' => $subjects,
			'target' => $target,
			'highestClassAverage' => $highestClassAverage

		]);
	}

	public function endoftermResultPDF( $sid ) {

		$student = student::find($sid);

		$subids = array();

		$subjectResults = subjectResult::where('sid',$student->sid)->get();

		foreach($subjectResults as $subjectResult){
			array_push($subids,$subjectResult->subid);
		}
		$subjects = subjects::whereIn("subid",$subids)->get();

		//get batch number
		if(result::where('cid',$student->cid)->count() > 0){
			$batch = result::where('cid',$student->cid)->get()->last()->batch;
			if(!isset($batch) || empty($batch)) $batch = 0;
		} else $batch = 0;

		$highestClassAverage = result::where('cid',$student->cid)->where('batch',$batch)->orderBy('average','desc')->get()->first()->average;

//		$highestClassAverage = result::where('cid',$student->cid)->orderBy('average','desc')->get()->first()->average;
		$target = 60;

		foreach($subjects as $subject){
			$result = subjectResult::where('sid',$student->sid)->where('subid',$subject->subid)->get()->last();

			$cat1 = $result->ce1 + $result->ce2 + $result->as1 + $result->cat1;
			$cat2 = $result->ce3 + $result->ce4 + $result->as2 + $result->cat2;
			$exam = $result->exam;
			$project = $result->pr;

			$totalOver100 = $result->ce1 + $result->ce2 + $result->as1 + $result->cat1 + $result->ce3 + $result->ce4 + $result->as2 + $result->cat2 + $result->pr + $result->exam;


			$letterGrade = '';
			$remark = '';
			$targetStatus = $totalOver100 - $target;

			switch ($totalOver100){
				case $totalOver100 < 50:
					$letterGrade = 'F';
					$remark = 'FAIL';
					break;
				case $totalOver100 >= 50 && $totalOver100 < 60:
					$letterGrade = 'E';
					$remark = 'PROBATION';
					break;
				case $totalOver100 >= 60 && $totalOver100 < 70:
					$letterGrade = 'C';
					$remark = 'CREDIT';
					break;
				case $totalOver100 >= 70 && $totalOver100 < 80:
					$letterGrade = 'B';
					$remark = 'GOOD';
					break;
				case $totalOver100 >= 80 && $totalOver100 < 90:
					$letterGrade = 'B+';
					$remark = 'VERY GOOD';
					break;
				case $totalOver100 >= 90 && $totalOver100 <= 100:
					$letterGrade = 'A';
					$remark = 'EXCELLENT';

			}

			$resultItem = array(
				'ca1' => $cat1,
				'ca2' => $cat2,
				'pr' => $project,
				'exam' => $exam,
				'totalOver100' => $totalOver100,
				'letterGrade' => $letterGrade,
				'remarks' => $remark,
				'targetStatus' => $targetStatus

			);


			$subject['result'] = $resultItem;

		}


		return view("dashboard.staff.results.endofterm.template",[
			'student' => $student,
			'subjects' => $subjects,
			'target' => $target,
			'highestClassAverage' => $highestClassAverage

		]);


	}

	public function generateEndOfTermResultPDF( $sid) {


		return $this->endoftermResultPDF($sid);

		$data = $this->endoftermResultPDF($sid);

		// instantiate and use the dompdf class
		$dompdf = new Dompdf();
		$dompdf->loadHtml($data);


		// (Optional) Setup the paper size and orientation
//		$dompdf->setPaper('A4', 'landscape');


		// Render the HTML as PDF
		$dompdf->render();

		// Output the generated PDF to Browser
		$dompdf->stream();
	}


	// ss3

	public function setupEndOfTermResultSS3( $cid ) {

		$class = classes::find($cid);

		if(Input::has('population'))
			$population = Input::get('population');
		else $population = 0;

		if(Input::has('schoolOpened'))
			$schoolOpened = Input::get('schoolOpened');
		else $schoolOpened = 0;

		$students = student::where('cid',$cid)->get();
		return view('dashboard.staff.results.endoftermSS3.setup',[
			'students' => $students,
			'population' => $population,
			'schoolOpened' => $schoolOpened,
			'class' => $class
		]);
	}

	public function postSetupEndOfTermResultSS3( Request $request ) {
		$cid = $request->input('cid');
		$sids = $request->input('sids');
		$fnames = $request->input('fnames');
		$snames = $request->input('snames');
		$gender = $request->input('gender');
		$house = $request->input('house');
		$population = $request->input('population');
		$attendance = $request->input('attendance');
		$schoolOpened = $request->input('schoolOpened');
		$comments = $request->input('comments');

		if(result::where('cid',$cid)->count() > 0){
			$batch = result::where('cid',$cid)->get()->last()->batch + 1;
			if(!isset($batch) || empty($batch)) $batch = 0;
		} else $batch = 0;



		DB::beginTransaction();
		for($i = 0; $i < count($sids); $i++){

			$tComments = resultComment::where('number',$comments[$i])->get()->random();


			$hComments = resultComment::where('number',$comments[$i])->get()->random();


			if($hComments->rcid == $tComments->rcid ) $hComments = resultComment::where('number',$comments[$i])->get()->random();

			$student = student::find($sids[$i]);
			$student->fname = $fnames[$i];
			$student->sname = $snames[$i];
			$student->house = $house[$i];
			$student->gender = $gender[$i];
			$student->save();

			$result = new result();
			$result->sid = $sids[$i];
			$result->cid = $cid;
			$result->attendance = $attendance[$i];
			$result->population = $population[$i];
			$result->schoolOpened = $schoolOpened[$i];
			$result->tComments = $tComments->comment;
			$result->hComments = $hComments->comment;
			$result->batch = $batch;
			$result->save();
		}
		DB::commit();

		session()->flash('success','Submitted.');
		return redirect('verify-endofterm-result-ss3?cid=' . $cid);
	}




	public function verifyEndOfTermResultSS3( Request $request) {

		$cid = Input::get('cid');

		if(result::where('cid',$cid)->where('population','<>',null)->count() <= 0) {
			session()->flash('error',"Result hasn't been generated.");
			return redirect()->back();
		}

		$lastResult = result::where('cid',$cid)->where('population','<>',null)->get()->last();

		$population = $lastResult->population;
		$schoolOpened = $lastResult->schoolOpened;

		$students = student::where('cid',$cid)->get();

		foreach($students as $student){
			$subids = array();

			$subjectResults = subjectResult::where('sid',$student->sid)->get();

			foreach($subjectResults as $subjectResult){
				array_push($subids,$subjectResult->subid);
			}
			$subjects = subjects::whereIn("subid",$subids)->get();


			$subjectScores = array();

			$emptyResultCount = 0;

			foreach($subjects as $subject){
				$result = subjectResult::where('sid',$student->sid)->where('subid',$subject->subid)->get()->last();


				$totalOver100 = ($result->exam / 40) * 100;
				$remark = '';
				$target = 60;
				$targetStatus = $totalOver100 - $target;


				if(!empty($result->exam) && $result->exam > 0 ){

					$resultItem = array(
//						'ce1' => $result->ce1,
//						'ce2' => $result->ce2,
//						'as1' => $result->as1,
//						'cat1' => $result->cat1,
//						'total' => $totalScore,
						'totalOver100' => $totalOver100,
						'remarks' => $remark,
						'targetStatus' => $targetStatus

					);


					$subject['result'] = $resultItem;
					array_push($subjectScores,$totalOver100);

				} else{
					$emptyResultCount++;
				} // ensure the result is not empty


			}

			$numberOfSubjects = (count($subjects) - $emptyResultCount);
			$totalPossibleSubjectScore = $numberOfSubjects * 100;


			$totalSubjectScores = array_sum($subjectScores);
			$average = ($totalSubjectScores / $totalPossibleSubjectScore) * 100;

			$midTermResult = result::where('sid',$student->sid)->get()->last();
			$midTermResult->average = $average;
			$midTermResult->total = $totalSubjectScores;
			$midTermResult->save();


		}


		//get batch number
		if(result::where('cid',$cid)->count() > 0){
			$batch = result::where('cid',$cid)->get()->last()->batch;
			if(!isset($batch) || empty($batch)) $batch = 0;
		} else $batch = 0;


		$highestClassAverage = result::where('cid',$cid)->where('batch',$batch)->orderBy('average','desc')->get()->first()->average;


		$allResults = result::where('cid',$cid)->where('batch',$batch)->orderBy('average','desc')->get();

		$averages = array();

		$count = 1;

		foreach($allResults as $result){
			array_push($averages,$result->average);
			$result->position = $count;
			$result->save();
			$count++;
		}

		$students = student::where('cid',$cid)->get();



		return view('dashboard.staff.results.endoftermSS3.verify',[
			'students' => $students,
			'population' => $population,
			'schoolOpened' => $schoolOpened,
			'highestClassAverage' => $highestClassAverage
		]);
	}

	public function verifyEndOfTermResultDetailsSS3( $sid ) {
		$student = student::find($sid);

		$subids = array();

		$subjectResults = subjectResult::where('sid',$student->sid)->get();

		foreach($subjectResults as $subjectResult){
			array_push($subids,$subjectResult->subid);
		}
		$subjects = subjects::whereIn("subid",$subids)->get();


		//get batch number
		if(result::where('cid',$student->cid)->count() > 0){
			$batch = result::where('cid',$student->cid)->get()->last()->batch;
			if(!isset($batch) || empty($batch)) $batch = 0;
		} else $batch = 0;


		$highestClassAverage = result::where('cid',$student->cid)->where('batch',$batch)->orderBy('average','desc')->get()->first()->average;

//		$highestClassAverage = result::where('cid',$student->cid)->orderBy('average','desc')->get()->first()->average;

		$target = 60;

		foreach($subjects as $subject){
			$result = subjectResult::where('sid',$student->sid)->where('subid',$subject->subid)->get()->last();

			$totalOver100 = ($result->exam / 40) * 100;
			$letterGrade = '';
			$remark = '';
			$targetStatus = $totalOver100 - $target;

			switch ($totalOver100){
				case $totalOver100 < 50:
					$letterGrade = 'F';
					$remark = 'FAIL';
					break;
				case $totalOver100 >= 50 && $totalOver100 < 60:
					$letterGrade = 'E';
					$remark = 'PROBATION';
					break;
				case $totalOver100 >= 60 && $totalOver100 < 70:
					$letterGrade = 'C';
					$remark = 'CREDIT';
					break;
				case $totalOver100 >= 70 && $totalOver100 < 80:
					$letterGrade = 'B';
					$remark = 'GOOD';
					break;
				case $totalOver100 >= 80 && $totalOver100 < 90:
					$letterGrade = 'B+';
					$remark = 'VERY GOOD';
					break;
				case $totalOver100 >= 90 && $totalOver100 <= 100:
					$letterGrade = 'A';
					$remark = 'EXCELLENT';
					break;
			}

			$resultItem = array(

				'totalOver100' => $totalOver100,
				'letterGrade' => $letterGrade,
				'remarks' => $remark,
				'targetStatus' => $targetStatus

			);


			$subject['result'] = $resultItem;

		}


		return view("dashboard.staff.results.endoftermSS3.verifyDetails",[
			'student' => $student,
			'subjects' => $subjects,
			'target' => $target,
			'highestClassAverage' => $highestClassAverage

		]);
	}

	public function enoftermResultPDFSS3( $sid ) {

		$student = student::find($sid);

		$subids = array();

		$subjectResults = subjectResult::where('sid',$student->sid)->get();

		foreach($subjectResults as $subjectResult){
			array_push($subids,$subjectResult->subid);
		}
		$subjects = subjects::whereIn("subid",$subids)->get();

		//get batch number
		if(result::where('cid',$student->cid)->count() > 0){
			$batch = result::where('cid',$student->cid)->get()->last()->batch;
			if(!isset($batch) || empty($batch)) $batch = 0;
		} else $batch = 0;


		$highestClassAverage = result::where('cid',$student->cid)->where('batch',$batch)->orderBy('average','desc')->get()->first()->average;

//		$highestClassAverage = result::where('cid',$student->cid)->orderBy('average','desc')->get()->first()->average;
		$target = 60;

		foreach($subjects as $subject){
			$result = subjectResult::where('sid',$student->sid)->where('subid',$subject->subid)->get()->last();

//			$totalScore = $result->ce1 + $result->ce2 + $result->as1 + $result->cat1;
//			$total = 5 + 5 + 5 + 10;
			$totalOver100 =  ($result->exam / 40 ) * 100;
			$letterGrade = '';
			$remark = '';
			$targetStatus = $totalOver100 - $target;

			switch ($totalOver100){
				case $totalOver100 < 50:
					$letterGrade = 'F';
					$remark = 'FAIL';
					break;
				case $totalOver100 >= 50 && $totalOver100 < 60:
					$letterGrade = 'E';
					$remark = 'PROBATION';
					break;
				case $totalOver100 >= 60 && $totalOver100 < 70:
					$letterGrade = 'C';
					$remark = 'CREDIT';
					break;
				case $totalOver100 >= 70 && $totalOver100 < 80:
					$letterGrade = 'B';
					$remark = 'GOOD';
					break;
				case $totalOver100 >= 80 && $totalOver100 < 90:
					$letterGrade = 'B+';
					$remark = 'VERY GOOD';
					break;
				case $totalOver100 >= 90 && $totalOver100 <= 100:
					$letterGrade = 'A';
					$remark = 'EXCELLENT';

			}

			$resultItem = array(
//				'ce1' => $result->ce1,
//				'ce2' => $result->ce2,
//				'as1' => $result->as1,
//				'cat1' => $result->cat1,
//				'total' => $totalScore,
				'totalOver100' => $totalOver100,
				'letterGrade' => $letterGrade,
				'remarks' => $remark,
				'targetStatus' => $targetStatus

			);


			$subject['result'] = $resultItem;

		}


		return view("dashboard.staff.results.endoftermSS3.template",[
			'student' => $student,
			'subjects' => $subjects,
			'target' => $target,
			'highestClassAverage' => $highestClassAverage

		]);


	}

	public function generateEndOfTermResultPDFSS3( $sid) {


		return $this->enoftermResultPDFSS3($sid);

		$data = $this->enoftermResultPDFSS3($sid);

		// instantiate and use the dompdf class
		$dompdf = new Dompdf();
		$dompdf->loadHtml($data);


		// (Optional) Setup the paper size and orientation
//		$dompdf->setPaper('A4', 'landscape');


		// Render the HTML as PDF
		$dompdf->render();

		// Output the generated PDF to Browser
		$dompdf->stream();
	}

	// finish end of term result


	public function generateBroadsheet( $cid ) {



		if(result::where('cid',$cid)->where('population','<>',null)->count() <= 0) {
			session()->flash('error',"Result hasn't been generated.");
			return redirect()->back();
		}

		$lastResult = result::where('cid',$cid)->where('population','<>',null)->get()->last();

		$population = $lastResult->population;
		$schoolOpened = $lastResult->schoolOpened;

		$students = student::where('cid',$cid)->get();

		$subidsFromClass = array();
		$studentData = array();
		foreach($students as $student){
			$subids = array();

			$subjectResults = subjectResult::where('sid',$student->sid)->get();

			foreach($subjectResults as $subjectResult){
				array_push($subids,$subjectResult->subid);
				array_push($subidsFromClass,$subjectResult->subid);
			}

			$subjects = subjects::whereIn("subid",$subids)->get();



			$subjectScores = array();

			$emptyResultCount = 0;

			$subjectData = array();
			$studentTotal = 0;

			foreach($subjects as $subject){
				$result = subjectResult::where('sid',$student->sid)->where('subid',$subject->subid)->get()->last();

				$cat1 = $result->ce1 + $result->ce2 + $result->as1 + $result->cat1;
				$cat2 = $result->ce3 + $result->ce4 + $result->as2 + $result->cat2;
				$exam = $result->exam;
				$project = $result->pr;

				$average = $result->ce1 + $result->ce2 + $result->as1 + $result->cat1 + $result->ce3 + $result->ce4 + $result->as2 + $result->cat2 + $result->pr + $result->exam;
				$remark = '';
				$target = 60;
				$targetStatus = $average - $target;


				if(!empty($result->ce1) &&
				   !empty($result->ce2) &&
				   !empty($result->as1) &&
				   !empty($result->ce3) &&
				   !empty($result->ce4) &&
				   !empty($result->as2) &&
				   !empty($result->cat2) &&
				   !empty($result->pr) &&
				   !empty($result->exam) &&
				   $average > 0
				){

					$resultItem = array(
						'ca1' => $cat1,
						'ca2' => $cat2,
						'pr' => $project,
						'exam' => $exam,

						'totalOver100' => $average,
						'remarks' => $remark,
						'targetStatus' => $targetStatus

					);

					$studentTotal += $average;

					$subject['result'] = $resultItem;
					array_push($subjectData,$subject);
					array_push($subjectScores,$average);

				} else{
					$emptyResultCount++;
				} // ensure the result is not empty

			}



			$numberOfSubjects = (count($subjects) - $emptyResultCount);
			$totalPossibleSubjectScore = $numberOfSubjects * 100;
			$totalSubjectScores = array_sum( $subjectScores );

			if($totalSubjectScores > 0 && $totalPossibleSubjectScore > 0) {

				$average            = ( $totalSubjectScores / $totalPossibleSubjectScore ) * 100;

				$endOfTermResult          = result::where( 'sid', $student->sid )->get()->last();
				$endOfTermResult->average = $average;
				$endOfTermResult->total   = $totalSubjectScores;
				$endOfTermResult->save();
			}


			$average = $studentTotal / count($subjectData);

				$letterGrade = '';

				switch ($average){
					case $average <= 49:
						$letterGrade = 'F';
						$remark = 'FAIL';
						break;
					case $average >= 50 && $average < 60:
						$letterGrade = 'E';
						$remark = 'PROBATION';
						break;
					case $average >= 60 && $average < 70:
						$letterGrade = 'C';
						$remark = 'CREDIT';
						break;
					case $average >= 70 && $average < 80:
						$letterGrade = 'B';
						$remark = 'GOOD';
						break;
					case $average >= 80 && $average < 90:
						$letterGrade = 'B+';
						$remark = 'VERY GOOD';
						break;
					case $average >= 90 && $average <= 100:
						$letterGrade = 'A';
						$remark = 'EXCELLENT';
						break;
				}


			$student['subjects'] = $subjectData;
			$student['total'] = $studentTotal;
			$student['noOfSubjects'] = count($subjectData);
			$student['average'] = $average;
			$student['letterGrade'] = $letterGrade;


			array_push($studentData,$student);
		}



		$studentsByPosition = collect($studentData)->sortByDesc('average');

		$position  = 1;
		foreach($studentsByPosition as $student){
			$student['position'] = $this->appendNumberSuffix($position);
			$position++;
		}


		$subjectsFromClass = subjects::whereIn('subid',$subidsFromClass)->get();
		$uniqueSubjectsFromClass = collect($subjectsFromClass)->unique();


//		$studentData = collect($studentData)->sortBy('fname');

		return view("dashboard.staff.results.broadSheet",[
			'students' => $studentData,
			'subjects' => $uniqueSubjectsFromClass,

		]);


	}

	public function generateBroadsheetSS3( $cid ) {



		if(result::where('cid',$cid)->where('population','<>',null)->count() <= 0) {
			session()->flash('error',"Result hasn't been generated.");
			return redirect()->back();
		}

		$lastResult = result::where('cid',$cid)->where('population','<>',null)->get()->last();

		$population = $lastResult->population;
		$schoolOpened = $lastResult->schoolOpened;

		$students = student::where('cid',$cid)->get();

		$subidsFromClass = array();
		$studentData = array();
		foreach($students as $student){
			$subids = array();

			$subjectResults = subjectResult::where('sid',$student->sid)->get();

			foreach($subjectResults as $subjectResult){
				array_push($subids,$subjectResult->subid);
				array_push($subidsFromClass,$subjectResult->subid);
			}

			$subjects = subjects::whereIn("subid",$subids)->get();



			$subjectScores = array();

			$emptyResultCount = 0;

			$subjectData = array();
			$studentTotal = 0;

			foreach($subjects as $subject){
				$result = subjectResult::where('sid',$student->sid)->where('subid',$subject->subid)->get()->last();

				$exam = ($result->exam / 40 ) * 100;

				$average = ($result->exam / 40 ) * 100;
//				$average = $result->ce1 + $result->ce2 + $result->as1 + $result->cat1 + $result->ce3 + $result->ce4 + $result->as2 + $result->cat2 + $result->pr + $result->exam;
				$remark = '';
				$target = 60;
				$targetStatus = $average - $target;


				if(
				   !empty($result->exam) &&
				   $average > 0
				){

					$resultItem = array(
						'exam' => $exam,

						'totalOver100' => $average,
						'remarks' => $remark,
						'targetStatus' => $targetStatus

					);

					$studentTotal += $average;

					$subject['result'] = $resultItem;
					array_push($subjectData,$subject);
					array_push($subjectScores,$average);

				} else{
					$emptyResultCount++;
				} // ensure the result is not empty

			}



			$numberOfSubjects = (count($subjects) - $emptyResultCount);
			$totalPossibleSubjectScore = $numberOfSubjects * 100;
			$totalSubjectScores = array_sum( $subjectScores );

			if($totalSubjectScores > 0 && $totalPossibleSubjectScore > 0) {

				$average            = ( $totalSubjectScores / $totalPossibleSubjectScore ) * 100;

				$endOfTermResult          = result::where( 'sid', $student->sid )->get()->last();
				$endOfTermResult->average = $average;
				$endOfTermResult->total   = $totalSubjectScores;
				$endOfTermResult->save();
			}


			$average = $studentTotal / count($subjectData);

			$letterGrade = '';

			switch ($average){
				case $average <= 49:
					$letterGrade = 'F';
					$remark = 'FAIL';
					break;
				case $average >= 50 && $average < 60:
					$letterGrade = 'E';
					$remark = 'PROBATION';
					break;
				case $average >= 60 && $average < 70:
					$letterGrade = 'C';
					$remark = 'CREDIT';
					break;
				case $average >= 70 && $average < 80:
					$letterGrade = 'B';
					$remark = 'GOOD';
					break;
				case $average >= 80 && $average < 90:
					$letterGrade = 'B+';
					$remark = 'VERY GOOD';
					break;
				case $average >= 90 && $average <= 100:
					$letterGrade = 'A';
					$remark = 'EXCELLENT';
					break;
			}


			$student['subjects'] = $subjectData;
			$student['total'] = $studentTotal;
			$student['noOfSubjects'] = count($subjectData);
			$student['average'] = $average;
			$student['letterGrade'] = $letterGrade;


			array_push($studentData,$student);
		}



		$studentsByPosition = collect($studentData)->sortByDesc('average');

		$position  = 1;
		foreach($studentsByPosition as $student){
			$student['position'] = $this->appendNumberSuffix($position);
			$position++;
		}


		$subjectsFromClass = subjects::whereIn('subid',$subidsFromClass)->get();
		$uniqueSubjectsFromClass = collect($subjectsFromClass)->unique();



		return view("dashboard.staff.results.endoftermSS3.broadSheet",[
			'students' => $studentData,
			'subjects' => $uniqueSubjectsFromClass,

		]);


	}


	public function scoreSheet( $subid, $cid ) {
		$students = student::where('cid',$cid)->get();
		$subject = subjects::find($subid);
		$class = classes::find($cid);

		$allResults = array();

		foreach($students as $student){
			$result = subjectResult::where('sid',$student->sid)->where('subid',$subject->subid)->get()->last();
			$totalOver100 = $result->ce1 + $result->ce2 + $result->as1 + $result->cat1 + $result->ce3 + $result->ce4 + $result->as2 + $result->cat2 + $result->pr + $result->exam;

			switch ($totalOver100){
				case $totalOver100 < 49:
					$letterGrade = 'F';
					$remark = 'FAIL';
					break;
				case $totalOver100 >= 50 && $totalOver100 < 60:
					$letterGrade = 'E';
					$remark = 'PROBATION';
					break;
				case $totalOver100 >= 60 && $totalOver100 < 70:
					$letterGrade = 'C';
					$remark = 'CREDIT';
					break;
				case $totalOver100 >= 70 && $totalOver100 < 80:
					$letterGrade = 'B';
					$remark = 'GOOD';
					break;
				case $totalOver100 >= 80 && $totalOver100 < 90:
					$letterGrade = 'B+';
					$remark = 'VERY GOOD';
					break;
				case $totalOver100 >= 90 && $totalOver100 <= 100:
					$letterGrade = 'A';
					$remark = 'EXCELLENT';

			}

			$result->total = $totalOver100;
			$result->save();
			$student['result'] = $result;
			$student['letterGrade'] = $letterGrade;

			array_push($allResults,$result);

		}


		$allResults = collect($allResults)->sortByDesc('total');
		$count = 1;

		foreach($allResults as $result){
			foreach($students as $student){
				if($result->sid == $student->sid){
					$student['position'] = $this->appendNumberSuffix($count);
				}
			}
			$count++;
		}


		return view('dashboard.staff.results.scoreSheet',[
			'students' => $students,
			'subject' => $subject,
			'class' => $class
		]);

	}

	function appendNumberSuffix($i){return strtoupper($i.@(($j=abs($i)%100)>10&&$j<14?th:[th,st,nd,rd][$j%10]?:th));}


	public function result(Request $request) {
		$class = $request->input('cid');
		$subid = $request->input('subid');


		return redirect('add-result/subject/' .$subid . '/'. $class );
	}

	public function addSubjectResult( $subid, $cid ) {
		$students = student::where('cid',$cid)->get();
		$subject = subjects::find($subid);

		$results = array();

		foreach($students as $student){
			$result = subjectResult::where('sid',$student->sid)->where('subid',$subject->subid)->get()->last();
			$student['result'] = $result;
		}


		return view('results.studentsManage',[
			'students' => $students,
			'subject' => $subject,
			'cid' => $cid

		]);

	}

	public function postAddSubjectResults( Request $request ) {

		$sids = $request->input('sids');
		$ce1s = $request->input('ce1');
		$ce2s = $request->input('ce2');
		$as1s = $request->input('as1');
		$cat1s = $request->input('cat1');
		$ce3s = $request->input('ce3');
		$ce4s = $request->input('ce4');
		$as2s = $request->input('as2');
		$cat2s = $request->input('cat2');
		$prs = $request->input('pr');
		$exams = $request->input('exam');



		for($i = 0; $i < count($sids); $i++){

			if($ce1s[$i] > 5){
				$ce1s[$i] = ($ce1s[$i] / 100) * 5;
			}

			if($ce2s[$i] > 5){
				$ce2s[$i] = ($ce2s[$i] / 100) * 5;
			}

			if($as1s[$i] > 5){
				$as1s[$i] = ($as1s[$i] / 100) * 5;
			}

			if($cat1s[$i] > 10){
				$cat1s[$i] = ($cat1s[$i] / 100) * 10;
			}

			if($ce3s[$i] > 5){
				$ce3s[$i] = ($ce3s[$i] / 100) * 5;
			}

			if($ce4s[$i] > 5){
				$ce4s[$i] = ($ce4s[$i] / 100) * 5;
			}

			if($as2s[$i] > 5){
				$as2s[$i] = ($as2s[$i] / 100) * 5;
			}

			if($cat2s[$i] > 10){
				$cat2s[$i] = ($cat2s[$i] / 100) * 10;
			}

			if($prs[$i] > 10){
				$prs[$i] = ($prs[$i] / 100) * 10;
			}

//			if($exams[$i] > 40){
				$exams[$i] = ($exams[$i] / 100) * 40;
//			}

			$subjectResult = new subjectResult();
			$subjectResult->subid = $request->input('subid');
			$subjectResult->sid = $sids[$i];
			$subjectResult->ce1 = $ce1s[$i];
			$subjectResult->ce2 = $ce2s[$i];
			$subjectResult->as1 = $as1s[$i];
			$subjectResult->cat1 = $cat1s[$i];
			$subjectResult->ce3 = $ce3s[$i];
			$subjectResult->ce4 = $ce4s[$i];
			$subjectResult->as2 = $as2s[$i];
			$subjectResult->cat2 = $cat2s[$i];
			$subjectResult->pr = $prs[$i];
			$subjectResult->exam = $exams[$i];
 			$subjectResult->save();
		}

		 session()->flash('success','Results added');
		 return redirect()->back();
	}


	public function addResultEntry(Request $request,$subid,$cid,$sid){

		$class = classes::findorfail($cid);
		$subject = subjects::findorfail($subid);
		$student = student::findorfail($sid);

		$result = subjectResult::where('subid',$subid)->where('sid',$sid)->first();

		$data = [
			'class' =>$class,
			'subject' => $subject,
			'student' => $student
		];

		return view('results.resultEntry',[
			'class' =>$class,
			'subject' => $subject,
			'student' => $student,
			'result' => $result

 		]);

	}


	public function postResultEntry(Request $request) {

		try{
			$subid = $request->input('subid');
			$sid = $request->input('sid');

			$subjectResult = new subjectResult();
			$subjectResult->subid = $subid;
			$subjectResult->sid = $sid;
			$subjectResult->ce1 = $request->input('ce1');
			$subjectResult->ce2 = $request->input('ce2');
			$subjectResult->as1 = $request->input('as1');
			$subjectResult->cat1 = $request->input('cat1');
			$subjectResult->ce3 = $request->input('ce3');
			$subjectResult->ce4 = $request->input('ce4');
			$subjectResult->as2 = $request->input('as2');
			$subjectResult->cat2 = $request->input('cat2');
			$subjectResult->pr = $request->input('pr');
			$subjectResult->exam = $request->input('exam');
//		$subjectResult->ca = $subjectResult->ca1+$subjectResult->ca2+$subjectResult->ca3;
			$subjectResult->total = $subjectResult->ca+$subjectResult->exam;
			$subjectResult->comments = $request->input('comments');
			$subjectResult->save();

			session()->flash('success','Result Saved.');
			return redirect('add-result/subject');

		}catch (\Exception $exception){
			session()->flash('error','Sorry, Something Went Wrong');
		}


	}

	public function addTest() {
		$stid = session('staff')->stid;

		$subjectTeachers = subjectTeacher::where('stid',$stid)->get();
		$subIDs = array();
		$cids = array();

		foreach($subjectTeachers as $subjectTeacher){
			array_push($subIDs,$subjectTeacher->subid);
			array_push($cids,json_decode($subjectTeacher->cids));
		}
		$subjects = subjects::where('stid', $stid)->orWhereIn('subid',$subIDs)->get();

		$classes = classes::where('stid', $stid)->orWhereIn('cid',$cids)->get();


		$classes = classes::all();

		return view('dashboard.staff.tests.add',[
			'classes'  => $classes,
			'subjects' => $subjects
		]);
	}

	public function manageTests() {
		$tests = test::where('stid',session()->get('staff')->stid)->orderBy('created_at','desc')->paginate(30);
		$from = collect($tests)->get('from');
		return view('dashboard.staff.tests.manage',[
			'tests' => $tests,
			'from'  => $from
		]);
	}

	public function testDetails($testid) {
		$test = test::find($testid);

		return view('dashboard.staff.tests.details',[
			'test' => $test
		]);
	}

	public function postEditTest( Request $request ) {


		$testid = $request->input('testid');
		$startTIme = $request->input('startTime');
		$endTime   = $request->input('endTime');
		$attempts  = $request->input('attempts');

		if(!empty($startTIme) && !empty($endTime)) {
			$startTIme = Carbon::createFromFormat( "m/d/Y H:i", $startTIme );
			$endTime   = Carbon::createFromFormat( "m/d/Y H:i", $endTime );

			$test            = test::find( $testid );
			$test->startTime = $startTIme;
			$test->endTime   = $endTime;
			$test->stid      = session()->get( 'staff' )->stid;
			$test->save();

		}

		if(!empty($attempts)){
			$test            = test::find( $testid );
			$test->attempts = $attempts;
			$test->stid      = session()->get( 'staff' )->stid;
			$test->save();

		}

		session()->flash('success','Test Edited');
		return redirect()->back();
	}

	public function postAddTest(Request $request) {

		$cids      = $request->input('cids');
		$startTIme = $request->input('startTime');
		$endTime   = $request->input('endTime');
		$attempts  = $request->input('attempts');

		$startTIme = Carbon::createFromFormat("m/d/Y H:i",$startTIme);
		$endTime   = Carbon::createFromFormat("m/d/Y H:i",$endTime);


		$test = new test();
		$test->name = $request->input('title');
		$test->description = $request->input('description');
		$test->duration = $request->input('duration');
		$test->startTime = $startTIme;
		$test->endTime = $endTime;
		$test->stid = session()->get('staff')->stid;
		$test->subid = $request->input('subid');
		if(!empty($attempts))
		$test->attempts = $request->input('attempts');
		$test->save();

		foreach($cids as $cid){
			$testClass = new testClass();
			$testClass->testid = $test->testid;
			$testClass->cid = $cid;
			$testClass->save();

		}

		session()->flash('success','Test Added.');
		return redirect()->back();


	}

	public function testResults( $testid ) {
		$test = test::find($testid);


		return view('dashboard.staff.tests.results',[
			'test' => $test
		]);
	}


	public function addQuestion( $testid ) {
		$test = test::find($testid);
		return view('dashboard.staff.questions.add',[
			'test' => $test
		]);
	}

	public function questionDetails( $qid ) {
		$question = question::find($qid);

		return view('dashboard.staff.questions.details',[
			'question' => $question
		]);
	}

	public function deleteAllQuestions( $testid ) {
		$test = test::find($testid);
		foreach($test->Questions as $question){
			$question->delete();
		}
		session()->flash('success','Questions Deleted.');
		return redirect('test/' . $testid);
	}

	public function deleteQuestion( $qid ) {
		$question = question::find($qid);
		$testid = $question->Test->testid;
		question::destroy($qid);
		session()->flash('success','Question Deleted.');
		return redirect('test/' . $testid);
	}

	public function postAddQuestion( Request $request ) {
		$testid = $request->input('testid');

		if($request->hasFile('image')){
			$filename = Carbon::now()->timestamp . $request->file('image')->getClientOriginalName();

			$request->file('image')->move('uploads/tests',$filename);
			$imageUrl = url('uploads/tests/' . $filename);
		}

		$question = new question();
		$question->testid = $testid;
		$question->question = $request->input('question');
		$question->option1 = $request->input('option1');
		$question->option2 = $request->input('option2');
		$question->option3 = $request->input('option3');
		$question->option4 = $request->input('option4');
		$question->option5 = $request->input('option5');
		$question->correctAnswer = $request->input('correctAnswer');
		$question->stid = session()->get('staff')->stid;

		if($request->hasFile('image')) $question->image = $imageUrl;

		$question->save();

		session()->flash('success','Question Added');
		return redirect()->back();

	}

	public function uploadQuestions( $testid ) {
		$test = test::find($testid);
		return view('dashboard.staff.questions.upload',[
			'test' => $test
		]);

	}


	public function getStudents(Request $request) {

		if (Input::has('name')){
			$name = Input::get('name');
			$students = student::where('fname','like',"%$name%" or 'sname','like',"%$name%")->get();
		}else{
			$students = student::all();
		}

		return view('sickbay.manage',[
			'students' => $students
		]);

	}

	public function sickbayStudentDetail($sid ) {
		$student = student::findorfail($sid);
		$records = sickbay::where('sid',$sid)->orderBy('created_at','desc')->get();
//		return $records;
		return view('sickbay.detail',[
			'student'=> $student,
			'records' => $records
		]);

		}

	public function postSickbayVisit( Request $request,$sid ) {
//		return $request->all();
//		$sid = $request->input('sid');
		$record = new sickbay();
		$record->sid = $sid;
		$record->stid = session()->get('staff')->stid;
		$record->diagnosis = $request->input('diagnosis');
		$record->treatment = $request->input('treatment');
		$record->details = $request->input('details');

		$done = $record->save();
		if ($done){
			session()->flash('success','SickBay Visit Saved.');
			return redirect()->back();
		}else{
			session()->flash('error','Sorry Something Went Wrong.');
			return redirect()->back();
		}
		}


	public function sickbayVisitDetail($sbid) {
		$record = sickbay::findorfail($sbid);

		return view('sickbay.visitDetail',[
			'record'=> $record
		]);
		}


	public function staffSubjects(  ) {

		$subjects = $this->mySubjects();
		return view('dashboard.staff.subjects.manage',[
			'subjects' => $subjects
		]);
	}

	public function subjectDetails($subid) {
		$subject = subjects::find($subid);

		return view('dashboard.staff.subjects.details',[
			'subject' => $subject,
			'tests' => $subject->Tests,
			'assignments' => $subject->Assignments
		]);


	}



	public function staffClasses() {
		$id = session('staff')->stid;
		$classes = classes::where('stid', $id)->get();
		return view('dashboard.staff.classes.manage',[
			'classes' => $classes
		]);
	}

	public function classDetails( $cid ) {
		$class = classes::find($cid);

		return view('dashboard.staff.classes.details',[
			'class' => $class
		]);
	}


	public function addBill(  ) {
		return view('dashboard.staff.bills.add');
		}


	public function postBill(Request $request ) {

		$bill  = new bills();
		$bill->name = $request->input('name');
		$bill->due = $request->input('due');
		$bill->description = $request->input('description');
		$bill->amount = $request->input('amount');
		$status = $bill-> save();
		if ($status){
			session()->flash('success','Bill Successfully Created');
			return redirect()->back();
		}else{
			session()->flash('error','Sorry Something Went Wrong ');
			return redirect()->back();
		}

		}

	public function getBills(Request $request ) {
		$bills = bills::orderBy('created_at','desc')->paginate(10);
		return view('dashboard.staff.bills.manage',[
			'bills' => $bills
		]);
		}


	public function billDetails($bid) {
		$bill = bills::findorfail($bid);
		return view('dashboard.staff.bills.details',[
			'bill' => $bill
		]);
	}


	public function assignStudentBill( Request $requeset ) {


	}


	public function postAddSubjectUpdate( Request $request ) {
		$subjectUpdate = new subjectUpdate();
		$subjectUpdate->stid = session()->get('staff')->stid;
		$subjectUpdate->subid = $request->input('subid');
		$subjectUpdate->update = $request->input('update');
		$subjectUpdate->save();

		session()->flash('success','Update Posted.');
		return redirect()->back();
	}


	public function deleteSubjectUpdate( $suid ) {
		subjectUpdate::destroy($suid);
		session()->flash('success','Update Deleted.');
		return redirect()->back();
	}

	public function deleteSubjectFile( $sfid ) {

		$subjectFile = subjectFile::find($sfid);
		$urlArray = explode("/", $subjectFile->url);
		$fileName = $urlArray[3]. "/" . $urlArray[4];
		unlink($fileName);
		subjectFile::destroy($sfid);
		session()->flash('success','File Deleted');
		return redirect()->back();

	}

	public function postAddSubjectFile(Request $request) {

		$subid = $request->input('subid');
		if($request->hasFile('files')){
			foreach($request->file('files') as $file){
				$fileName = Carbon::now()->timestamp . $subid. $file->getClientOriginalName();
				$file->move('uploads/files',$fileName);
				$fileUrl = url('uploads/files' . $fileName);

				$subjectFile = new subjectFile();
				$subjectFile->stid = $request->input('stid');
				$subjectFile->subid = $subid;
				$subjectFile->description = $request->input('description');
				$subjectFile->url = $fileUrl;
				$subjectFile->save();
			}
		}

		session()->flash('success','Files Uploaded.');
		return redirect()->back();
	}


	public function assignments() {

		$stid = session()->get('staff')->stid;
		$assignments = assignment::where('stid',$stid)->get();
		return view('dashboard.staff.assignments.manage',[
			'assignments' => $assignments
		]);
	}

	public function addNews() {
		return view('dashboard.staff.news.add');
	}

	public function postAddNews(Request $request) {
		$stid = session()->get('staff')->stid;

		$news = new news();
		$news->stid = $stid;
		$news->title = $request->input('title');
		$news->content = $request->input('content');
		$news->save();


		if($request->hasFile('images')) {
			foreach ( $request->file( 'images' ) as $item ) {
				$rand           = Str::random(5) . \Carbon\Carbon::now()->timestamp;
				$inputFileName = $item->getClientOriginalName();
				$item->move( "uploads/news", $rand . $inputFileName );

				$images      = new newsImage();
				$images->newsid = $news->newsid;
				$images->url = url( 'uploads/news/'.$rand.$inputFileName );
				$images->save();
			}
		}

		Mail::to('toby.okeke@gmail.com')->send(new NewsEmail($news));

		session()->flash('success','News Added.');
		return redirect()->back();
	}

	public function news() {
		$news = news::orderByDesc('created_at')->paginate(10);

		$from = collect($news)->get('from');

		return view('dashboard.staff.news.manage',[
			'newsItems' => $news,
			'from' => $from
		]);
	}

	public function newsDetails( $newsid ) {
		$news = news::find($newsid);
		return view('dashboard.staff.news.details',[
			'news' => $news
		]);
	}

	public function deleteNews( $newsid ) {
		news::destroy($newsid);
		$images = newsImage::where('newsid',$newsid)->get();
		foreach($images as $image){
			$urlArray = explode("/", $image->url);
			$fileName = $urlArray[3]. "/" .$urlArray[4]. "/" . $urlArray[5];

			unlink($fileName);
			$image->delete();
		}

		session()->flash('success','News Deleted.');
		return redirect('staff/news');
	}

	public function addAssignment() {
		$focuses = focus::all();

		$subjects = $this->mySubjects();

		$classes = classes::all();


		return view('dashboard.staff.assignments.add',
			[
				'focuses' => $focuses,
				'subjects' => $subjects,
				'classes' => $classes
			]);
	}

	public function assignmentSubmissions( $aid ) {

	}

	public function getGradeAssignmentSubmissions($asid){
		$assignment = assignment::all();
		$submission = assignmentSubmission::findorfail($asid);
		return view('dashboard.staff.assignments.grade',[
			'submission' => $submission,
			'assignment' => $assignment
		]);
	}

	public function postGradeAssignmentSubmissions(Request $request, $asid){

		$submission = assignmentSubmission::findorfail($asid);
		$submission->grade = $request->input('grade');
		$submission->comment = $request->input('comment');
		$status = $submission->save();
	
		if ($status){
			session()->flash('success','Project Grade Added Successfully');
		}else{
			session()->flash('error','Something Went Wrong');
		}
	
		return redirect()->back();
	}




	public function postAddAssignment( Request $request ) {

		$due   = $request->input('due');
		$due = Carbon::createFromFormat("m/d/Y H:i",$due);
		$subid = $request->input('subid');


		if($request->hasFile('file')){
			$fileName = Carbon::now()->timestamp . $subid . $request->file('file')->getClientOriginalName();
			$request->file('file')->move('uploads/assignments',$fileName);
			$fileUrl = url('uploads/assignments/' . $fileName);
		} else{
			session()->flash('error','Please attach a file.');
			return redirect()->back();
		}

		$assignment = new assignment();
		 $assignment->name = $request->input('name');
		 $assignment->subid = $request->input('subid');
		 $assignment->stid = $this->stid();
		 $assignment->description = $request->input('description');
		 $assignment->due = $due;
		 $assignment->cid = $request->input('cid');
		 $assignment->total = $request->input('total');
		 $assignment->url = $fileUrl;
		 $assignment->save();
		 session()->flash('success','Assignment Added.');
		 return redirect()->back();


	}

	public function assignmentDetails( $aid ) {
		$assignment = assignment::find($aid);

		return view('dashboard.staff.assignments.details',[
			'assignment' => $assignment
		]);
	}


	public function deleteAssignment( $aid ) {

		$assignment = assignment::find($aid);
		$urlArray = explode("/", $assignment->url);
		$fileName = $urlArray[3]. "/" .$urlArray[4]. "/" . $urlArray[5];

		unlink($fileName);
		$assignment->delete();

		session()->flash('success','Assignment Deleted');
		return redirect('staff/assignments');
	}


	public function stid() {
		if(session()->has('staff')) $stid = session()->get('staff')->stid;
		else
			return redirect("staff/login");
		return $stid;
	}

	public function mySubjects(){
		$stid = session()->get('staff')->stid;
		$subIDs = array();
		$subjectTeachers = subjectTeacher::where('stid',$stid)->get();

		foreach($subjectTeachers as $subjectTeacher){
			array_push($subIDs,$subjectTeacher->subid);
		}
		$subjects = subjects::where('stid', $stid)->orWhereIn('subid',$subIDs)->get();

		return $subjects;
	}

	public function myClasses(  ) {

		$stid = session()->get('staff')->stid;
		$cids = array();
		$subjectTeachers = subjectTeacher::where('stid',$stid)->get();

		foreach($subjectTeachers as $subjectTeacher){

			array_push($cids,$subjectTeacher->cid);
		}

		$classes = classes::where('stid', $stid)->orWhereIn('cid',$cids)->get();

		return $classes;

	}


}
