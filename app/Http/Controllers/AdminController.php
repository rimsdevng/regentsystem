<?php

namespace App\Http\Controllers;

use App\Application;
use App\classes;
use App\designation;
use App\focus;
use App\focusSubject;
use App\parents;
use App\resultComment;
use App\sickbay;
use App\staff;
use App\student;
use App\subjects;
use App\subjectStudent;
use App\subjectTeacher;
use App\test;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use PHPExcel_IOFactory;
use PHPExcel_Shared_Date;

class AdminController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
	}

	public function index()
	{
		$students  = student::all();
		$staffs      = staff::all();
		$parents    = parents::all();
		$subjects   = subjects::all();
		$pendingApplications = Application::where('status','pending')->get();
		$applications = Application::all();


		return view('home',[
			'staff' =>$staffs,
			'students' => $students,
			'subjects' => $subjects,
			'parents' =>  $parents,
			'pendingApplications' => $pendingApplications,
			'applications' => $applications

		]);
	}


	public function addStaff(  ) {
		$designation = designation::all();
		return view('staff.add',[
			'designations' => $designation
		]);
	}

	public function uploadStaff() {
		return view('staff.upload');
	}

	public function resultComments() {

		$resultComments = resultComment::orderBy('created_at','desc')->paginate();

		return view('resultcomments.manage',[
			'comments' => $resultComments
		]);

	}

	public function addResultComment() {
		return view('resultcomments.add');
	}

	public function postAddResultComment( Request $request ) {
		$resultComment = new resultComment();
		$resultComment->comment = $request->input('comment');
		$resultComment->number = $request->input('number');
		$resultComment->save();

		session()->flash('success','Comment Added');
		return redirect()->back();
	}

	public function deleteResultComment( $rcid ) {
		resultComment::destroy($rcid);
		session()->flash('success','Comment Deleted.');
		return redirect()->back();
	}


	public function resultCommentDetails( $rcid ) {

	}

	public function postUploadStaff( Request $request ) {


		if ( $request->hasFile( 'file' ) ) {
			$fileName = Carbon::now()->timestamp . $request->file( 'file' )->getClientOriginalName();
			$request->file( 'file' )->move( 'uploads/staff', $fileName );
			$fileUrl = url( 'uploads/staff/' . $fileName );
		} else {
			session()->flash( 'error', 'Please attach a file.' );

			return redirect()->back();
		}


		try {


			$downloadUrl = url( "/uploads/staff/" . $fileName );

			/* Identify file, create reader and load file  */
			$inputFileType = PHPExcel_IOFactory::identify( getcwd() . "/" . "uploads/staff/" . $fileName );
			$objReader     = PHPExcel_IOFactory::createReader( $inputFileType );
			$objPHPExcel   = PHPExcel_IOFactory::load( getcwd() . "/" . "uploads/staff/" . $fileName );

			$sheet         = $objPHPExcel->getActiveSheet();
			$highestRow    = $sheet->getHighestRow();
			$highestColumn = $sheet->getHighestColumn();
			$time_pre      = microtime( true );

			//  Read a row of data into an array
			$rowData = $sheet->rangeToArray( 'A2:' . $highestColumn . $highestRow,
				null, false, false );

			$count = 0;

			$failedImports = array();

			DB::beginTransaction();
			// add results to data base from file
			foreach ( $rowData as $cell ) {
				$count ++;
				try {


					$date        = "";
					$dateCell    = $objPHPExcel->getActiveSheet()->getCell( 'G' . ( $count + 1 ) );
					$invalidDate = $objPHPExcel->getActiveSheet()->getCell( 'G' . ( $count + 1 ) )->getValue();
					if ( PHPExcel_Shared_Date::isDateTime( $dateCell ) ) {
						$date = date( "d-m-Y", PHPExcel_Shared_Date::ExcelToPHP( $invalidDate ) );
					}


					$staff                     = new staff();
					$staff->title              = $cell[0];
					$staff->fname              = $cell[1];
					$staff->sname              = $cell[2];
					$staff->gender             = $cell[4];
					$staff->nationality        = $cell[5];
					$staff->phone              = $cell[6];
					$staff->dob                = $date;
					$staff->stateOfOrigin      = $cell[8];
					$staff->email              = strtolower($cell[9]);
					$staff->bio                = $cell[10];
					$staff->residentialAddress = $cell[11];

					$staff->image = url( "admin/images/default-image.jpg" );


					$dTerm       = strtolower( $cell[3] );
					$designation = designation::where( 'name', $dTerm )->get();

					if ( count( $designation ) <= 0 ) {

						$designation           = new designation();
						$designation->name     = title_case( $cell[3] );
						$designation->category = $cell[12];
						$designation->save();

					} else {
						$designation = designation::where( 'name', $dTerm )->first();
					}


					$staff->did      = $designation->did;
					$staff->category = $designation->category;
					$staff->password = bcrypt( "regent123" );

					$staff->save();


				} catch ( \Exception $e ) {
					return $e->getMessage();
					array_push( $failedImports, $count );
				}

			}

			DB::commit();
			if ( count( $failedImports ) > 0 ) {
				$error = "";
				foreach ( $failedImports as $item ) {
					$error .= ",$item";
				}

				$failed = count( $failedImports );
				session()->flash( 'error', "$failed did not import. [$error] " );
			}

			$number = $count - count( $failedImports );
			$request->session()->flash( 'success', "$count records in total. $number uploaded." );

			return redirect()->back();

		} catch ( \Exception $exception ){

			return $exception;

			session()->flash('error','Something went wrong. Please try again.');
			return redirect()->back();
		}

	}

	public function editStaff($stid) {
		$staff = staff::find($stid);
		$designations = designation::all();
		return view('staff.edit',[
			'staff' => $staff,
			'designations' => $designations
		]);
	}

	public function postEditStaff( Request $request, $stid ) {

		$password = $request->input('password');

		$did = $request->input('did');
		$designation = designation::find($did);

		$stid = $request->input('stid');
		$staff = staff::find($stid);
		$staff->title = $request->input('title');
		$staff->fname = $request->input('fname');
		$staff->sname = $request->input('sname');

		$staff->bio = $request->input('bio');
		$staff->dob  = $request->input('dob');
		$staff->gender = $request->input('gender');
		$staff->did = $request->input('did');
		$staff->phone = $request->input('phone');
		$staff->email = strtolower($request->input('email'));
		$staff->image = $request->input('image');
		$staff->stateOfOrigin = $request->input('stateOfOrigin');
		$staff->nationality = $request->input('nationality');
		$staff->residentialAddress = $request->input('residentialAddress');
		$staff->category = $designation->category;

		if(!empty($password)) $staff->password = $password;
		$staff->save();

		session()->flash('success','Staff Edited');
		return redirect()->back();

	}

	public function postAddStaff(Request $request) {
		$did = $request->input('did');
		$designation = designation::find($did);

		$staff = new staff();
		$staff->title = $request->input('title');
		$staff->fname = $request->input('fname');
		$staff->sname = $request->input('sname');

		$staff->bio = $request->input('bio');
		$staff->dob  = $request->input('dob');
		$staff->gender = $request->input('gender');
		$staff->did = $request->input('did');
		$staff->phone = $request->input('phone');
		$staff->email = strtolower($request->input('email'));
		$staff->image = $request->input('image');
		$staff->stateOfOrigin = $request->input('stateOfOrigin');
		$staff->nationality = $request->input('nationality');
		$staff->residentialAddress = $request->input('residentialAddress');
		$staff->category = $designation->category;
		$staff->password = bcrypt("regent123");    

		$status = $staff->save();

		if ($status){
			session()->flash('success','Staff Added');
		}else{
			session()->flash('error','Something Went Wrong');
		}

		return redirect()->back();

	}

	public function manageStaff() {
		if(Input::has('term')){
			$term = Input::get('term');
			$staff = staff::where('fname', 'like',"%$term%")->orWhere('sname', 'like',"%$term%")->orderBy('created_at','desc')->paginate(30);
		} else
			$staff = staff::orderBy('created_at','desc')->paginate(30);

		$from = collect($staff)->get('from');
		return view('staff.manage',[
			'staffs' => $staff,
			'from' => $from

		]);
	}

	public function staffDetail($stid) {
		$staff = staff::findorfail($stid);
		return view('staff.detail',[
			'staff'=> $staff
		]);

	}


	public function studentEmailList() {
		$students = student::all()->sortBy('cid');
		$count = 1;
		echo "<style>table,tr,td{
  border: 1px solid black;
  border-collapse: collapse;
}

td{
  padding: 5px;
}</style>";
		echo "<table><tr><th>S/N</th><th>Name</th><th>Email</th><th>Password</th><th>Class</th></tr>";
		foreach($students as $item){

			echo "<tr><td>$count</td> ". "<td>  $item->fname  $item->sname </td>" . "<td>$item->email</td><td>regent123</td>" ;
			if(isset($item->Class)) echo  "<td>" . $item->Class->name . "</td>";
			echo "</tr>";
			$count++;
		}
		echo "</table>";


	}

	public function parentEmailList() {
		$students = student::all();
		$count = 1;
		echo "<style>table,tr,td{
  border: 1px solid black;
  border-collapse: collapse;
}

td{
  padding: 5px;
}</style>";
		echo "<table><tr><th>S/N</th><th>Name</th><th>Email</th><th>Password</th><th>Class</th></tr>";
		foreach($students as $item){

			if(!empty($item->fname)) {
				echo "<tr><td>$count</td> " . "<td>  $item->fname  $item->sname </td><td>$item->email</td><td>hendon123</td>";
				if ( isset( $item->Class ) ) {
					echo "<td>" . $item->Class->name . "</td>";
				}
				echo "</tr>";
				echo "<tr><td colspan='1'></td><th>Parent Name</th><th>Parent Phone</th><th>Parent Email</th><th>Parent Password</th></tr>";

				foreach($item->Parents as $parent) {
					echo "<tr><td colspan='1'>Parent</td><td>$parent->fname $parent->sname</td><td>$parent->phone</td><td>$parent->email</td><td>regent123</td></tr>";
				}
				echo "<tr><td colspan='6' style='height: 60px;'></td>&nbsp;</tr>";
			}
			$count++;
		}
		echo "</table>";


	}

	public function deleteStudent( $sid ) {
		$student = student::find($sid);
		$student->delete();
		session()->flash('success','Student Deleted');
		return redirect('students');
	}


	public function students(Request $request) {

//		$students = student::all();
//		foreach($students as $item){
//			echo $item->email . "<br>";
//		}
//		return;

		if(Input::has('name')){
			$term = Input::get('name');
			$student = student::where('sname' ,'like',"%$term%")->orWhere('fname' ,'like',"%$term%")->orderBy('created_at','desc')->paginate(30);
		} else
			$student = student::orderBy('created_at','desc')->paginate(30);
		$from =  collect($student)->get('from');
		return view('students.manage',[
			'students'=> $student,
			'from' => $from
		]);
	}


	public function studentDetail($sid) {
		$student = student::find($sid);
		$health = sickbay::where('sid',$sid)->orderBy('created_at','desc')->get();

		$subids = array();
		$subjectStudents = subjectStudent::where('sid',$student->sid)->get();

		foreach($subjectStudents as $subjectStudent){
			array_push($subids,$subjectStudent->subid);
		}

		$subjects = subjects::whereNotIn('subid',$subids)->get();


		return view('students.detail',[
			'student' => $student,
			'healthRecords' => $health,
			'subjects' => $subjects
		]);

	}

	public function editStudent($sid) {
		$student = student::find($sid);
		$focuses = focus::all();
		$classes = classes::all();

		return view('students.edit',[
			'student' => $student,
			'focuses' => $focuses,
			'classes' => $classes
		]);

	}

	public function postEditStudent(Request $request, $sid) {


		if($request->hasFile('image')){
			$filename = $request->file('image')->getClientOriginalName();
			$request->file('image')->move('uploads',$filename);
			$imageUrl = url('uploads/' . $filename);
		}

		$student = student::find($sid);
//		$student->fname = $request->input('fname');
//		$student->sname = $request->input('sname');
		$student->gender = $request->input('gender');
		$student->dob = $request->input('dob');
		$student->address = $request->input('address');
		$student->email = strtolower($request->input('email'));
		$student->phone = $request->input('phone');
		$student->nationality = $request->input('nationality');
		$student->religion = $request->input('religion');
		$student->studentid = $request->input('studentid');
		$student->session = $request->input('session');
		$student->fid = $request->input('fid');
		$student->cid = $request->input('cid');

		if($request->hasFile('image')) $student->image = $imageUrl;

		if(!empty($request->input('password')))
		$student->password = bcrypt($request->input('password'));
		$student->save();


		$focusid = $request->input('fid');
		$focusSubjects = focusSubject::where('fid',$focusid)->get();

		foreach ($focusSubjects as $subject):

			if(subjectStudent::where('sid',$sid)->where('subid',$subject->subid)->count() <= 0){
				$subjectStudent =  new subjectStudent();
				$subjectStudent->sid = $sid;
				$subjectStudent->subid = $subject->subid;
				$subjectStudent->save();
			}

		endforeach;

		session()->flash('success','Updated Successfully');
		return redirect()->back();
	}

	public function addSubjectTeacher( Request $request ) {
		$stid = $request->input('stid');
		$subid = $request->input('subid');
		$cids = $request->input('cids');

		if(empty($cids)) {
			session()->flash('error','You must select a class');
			return redirect()->back();
		}

		if(subjectTeacher::where('stid',$stid)->where('subid',$subid)->count() > 0 ) {
			session()->flash('error','Teacher is already assigned to this subject');
			return redirect()->back();
		}
		$subjectTeacher = new subjectTeacher();
		$subjectTeacher->stid = $stid;
		$subjectTeacher->subid = $subid;
		$subjectTeacher->cids = json_encode($cids);
		$subjectTeacher->save();

		session()->flash('success','Supervisor Added');
		return redirect()->back();
	}


	public function removeSubjectTeacher( Request $request ) {
		$stid = $request->input('stid');
		$subid = $request->input('subid');
		if(subjectTeacher::where('stid',$stid)->where('subid',$subid)->count() <= 0 ) {
			session()->flash('error','Teacher is not assigned to subject');
			return redirect()->back();
		}
		$subjectTeacher = subjectTeacher::where('stid',$stid)->where('subid',$subid)->first();
		$subjectTeacher->delete();

		session()->flash('success','Supervisor Un-assigned');
		return redirect()->back();
	}

	public function postAssignStudentSubject( Request $request ) {
		$sid = $request->input('sid');
		$subids = $request->input('subids');

		foreach($subids as $subid){
			$subjectStudent = new subjectStudent();
			$subjectStudent->subid = $subid;
			$subjectStudent->sid = $sid;
			$subjectStudent->save();
		}

		session()->flash('success','Projects Added.');

		return redirect()->back();


	}

	public function parents(  ) {

//		$parents = parents::all();
//		foreach($parents as $parent){
//			echo $parent->email . "<br>";
//		}
//		return;
//
		if(Input::has('term')){

			$term = Input::get('term');
			$parents = parents::where('fname', 'like',"%$term%")
			                  ->orWhere('sname', 'like',"%$term%")
			                  ->orWhere('phone', 'like',"%$term%")
			                  ->orWhere('email', 'like',"%$term%")->paginate(30);
		} else
		$parents = parents::paginate(30);

		$from = collect($parents)->get('from');

		return view('parents.manage',[
			'parents' => $parents,
			'from'  => $from
		]);

	}


	public function parentDetails($pid) {
		$parent = parents::find($pid);
		return view('parents.detail',[
			'parent'=> $parent
		]);
	}



	public function editParent($pid) {
		$parent = parents::find($pid);
		return view('parents.edit',[
			'parent' => $parent
		]);

	}

	public function  postEditParent(Request $request, $pid) {


		if($request->hasFile('image')){
			$filename = $request->file('image')->getClientOriginalName();
			$request->file('image')->move('uploads',$filename);
			$imageUrl = url('uploads/' . $filename);
		}

		$student = parents::find($pid);
		$student->update(request()->all());
		$student->save();

		session()->flash('success','Updated Successfully');
		return redirect()->back();
	}

	public function deleteParent( $pid ) {
		$student = parents::find($pid);
		$student->delete();
		session()->flash('success','Parent Deleted');
		return redirect()->back();
	}

//	subjects

	public function addSubject(  ) {
		$staffs = staff::all();
		$classes = classes::all();
		return view('subject.add',[
			'staffs' => $staffs,
			'classes' => $classes
		]);
	}

	public function postAddSubject(Request $request  ) {

		$cids = $request->input('cids');

		$cids = json_encode($cids);

		$subject  = new  subjects();
		$subject->name = $request->input('name');
		$subject->stid = $request->input('stid');
		$subject->cids = $cids;
		$status = $subject->save();

		if ($status){
			session()->flash('success','Project Added');
		}else{
			session()->flash('error','Something Went Wrong');
		}

		return redirect()->back();
	}

	public function getAssignProjectToSupervisor($subid){

		$subjects = subjects::all();
		$staffs = staff::all();
		$classes = classes::all();
		$subject = subjects::findorfail($subid);
		return view('subject.edit',[
			'subject' => $subject,
			'subjects' =>$subjects,
			'staffs' => $staffs,
			'classes' => $classes
		]);
	}

	public function postAssignProjectToSupervisor(Request $request, $subid){
		$cids = $request->input('cids');
		
		$cids = json_encode($cids);
		
		$subject = subjects::findorfail($subid);
		$subject->name = $request->input('name');
		$subject->stid = $request->input('stid');
		$subject->cids = $cids;
		$status = $subject->save();

		if ($status){
			session()->flash('success','Project Supervisor Assigned Successfully');
		}else{
			session()->flash('error','Something Went Wrong');
		}

		return redirect()->back();

	}


	public function postUploadSubject(Request $request) {


		if ( $request->hasFile( 'file' ) ) {
			$fileName = Carbon::now()->timestamp . $request->file( 'file' )->getClientOriginalName();
			$request->file( 'file' )->move( 'uploads/staff', $fileName );
			$fileUrl = url( 'uploads/staff/' . $fileName );
		} else {
			session()->flash( 'error', 'Please attach a file.' );

			return redirect()->back();
		}


		try {


			$downloadUrl = url( "/uploads/subjects/" . $fileName );

			/* Identify file, create reader and load file  */
			$inputFileType = PHPExcel_IOFactory::identify( getcwd() . "/" . "uploads/subjects/" . $fileName );
			$objReader     = PHPExcel_IOFactory::createReader( $inputFileType );
			$objPHPExcel   = PHPExcel_IOFactory::load( getcwd() . "/" . "uploads/subjects/" . $fileName );

			$sheet         = $objPHPExcel->getActiveSheet();
			$highestRow    = $sheet->getHighestRow();
			$highestColumn = $sheet->getHighestColumn();
			$time_pre      = microtime( true );

			//  Read a row of data into an array
			$rowData = $sheet->rangeToArray( 'A2:' . $highestColumn . $highestRow,
				null, false, false );

			$count = 0;

			$failedImports = array();

			DB::beginTransaction();
			// add results to data base from file
			foreach ( $rowData as $cell ) {
				$count ++;
				try {


					$subject  = new  subjects();
					$subject->name = $cell[0];
					$subject->stid = $cell[1];
					$subject->cids = $cell[2];
					$subject->save();



				} catch ( \Exception $e ) {
//					return $e->getMessage();
					array_push( $failedImports, $count );
				}

			}

			DB::commit();
			if ( count( $failedImports ) > 0 ) {
				$error = "";
				foreach ( $failedImports as $item ) {
					$error .= ",$item";
				}

				$failed = count( $failedImports );
				session()->flash( 'error', "$failed did not import. [$error] " );
			}

			$number = $count - count( $failedImports );
			$request->session()->flash( 'success', "$count records in total. $number uploaded." );

			return redirect()->back();

		} catch ( \Exception $exception ){

			return $exception;

			session()->flash('error','Something went wrong. Please try again.');
			return redirect()->back();
		}
	}

	public function subjects() {
		if(Input::has('term')){

			$term = Input::get('term');
			$subjects = subjects::where('name', 'like',"%$term%")->paginate(30);
		} else
			$staff = staff::all();
			$subjects  = subjects::all();
		return view('subject.manage',[
			'subjects' => $subjects,
			'staff' => $staff
		]);
	}

	public function changeSubjectTeacher( Request $request ) {
		$subid = $request->input('subid');
		$stid = $request->input('stid');

		$subject = subjects::find($subid);
		$subject->stid = $stid;
		$subject->save();

		session()->flash('success','Supervisor Changed');
		return redirect()->back();
	}

	public function postAddSubjectStudent( Request $request ) {
		 $subjectStudent = new subjectStudent();
		 $subjectStudent->subid = $request->input('subid');
		 $subjectStudent->sid = $request->input('sid');
		 $subjectStudent->save();
		 session()->flash('success','Student Added.');
		 return redirect()->back();
	}

	public function enrollAllStudents() {
		$students = student::all();

		$enrollmentCount = 0;
		foreach($students as $student){

			$subjects = subjects::all();

			foreach($subjects as $subject){
				$cids = json_decode($subject->cids);
				$subjectTeachers = subjectTeacher::where('subid',$subject->subid)->get();
				foreach($subjectTeachers as $subjectTeacher){
					$otherCids = json_decode($subjectTeacher->cids);

					foreach($otherCids as $otherCid){
						array_push($cids,$otherCid);
					}
				}

				foreach($cids as $cid){
					if($student->cid == $cid){
						$enrollmentCount++;
						$subjectStudent = new subjectStudent();
						$subjectStudent->subid = $subject->subid;
						$subjectStudent->sid = $student->sid;
						$subjectStudent->save();
					}
				}
			}

		}

		return $enrollmentCount;
	}

	public function removeSubjectStudent( $ssid ) {
		subjectStudent::destroy($ssid);
		session()->flash('success','Student Removed.');
		return redirect()->back();
	}

	public function removeSubjectStudent2( $subid, $sid ) {
		$subjectStudent = subjectStudent::where('subid',$subid)->where('sid',$sid)->first();
		$subjectStudent->delete();

		session()->flash('success','Project Removed');
		return redirect()->back();
	}

	public function subjectDetails($subid) {

		$subject = subjects::findorfail($subid);
		$subjectTeachers = $subject->Teachers;

		$cids = json_decode($subject->cids);
		$moreCids = [];

		$classes = classes::whereNotIn('cid',$cids)->get();

		foreach($subjectTeachers as $teacher){
			$st = \App\subjectTeacher::where('stid',$teacher->stid)->where('subid',$subject->subid)->first();

			$moreCids = json_decode($st->cids);
			if(count($moreCids) > 0)
			$classes = classes::whereNotIn('cid',$cids)->whereNotIn('cid',$moreCids)->get();

		}

		$students = student::all();

		$teachers = staff::where('category','Academic')->where('stid','<>',$subject->Teacher->stid)->whereNotIn('stid',$subjectTeachers)->get();

		return view('subject.details',[
			'subject' => $subject,
			'teachers' => $teachers,
			'classes' => $classes,
			'students' => $students
		]);

	}

	public function postAddTest( Request $request ) {
		$subjects = subjects::all();


		$startTIme = $request->input('startTime');
		$endTime   = $request->input('endTime');
		$deadline   = $request->input('deadline');

		$startTIme = Carbon::createFromFormat("m/d/Y H:i",$startTIme);
		$endTime   = Carbon::createFromFormat("m/d/Y H:i",$endTime);
		$deadline   = Carbon::createFromFormat("m/d/Y H:i",$deadline);

		foreach($subjects as $subject){

			$test = new test();
			$test->name = $request->input('title');
			$test->description = $request->input('description');
			$test->duration = $request->input('duration');
			$test->startTime = $startTIme;
			$test->endTime = $endTime;
			$test->stid = $subject->Teacher->stid;
			$test->subid = $subject->subid;
			$test->deadline = $deadline;
			$test->save();

		}


		session()->flash('success','Test Added.');
		return redirect()->back();

	}

	public function addTest() {
		return view('tests.add');
	}

	public function tests() {
//		$tests = test::where('deadline','<>',null)->get();

		if(Input::has('term')){
			$term  = Input::get('term');
			$tests = test::where('name' ,'like',"%$term%")->orWhere('testid' ,'like',"%$term%")->orderBy('created_at','desc')->paginate(30);
		} else
			$tests = test::orderBy('created_at','asc')->paginate(30);

		$allTests = test::all();
		$from = collect($tests)->get('from');
		return view('tests.manage',[
			'tests' => $tests,
			'allTests' => $allTests,
			'from' => $from
		]);
	}

	public function testDetails( $testid ) {
		$test = test::find($testid);
		return view('tests.details',[
			'test' => $test
		]);
	}

	//designations

	public function addDesignation(  ) {
		return view('designation.add');
	}


	public function editDesignation( $did ) {
		$designation = designation::find($did);

		return view('designation.edit',[
			'designation' => $designation
		]);
	}

	public function postEditDesignation( Request $request,$did) {

		 $designation = designation::find($did);
		 $designation->name = $request->input('name');
		 $designation->category = $request->input('category');
		 $designation->save();

		 $staff = staff::where('did',$designation->did)->get();

		 foreach($staff as $item){
		 	$thisStaff = staff::find($item->stid);
		 	$thisStaff->category = $designation->category;
		 	$thisStaff->save();
		 }
		 session()->flash('success','Designation Edited');
		 return redirect()->back();

	}

	public function postDesignation(Request $request) {
		$designation = new designation();
		$designation->name = $request->input('name');
		$designation->category = $request->input('category');

		$status = $designation->save();
		if ($status){
			session()->flash('success','Designation Added.');
		}else{
			session()->flash('error', 'Sorry, Something Went Wrong');
		}
		return redirect()->back();

	}

	public function designations() {
		$designation = designation::all();
		return view('designation.manage',[
			'designations' => $designation
		]);
	}


	public function deleteDesignation( $did ) {

		$designation = designation::find($did);

		if(count($designation->Staff) > 0) {
			session()->flash('error','Please remove all staff from designation before deleting.');
			return redirect()->back();
		}
		$designation = designation::destroy($did);
		if ($designation){
			session()->flash('success','Designation Deleted');
		}else{
			session()->flash('error', 'Something Went Wrong');
		}
		return redirect()->back();
	}





}
