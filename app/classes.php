<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class classes extends Model
{
    protected $primaryKey = 'cid';
    protected $table = 'classes';
    protected $guarded = [];


	public function Teacher() {
		return $this->hasOne(staff::class,'stid','stid');
    }


	public function Students() {
		return $this->hasMany(student::class,'cid','cid');
    }
}
