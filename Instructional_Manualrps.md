<!--
Regent Project Managment System

Designed by: HARRY AGBOMADZI


ID.NO.: 00860115    
Department: Information System Science of Regent University College of Science and Technology, Ghana

Kindly Follow guideline below to run the software. 
-->

## Regent Project Management System

**Requirements**
- Xamp or Wamp or Mamp is required for to running PhpMyAdmin after installing this:



Note: cmder will be refered as console

##Mac Os, Ubuntu and windows users continue here:

Create a database locally named `rpsdb` utf8_general_ci

- it is recommmend to download and install composer from: https://getcomposer.com and also global installtion of laravel from https://laravel.com

- Note: Ensure that the php version is >= 7.0



Download composer https://getcomposer.org/download/
Pull Laravel/php project from git provider.
Rename .env.example file to .envinside your project root and fill the database information. (windows wont let you do it, so you have to open your console cd your project root directory and run mv .env.example .env )

Open the console and cd your project root directory
Run composer install or php composer.phar install
Run php artisan key:generate

Import the database `rpsdb` in the project folder `rps`

Users Roles:
======================================================== 

Committee: 
email: harrya@regent.edu.gh
password: 123456

Student:
Email: j.rimamchirika@regent.edu.gh
password: 123456

Supervisor:
email:attahopuku@regent.edu.gh
password: 123456





* To make use of a fresh database without any record, please - `Run php artisan migrate`2   r
 and `Run php artisan db:seed to run seeders`, if any.

- `Run php artisan serve`

##### You can now access your project at `localhost:8000`

If for some reason your project stop working do these:
Run: `composer install`
Run: `php artisan key:generate`
Run: `php artisan migrate`




