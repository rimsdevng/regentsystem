<?php use Carbon\Carbon; use Illuminate\Support\Facades\Input; ?>
@extends('layouts.admin')
@section('content')
    <div class="content-wrap">
        <div class="main">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-8 p-r-0 title-margin-right">
                        <div class="page-header">
                            <div class="page-title">

                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                    <div class="col-lg-4 p-l-0 title-margin-left">
                        <div class="page-header">
                            <div class="page-title">
                                <ol class="breadcrumb text-right">
                                    <li><a href="#">Dashboard</a></li>
                                    <li class="active">Tests</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                </div>


                @include('notification')


                <div class="row">
                    <div class="col-md-12">
                        <a href="{{url('tests')}}" class="btn btn-secondary padding-overlay pull-left"> Back </a>
                        <a href="{{url('admin/add-test')}}" class="btn btn-success padding-overlay pull-right"> Add Test </a>
                    </div>
                </div>

                <div id="main-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card alert">
                                <div class="card-header pr">
                                    <h4>Manage Tests </h4>
                                    <div class="search-action">
                                        <div class="search-type dib">
                                            <form>
                                                <input class="form-control input-rounded" name="term" value="{{Input::get('term')}}" placeholder="Search" type="text">
                                            </form>
                                        </div>
                                    </div>
                                    <br>
                                    <span>{{count($allTests)}} in total</span>

                                </div>
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table class="table student-data-table m-t-20">
                                            <thead>
                                            <tr>
                                                <th>S/N</th>
                                                <th>Name</th>
                                                <th>Subject</th>
                                                <th>No Of Questions</th>
                                                <th>Deadline</th>
                                                <th>Class(es)</th>
                                                <th></th>
                                            </tr>
                                            </thead>
                                            <tbody>

                                            @if(count($tests)>0)
									            <?php $count = $from; ?>
                                                @foreach($tests as $test)
                                                    <tr>

                                                        <td>
                                                            <?php echo $count; ?>
                                                        </td>
                                                        <td>
                                                            {{$test->name}}
                                                        </td>
                                                        <td>
                                                            {{$test->Subject->name}}
                                                        </td>
                                                        <td>
                                                            {{count($test->Questions)}}
                                                        </td>
                                                        <td>

                                                            @if(isset($test->deadline) && Carbon::now()->diffInDays($test->deadline) <= 7)
                                                                <span class="text-danger">{{$test->deadline->toDayDateTimeString()}}</span>
                                                            @endif
                                                            @if(isset($test->deadline) && Carbon::now()->diffInDays($test->deadline) > 7 && Carbon::now()->diffInDays($test->deadline) < 14)
                                                                <span class="text-warning">{{$test->deadline->toDayDateTimeString()}}</span>
                                                            @endif
                                                            @if(isset($test->deadline) && Carbon::now()->diffInDays($test->deadline) >= 14)
                                                                <span class="text-success">{{$test->deadline->toDayDateTimeString()}}</span>
                                                            @endif

                                                            @if(!isset($test->deadline))
                                                                    No Deadline
                                                            @endif


                                                        </td>
                                                        <td>
                                                            @foreach($test->Classes as $item)
                                                                {{$item->name}},
                                                            @endforeach
                                                        </td>
                                                        <td>
                                                            <span><a href="{{url('admin/test/' . $test->testid)}}"><i class="ti-eye color-default"></i></a> </span>
                                                        </td>
                                                    </tr>
										            <?php $count ++; ?>
                                                @endforeach
                                            @else

                                                <tr>
                                                    <td colspan="4" style="color: silver; text-align: center; margin-top: 30px;"> There are no tests </td>
                                                </tr>

                                            @endif




                                            </tbody>
                                        </table>

                                        <div class="pull-right">
                                            {{$tests->links()}}
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /# column -->


                    </div>
                    <!-- /# row -->

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="footer">
                                <p>This dashboard was generated on <span id="date-time"></span> <a href="#" class="page-refresh">Refresh Dashboard</a></p>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- /# main content -->
            </div>
            <!-- /# container-fluid -->
        </div>
        <!-- /# main -->
    </div>


@endsection







