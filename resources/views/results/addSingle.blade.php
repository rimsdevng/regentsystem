@extends('layouts.admin')
@section('content')
    <div class="content-wrap">
        <div class="main">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-8 p-r-0 title-margin-right">
                        <div class="page-header">
                            <div class="page-title">

                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                    <div class="col-lg-4 p-l-0 title-margin-left">
                        <div class="page-header">
                            <div class="page-title">
                                <ol class="breadcrumb text-right">
                                    <li><a href="#">Dashboard</a></li>
                                    <li class="active">Results</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                </div>
                <!-- /# row -->
                <div class="main-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card alert">
                                <div class="card-header">
                                    <h4>Select a class and subject</h4>
                                    <div class="card-header-right-icon">
                                        <ul>
                                            <li class="card-close" data-dismiss="alert"><i class="ti-close"></i></li>
                                            <li class="doc-link"><a href="#"><i class="ti-link"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="menu-upload-form">
                                        <form class="form-horizontal" method="post" action="{{url('add-result/subject')}}">
                                           {{csrf_field()}}

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Class</label>
                                                <div class="col-sm-10">
                                                    <select name="cid" id="" class="form-control" required>
                                                        <option selected disabled>Pick a class</option>
                                                        @foreach($classes as $class)
                                                            <option value="{{$class->cid}}">{{$class->name}}</option>
                                                        @endforeach
                                                    </select>
                                                    {{--<input type="text" class="form-control" placeholder="Type your menu Title">--}}
                                                </div>
                                            </div>




                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Subject</label>
                                                <div class="col-sm-10">
                                                    <select name="subid" id="" class="form-control" required>
                                                        <option selected disabled>Pick a Subject</option>
                                                        @foreach($subjects as $subject)
                                                            <option value="{{$subject->subid}}">{{$subject->name}}</option>
                                                        @endforeach
                                                    </select>
                                                    {{--<input type="text" class="form-control" placeholder="Type your menu Title">--}}
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <div class="col-sm-offset-2 col-sm-10">
                                                    <button type="submit" class="btn btn-lg btn-primary">Next</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!-- /# card -->
                        </div>
                        <!-- /# column -->
                    </div>
                    <!-- /# row -->
                </div>
                <!-- /# main content -->
            </div>
            <!-- /# container-fluid -->
        </div>
        <!-- /# main -->
    </div>


@endsection



































