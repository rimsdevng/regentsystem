@extends('dashboard.staff.layouts.app')
@section('content')

    <div class="content-wrap">
        <div class="main">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-8 p-r-0 title-margin-right">
                        <div class="page-header">
                            <div class="page-title">
                                <h1>Dashboard</h1>
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                    <div class="col-lg-4 p-l-0 title-margin-left">
                        <div class="page-header">
                            <div class="page-title">
                                <ol class="breadcrumb text-right">
                                    <li><a href="#">Dashboard</a></li>
                                    <li >Admissions</li>
                                    <li class="active">Applications</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                </div>
                <!-- /# row -->
                <div id="main-content">
                    <!-- /# column -->

                    <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <div class="card alert">
                                <div class="card-header">
                                    <h4>Default Tab</h4>
                                </div>
                                <div class="card-body">
                                    <div class="default-tab">
                                        <ul class="nav nav-tabs" role="tablist">
                                            <li role="presentation" class="active"><a href="#home1" aria-controls="home" role="tab" data-toggle="tab">Student Data</a></li>
                                            <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Health Record</a></li>
                                            <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Add New Record</a></li>
                                            {{--<li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab"> Finance</a></li>--}}
                                        </ul>
                                        <div class="tab-content">
                                            <div role="tabpanel" class="tab-pane active" id="home1">
                                                {{--<h3>*Note</h3>--}}

                                                {{--first card--}}
                                                <div class="card-body">
                                                    <div class="user-profile m-t-15">
                                                        <div class="row">
                                                            <div class="col-lg-4">
                                                                <div class="user-photo m-b-30">
                                                                    <img class="img-responsive" src="{{$student->image}}" alt="Photo Space" />
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-8">
                                                                <div class="user-profile-name dib">{{$student->fname}} {{$student->sname}}</div>
                                                                <div class="useful-icon dib pull-right">
                                                                    <span><a href="{{url('/student/'.$student->sid.'/edit')}}" class="btn btn-danger"><i class="ti-pencil-alt"></i></a> </span>
                                                                    <span><a href="#"><i class="ti-printer"></i></a></span>
                                                                    {{--<span><a href="#"><i class="ti-download"></i></a></span>--}}
                                                                    {{--<span><a href="#"><i class="ti-share"></i></a></span>--}}
                                                                </div>
                                                                <div class="custom-tab user-profile-tab">
                                                                    <ul class="nav nav-tabs" role="tablist">
                                                                        <li role="presentation" class="active"><a href="#1" aria-controls="1" role="tab" data-toggle="tab">About</a></li>
                                                                    </ul>
                                                                    <div class="tab-content">
                                                                        <div role="tabpanel" class="tab-pane active" id="1">
                                                                            <div class="contact-information">
                                                                                <div class="phone-content">
                                                                                    <span class="contact-title">Student ID:</span>
                                                                                    {{--                                                                                    <span class="phone-number">{{$student->preferredName}}</span>--}}
                                                                                </div>
                                                                                <div class="phone-content">
                                                                                    <span class="contact-title"> Focus:</span>
                                                                                    <span class="phone-number">{{$student->fid}}</span>
                                                                                </div>
                                                                                <div class="website-content">
                                                                                    <span class="contact-title">Class:</span>
                                                                                    <span class="contact-website">{{$student->cid}}</span>
                                                                                </div>

                                                                                <div class="gender-content">
                                                                                    <span class="contact-title">Gender:</span>
                                                                                    <span class="gender">{{$student->gender}}</span>
                                                                                </div>
                                                                                <div class="phone-content">
                                                                                    <span class="contact-title">Phone</span>
                                                                                    {{--                                                                                    <span class="phone-number">{{$application->homePhone}}</span>--}}
                                                                                </div>


                                                                                <div class="birthday-content">
                                                                                    <span class="contact-title">Date of Birth:</span>
                                                                                    <span class="birth-date">{{$student->dob}} </span>
                                                                                </div>

                                                                                <div class="phone-content">
                                                                                    <span class="contact-title">Religion:</span>
                                                                                    {{--                                                                                    <span class="phone-number">{{$application->religionDenomination}}</span>--}}
                                                                                </div>
                                                                                <div class="email-content">
                                                                                    <span class="contact-title">Nationality:</span>
                                                                                    <span class="contact-email">{{$student->nationality}}</span>
                                                                                </div>
                                                                                <div class="email-content">
                                                                                    <span class="contact-title">First Language:</span>
                                                                                    {{--                                                                                    <span class="contact-email">{{$application->firstLanguage}}</span>--}}
                                                                                </div>

                                                                                <div class="address-content">
                                                                                    <span class="contact-title">Home Address:</span>
                                                                                    <span class="mail-address">{{$student->address}}</span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                {{--end of first card--}}

                                                {{--second --}}

                                                <hr>



                                                {{--end of second--}}

                                            </div>





                                            <div role="tabpanel" class="tab-pane" id="profile">
                                                {{--<p> This table Holds the Health status of the student and reports of any treatment administered at the school </p>--}}
                                                        @if(count($records)>0)

                                                    @foreach($records as $record)
                                                        <div class="row">
                                                            <div class="col-lg-12">
                                                                <div class="card alert">
                                                                    <div class="card-header">
                                                                        <h4>{{title_case($record->diagnosis)}} </h4>

                                                                    </div>
                                                                    <div class="card-body">
                                                                        <div class="user-profile m-t-15">
                                                                            <div class="row">
                                                                                {{--<div class="col-lg-4">--}}
                                                                                    {{--<div class="user-photo m-b-30">--}}
                                                                                        {{--<img class="img-responsive" src="assets/images/user-profile.jpg" alt="Photo Space" />--}}
                                                                                    {{--</div>--}}
                                                                                {{--</div>--}}
                                                                                <div class="col-lg-10">

                                                                                    <div class="custom-tab user-profile-tab">
                                                                                        <ul class="nav nav-tabs" role="tablist">
                                                                                            <li role="presentation" class="active"><a href="#1" aria-controls="1" role="tab" data-toggle="tab">About</a></li>
                                                                                        </ul>
                                                                                        <div class="tab-content">
                                                                                            <div role="tabpanel" class="tab-pane active" id="1">
                                                                                                <div class="contact-information">
                                                                                                    <div class="phone-content">
                                                                                                        <span class="contact-title">Treatment:</span>
                                                                                                            {{$record->treatment}}
                                                                                                    </div>
                                                                                                    <div class="phone-content">
                                                                                                        <span class="contact-title"> Details:</span>
                                                                                                        {{--<span class="phone-number">{{$parent->phone}}</span>--}}
                                                                                                        {{$record->details}}
                                                                                                    </div>

                                                                                                    <div class="row pull-right">
                                                                                                        <a  href="{{url('sickbay-visit/'.$record->sbid)}}" class="btn btn-info">Details</a>
                                                                                                    </div>



                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>
                                                        <hr>
                                                    @endforeach

                                                            @else

                                                            <h4 style="color: silver; text-align: center" > Student Has No Health Treatment Record</h4>

                                                @endif

                                            </div>
                                            <div role="tabpanel" class="tab-pane" id="messages">

                                                <div class="card-body">
                                                    <div class="menu-upload-form">
                                                        <form class="form-horizontal" method="post" action="{{url('post-sickbay-treatment/'.$student->sid)}}">
                                                            {{csrf_field()}}
                                                            <input type="hidden" value="{{$student->sid}}">

                                                            <div class="form-group">
                                                                <label class="col-sm-2 control-label">Diagnosis</label>
                                                                <div class="col-sm-10">
                                                                    <textarea type="text" class="form-control" name="diagnosis" rows="2" placeholder="Enter The Administered Treatment" required></textarea>
                                                                </div>
                                                            </div>



                                                            <div class="form-group">
                                                                <label class="col-sm-2 control-label">Treatment</label>
                                                                <div class="col-sm-10">
                                                                    <textarea type="text" class="form-control" name="treatment" rows="3" placeholder="Enter The Administered Treatment" required></textarea>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="col-sm-2 control-label">Details</label>
                                                                <div class="col-sm-10">
                                                                    <textarea type="text" class="form-control" name="details" rows="5" placeholder="Enter The Details Treatment And Other Important Information" required></textarea>
                                                                </div>
                                                            </div>





                                                            <div class="form-group">
                                                                <div class="col-sm-offset-2 col-sm-10">
                                                                    <button type="submit" class="btn btn-lg btn-primary">Send</button>
                                                                </div>
                                                            </div>


                                                        </form>
                                                    </div>
                                                </div>


                                            </div>
                                            <div role="tabpanel" class="tab-pane" id="settings">
                                                <p>This tab holds the financial record of the students payments and pending payments.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /# column -->

                        <!-- /# column -->
                    </div>
                    <!-- /# row -->

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="footer">
                                <p>This dashboard was generated on <span id="date-time"></span> <a href="#" class="page-refresh">Refresh Dashboard</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection









