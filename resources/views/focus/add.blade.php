@extends('layouts.admin')
@section('content')




    <div class="content-wrap">
        <div class="main">
            {{--@include('notification')--}}

            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-8 p-r-0 title-margin-right">
                        <div class="page-header">
                            <div class="page-title">
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                    <div class="col-lg-4 p-l-0 title-margin-left">
                        <div class="page-header">
                            <div class="page-title">
                                <ol class="breadcrumb text-right">
                                    <li><a href="#">Dashboard</a></li>
                                    <li class="active">School</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                </div>
                <!-- /# row -->
                @include('notification')

                <div class="main-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card alert">
                                <div class="card-header">
                                    <h4>Add School Faculty</h4>
                                    <p style=" color: silver"> This Is For Student Grouping eg. FECAS,SBL or General</p>
                                    <div class="card-header-right-icon">
                                        <ul>
                                            {{--<li class="card-close" data-dismiss="alert"><i class="ti-close"></i></li>--}}
                                            {{--<li class="doc-link"><a href="#"><i class="ti-link"></i></a></li>--}}
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="menu-upload-form">
                                        <form class="form-horizontal" method="post" action="{{url('post-focus')}}">
                                            {{csrf_field()}}
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Title</label>
                                                <div class="col-sm-10">
                                                    <input type="text" class="form-control" name="title" placeholder="Enter a Focus Name" required>
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Project</label>
                                                <div class="col-sm-10">
                                                    @foreach($subjects as $subject)
                                                    <input type="checkbox" value="{{$subject->subid}}" name="subid[]">
                                                    <label for="checkbox3">
                                                        {{$subject->name}}
                                                    </label>
                                                        @endforeach

                                                </div>
                                            </div>





                                            <div class="form-group">
                                                <div class="col-sm-offset-2 col-sm-10">
                                                    <button type="submit" class="btn btn-lg btn-primary">Send</button>
                                                </div>
                                            </div>


                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!-- /# card -->
                        </div>
                        <!-- /# column -->
                    </div>
                    <!-- /# row -->
                </div>
                <!-- /# main content -->
            </div>
            <!-- /# container-fluid -->
        </div>
        <!-- /# main -->
    </div>





@endsection