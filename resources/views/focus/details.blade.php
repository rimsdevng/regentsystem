@extends('layouts.admin')
@section('content')




    <div class="content-wrap">
        <div class="main">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-8 p-r-0 title-margin-right">
                        <div class="page-header">
                            <div class="page-title">
                                <h1>Dashboard</h1>
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                    <div class="col-lg-4 p-l-0 title-margin-left">
                        <div class="page-header">
                            <div class="page-title">
                                <ol class="breadcrumb text-right">
                                    <li><a href="#">Dashboard</a></li>
                                    <li class="active">Faculty</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                </div>
                <!-- /# row -->
                @include('notification')

                <div id="main-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card alert">
                                <div class="card-body">

                                    <p class="page-title">Faculty - {{$focus->name}}</p>
                                </div>
                            </div>

                            <div class="card alert">
                                <div class="card-body" align="center">

                                    <form class="form-inline" method="post" action="{{url('add-focus-subject')}}">
                                        @csrf
                                        <input type="hidden" name="fid" value="{{$focus->fid}}" >
                                        <div class="form-group">
                                            <label >Add Project</label>
                                            <select class="form-control" name="subid" style="width: 200px;">
                                                @foreach($subjects as $subject)
                                                    <option value="{{$subject->subid}}">{{$subject->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <button class="btn btn-success">Add</button>
                                    </form>

                                </div>
                            </div>


                            <div class="card alert">
                                <div class="card-body">

                                    <div class="table-responsive">
                                            <table class="table student-data-table m-t-20">
                                                <thead>
                                                <tr>
                                                    <th>S/n</th>
                                                    <th>Title</th>
                                                    <th>Subject Teacher</th>
                                                    <th>Created</th>
                                                    <th>Action</th>

                                                </tr>
                                                </thead>
                                                <tbody>

                                                @if( count($focus->Subjects) > 0 )

				                                    <?php $count = 1; ?>

                                                    @foreach($focus->Subjects as $subject)
                                                        <tr>

                                                            <td>
                                                                #<?php echo $count;?>
                                                            </td>
                                                            <td>
                                                                {{$subject->name}}
                                                            </td>
                                                            <td>
                                                                {{$subject->Teacher->fname}}  {{$subject->Teacher->sname}}
                                                            </td>
                                                            <td>
                                                                {{$subject->created_at->diffForHumans()}}
                                                            </td>

                                                            <td>
                                                                <span><a href="{{url('subject/'.$subject->subid.'/detail')}}"><i class="ti-eye color-default"></i></a> </span>
                                                                <span><a href="{{url('remove-focus-subject/' . $focus->fid . '/' . $subject->subid)}}"><i class="ti-trash color-danger"></i> </a></span>
                                                            </td>
                                                        </tr>
					                                    <?php $count ++; ?>
                                                    @endforeach
                                                @else
                                                    <tr>
                                                        <td colspan="5" style="text-align: center">There are no subjects added to this focus</td>
                                                    </tr>
                                                @endif




                                                </tbody>
                                            </table>
                                        </div>
                                </div>
                            </div>
                        </div>
                        <!-- /# column -->

                    </div>
                    <!-- /# row -->

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="footer">
                                <p>This dashboard was generated on <span id="date-time"></span> <a href="#" class="page-refresh">Refresh Dashboard</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection