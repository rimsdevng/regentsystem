<?php use Illuminate\Support\Facades\Input; ?>
@extends('layouts.admin')
@section('content')
    <div class="content-wrap">
        <div class="main">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-8 p-r-0 title-margin-right">
                        <div class="page-header">
                            <div class="page-title">
                                <h1>Dashboard</h1>
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                    <div class="col-lg-4 p-l-0 title-margin-left">
                        <div class="page-header">
                            <div class="page-title">
                                <ol class="breadcrumb text-right">
                                    <li><a href="#">Dashboard</a></li>
                                    <li class="active">Applicants</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                </div>
                <!-- /# row -->
                <div id="main-content">
                    <div class="row">
                        @include('notification')
                        <div class="col-lg-12">
                            <div class="card alert">
                                <div class="card-header pr">

                                    <h4>Add Students to System</h4>
                                    <div class="search-action">
                                        <a class="pull-right badge badge-success" href="{{url('applications/admit/all')}}">Approve All</a>

                                        <div class="search-type dib">
                                            <form>
                                                <input class="form-control input-rounded" value="{{Input::get('name')}}" name="name" placeholder="Search" type="text">
                                            </form>

                                        </div>
                                    </div><br>
                                    <span>{{count($allApplications)}} in total, {{count($pendingApplications)}} pending</span>
                                </div>
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table class="table table-hover student-data-table m-t-20">
                                            <thead>
                                            <tr>
                                                <th>S/N</th>
                                                <th>Names</th>
                                                <th>Gender</th>
                                                <th>Session</th>
                                                <th>Current School</th>
                                                <th>Created</th>
                                                <th></th>

                                            </tr>
                                            </thead>
                                            <tbody>

                                            @if(count($applications)>0)

												<?php $count = $from; ?>

                                                @foreach($applications as $application)
                                                    <tr>
                                                        {{--<td><label><input type="checkbox" value=""></label>#2901</td>--}}
                                                        {{--<td>--}}
                                                        {{--<div class="round-img">--}}
                                                        {{--<a href="#"><img src="assets/images/avatar/1.jpg" alt=""></a>--}}
                                                        {{--</div>--}}
                                                        {{--</td>--}}
                                                        <td>
                                                            <?php echo $count;?>
                                                        </td>
                                                        <td>
                                                            {{$application->surname}} {{$application->forenames}}
                                                        </td>
                                                        <td>
                                                            {{$application->gender}}
                                                        </td>
                                                        <td>
                                                            {{$application->proposedYear}}
                                                        </td>
                                                        <td>
                                                            {{$application->currentSchool}}
                                                        </td>

                                                        <td>
                                                            {{$application->created_at->toDayDateTimeString()}}
                                                        </td>

                                                        <td class="actions">
                                                            <span><a href="{{url('manage-applications/detail/'.$application->apid)}}"><i class="ti-eye color-default"></i></a> </span>
                                                            <span><a href="{{url('application/'.$application->apid . '/edit')}}"><i class="ti-pencil-alt color-success"></i></a></span>
                                                            <a href="{{url('make-student/'.$application->apid)}}" class="btn btn-success padding-overlay"> Make Student </a>
                                                            <a href="{{url('application/'.$application->apid . '/delete')}}" class="btn btn-danger padding-overlay"> Delete </a>
                                                        </td>
                                                    </tr>
													<?php $count ++; ?>
                                                @endforeach
                                            @else

                                                <tr>
                                                    <td colspan="7" style="color: silver; text-align: center; margin-top: 30px;"> There are no applications </td>
                                                </tr>
                                            @endif

                                            </tbody>
                                        </table>

                                        <div class="pull-right">
                                            {{$applications->links()}}

                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /# column -->

                    </div>
                    <!-- /# row -->

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="footer">
                                <p>This dashboard was generated on <span id="date-time"></span> <a href="#" class="page-refresh">Refresh Dashboard</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection