@extends('layouts.admin')
@section('content')

    <div class="content-wrap">
        <div class="main">

            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-8 p-r-0 title-margin-right">
                        <div class="page-header">
                            <div class="page-title">
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                    <div class="col-lg-4 p-l-0 title-margin-left">
                        <div class="page-header">
                            <div class="page-title">
                                <ol class="breadcrumb text-right">
                                    <li><a href="#">Dashboard</a></li>
                                    <li class="active">Add Student</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                </div>
                <!-- /# row -->
                @include('notification')
                <div class="main-content">


                    <form class="form-horizontal" method="post" action="{{url('post-create-application')}}">
                        {{csrf_field()}}
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card alert">
                                <div class="card-header">
                                    <h4> Student Data</h4>
                                    <div class="card-header-right-icon">
                                        <ul>
                                            <li class="card-close" data-dismiss="alert"><i class="ti-close"></i></li>
                                            <li class="doc-link"><a href="#"><i class="ti-link"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="menu-upload-form">

                                            <div class="card-body">
                                                <div class="menu-upload-form">

                                                    <div class="form-group">
                                                        <label>Surname</label>
                                                        <input type="text" class="form-control"  name="surName" placeholder="Enter Surname" required>
                                                    </div>


                                                    <div class="form-group">
                                                        <label>Forenames (full names)</label>
                                                        <input type="text" placeholder="Enter Other Names in full" name="foreNames" class="form-control">

                                                    </div>

                                                    <div class="form-group">
                                                        <label> Preferred Name</label>
                                                        <input type="text" name="preferredName" placeholder="Preferred Name" class="form-control" >

                                                    </div>



                                                    <div class="row">

                                                        <div class="form-group phone col-md-6">
                                                            <label for="gender">Gender</label>
                                                            <select class="form-control" name="gender" required>
                                                                <option selected disabled >Gender</option>
                                                                <option value="Male">Male</option>
                                                                <option value="Female">Female</option>
                                                            </select>
                                                        </div><!--//form-group-->


                                                        <div class="form-group email col-md-6">
                                                            <label for="email">Date of Birth<span class="required">*</span></label>
                                                            <input type="date" name="dob" class="form-control calendar bg-ash" placeholder="dd/mm/yyyy" id="text-calendar">

                                                        </div><!--//form-group-->

                                                    </div>


                                                    <div class="form-group forename">
                                                        <label for="email">Religion Domination<span class="required">*</span></label>
                                                        <input id="religion" name="religionDenomination"  type="text" class="form-control" placeholder="Religion Domination">
                                                    </div><!--//form-group-->

                                                    <div class="form-group forename">
                                                        <label for="email">Nationality<span class="required">*</span></label>
                                                        <input id="religion" name="nationality"  type="text" class="form-control" placeholder="Nationality">
                                                    </div><!--//form-group-->



                                                    <div class="form-group message">
                                                        <label for="message">Home Address of child<span class="required">*</span></label>
                                                        <textarea id="message" class="form-control" rows="3" name="homeAddress"  placeholder="Home Address of child" ></textarea>
                                                    </div><!--//form-group-->

                                                    <div class="form-group forename">
                                                        <label for="email">Home Telephone Number<span class="required">*</span></label>
                                                        <input id="religion" type="text" class="form-control" name="homePhone"  placeholder="Home Phone" >
                                                    </div><!--//form-group-->


                                                    <div class="form-group forename">
                                                        <label for="email">First Language(language spoken at home)</label>
                                                        <input id="religion" type="text" class="form-control" name="firstLanguage"  placeholder="First Language ">
                                                    </div><!--//form-group-->


                                                    <div class="form-group forename">
                                                        <label for="email">Proposed year of entry<span class="required">*</span></label>
                                                        <input id="religion" type="text" class="form-control" name="proposedYear"  placeholder=" Proposed year Of Entry" >
                                                    </div><!--//form-group-->


                                                    {{--comnnection--}}

                                                    {{--<div class="form-group phone">--}}
                                                        {{--<label for="gender">Have you a connection at Mama Clara?</label>--}}
                                                        {{--<select  class="form-control" name="connection" required>--}}
                                                            {{--<option value="">Connection?</option>--}}
                                                            {{--<option value="yes">Yes</option>--}}
                                                            {{--<option value="no">No</option>--}}
                                                        {{--</select>--}}
                                                    {{--</div><!--//form-group-->--}}


                                                    {{--accomodation--}}

                                                    {{--<div class="form-group phone">--}}
                                                        {{--<label for="gender">Accomodation</label>--}}
                                                        {{--<div class="row checkbox-group" >--}}
                                                            {{--<div class="col-md-4">--}}
                                                                {{--<input  id="" type="checkbox" value="day" name="accommodation">--}}
                                                                {{--<label for="checkbox3">--}}
                                                                    {{--Day--}}
                                                                {{--</label>--}}
                                                            {{--</div>--}}
                                                            {{--<div class="col-md-4">--}}
                                                                {{--<input  id="" type="checkbox" value="boarding" name="accommodation">--}}
                                                                {{--<label for="checkbox3">--}}
                                                                    {{--Boarding--}}
                                                                {{--</label>--}}
                                                            {{--</div>--}}
                                                            {{--<div class="col-md-4">--}}
                                                                {{--<input  id="" type="checkbox" value="parent" name="accommodation">--}}
                                                                {{--<label for="checkbox3">--}}
                                                                    {{--Parent--}}
                                                                {{--</label>--}}
                                                            {{--</div>--}}
                                                            {{--<div class="col-md-4">--}}
                                                                {{--<input  id="" type="checkbox" value="grandparent" name="accommodation">--}}
                                                                {{--<label for="checkbox3">--}}
                                                                    {{--GrandParent--}}
                                                                {{--</label>--}}
                                                            {{--</div>--}}
                                                            {{--<div class="col-md-4">--}}
                                                                {{--<input  id="" type="checkbox" value="sibling" name="accommodation">--}}
                                                                {{--<label for="checkbox3">--}}
                                                                    {{--Sibling--}}
                                                                {{--</label>--}}
                                                            {{--</div>--}}
                                                            {{--<div class="col-md-4">--}}
                                                                {{--<input id="" type="checkbox" value="other" name="accommodation">--}}
                                                                {{--<label for="checkbox3">--}}
                                                                    {{--Other--}}
                                                                {{--</label>--}}
                                                            {{--</div>--}}

                                                        {{--</div>--}}
                                                    {{--</div><!--//form-group-->--}}


                                                </div>
                                            </div>




                                    </div>
                                </div>
                            </div>
                            <!-- /# card -->
                        </div>
                        <!-- /# column -->
                    </div>

                        {{--current school--}}
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card alert">
                                    <div class="card-header">
                                        <div class="card-header-right-icon">
                                            <ul>
                                                <li class="card-close" data-dismiss="alert"><i class="ti-close"></i></li>
                                                <li class="doc-link"><a href="#"><i class="ti-link"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <div class="menu-upload-form">
                                                        <h3> Current School </h3>
                                                        <br>
                                                        <div class="form-group">
                                                            <label>Current School</label>
                                                            <input type="text" class="form-control"  name="currentSchool" placeholder="Current School Attended By Student And Address" >
                                                        </div>
                                                        {{--<div class="form-group">--}}
                                                            {{--<label>Attendance Date</label>--}}
                                                            {{--<input type="date" class="form-control"  name="attendanceDate" placeholder="Attendance Date" required>--}}
                                                        {{--</div>--}}

                                                        <div class="form-group">
                                                            <label>Head teacher</label>
                                                            <input type="text" class="form-control"  name="headTeacher" placeholder="Head Teacher's Name (Include title e.g Dr,Mr,Mrs)" >
                                                        </div>

                                                        <div class="form-group">
                                                            <label>Head teacher's Email </label>
                                                            <input type="email" class="form-control"  name="headTeacherEmail" placeholder="Head Teacher's Email" >
                                                        </div>

                                                        <div class="form-group">
                                                            <label>School Phone</label>
                                                            <input type="number" class="form-control"  name="schoolPhone" placeholder="Phone Number of Someone at the school" >
                                                        </div>
                                            {{--end form--}}
                                        </div>
                                    </div>
                                </div>
                                <!-- /# card -->
                            </div>
                            <!-- /# column -->
                        </div>



                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card alert">
                                    <div class="card-header">
                                        <div class="card-header-right-icon">
                                            <ul>
                                                <li class="card-close" data-dismiss="alert"><i class="ti-close"></i></li>
                                                <li class="doc-link"><a href="#"><i class="ti-link"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <div class="menu-upload-form">

                                            <hr>
                                            <h3> Father's Data </h3>
                                            <br>
                                            <div class="row">
                                                <div class="form-group phone col-md-6">
                                                    <label for="gender">Title</label>
                                                    <select class="form-control" name="fatherTitle" >
                                                        <option selected disabled >Please pick a Title</option>
                                                        <option>Mr.</option>
                                                        <option>Dr.</option>
                                                        <option>Eng.</option>
                                                        <option>Chief.</option>
                                                    </select>
                                                </div><!--//form-group-->
                                                <div class="form-group email col-md-6">
                                                    <label for="email">Name<span class="required">*</span></label>
                                                    <input id="dob" type="text" name="father" class="form-control" placeholder=" Name of Father/Legal Guardian" >
                                                </div><!--//form-group-->

                                            </div>

                                            <div class="form-group">
                                                <label>Fathers Profession </label>
                                                <input type="text" class="form-control"  name="fatherProfession" placeholder="Father's Profession" >
                                            </div>


                                            <div class="form-group">
                                                <label>Fathers Industry </label>
                                                <input type="text" class="form-control"  name="fatherIndustry" placeholder="Father's Industry" >
                                            </div>

                                            <div class="form-group">
                                                <label>Father Employers Name </label>
                                                <input type="text" class="form-control"  name="fatherEmployerName" placeholder="Father Employers Name" >
                                            </div>

                                            <div class="form-group">
                                                <label>Fathers Email </label>
                                                <input type="text" class="form-control"  name="fatherEmail" placeholder="Father's Email" >
                                            </div>


                                            <div class="form-group">
                                                <label>Fathers Phone </label>
                                                <input type="text" class="form-control"  name="fatherPhone" placeholder="Fathers Phone Number" >
                                            </div>


                                            {{--end form--}}
                                        </div>
                                    </div>
                                </div>
                                <!-- /# card -->
                            </div>
                            <!-- /# column -->
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card alert">
                                    <div class="card-header">
                                        <div class="card-header-right-icon">
                                            <ul>
                                                <li class="card-close" data-dismiss="alert"><i class="ti-close"></i></li>
                                                <li class="doc-link"><a href="#"><i class="ti-link"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <div class="menu-upload-form">


                                            <hr>
                                            <h3> Mother's Data </h3>
                                            <br>
                                            <div class="row">
                                                <div class="form-group phone col-md-6">
                                                    <label for="gender">Title</label>
                                                    <select class="form-control" name="motherTitle" >
                                                        <option selected disabled > Title</option>
                                                        <option >Mrs.</option>
                                                        <option>Dr.</option>
                                                        <option>Eng.</option>
                                                        <option>Chief.</option>
                                                    </select>
                                                </div><!--//form-group-->
                                                <div class="form-group email col-md-6">
                                                    <label for="email">Name<span class="required">*</span></label>
                                                    <input id="dob" type="text" name="mother" class="form-control" placeholder="Name of Mother/Legal Guardian" >
                                                </div><!--//form-group-->
                                            </div>
                                            <div class="form-group">
                                                <label>Mothers Profession </label>
                                                <input type="text" class="form-control"  name="motherProfession" placeholder="Mother's Profession" >
                                            </div>
                                            <div class="form-group">
                                                <label>Mothers Industry </label>
                                                <input type="text" class="form-control"  name="motherIndustry" placeholder="Mother's Industry" >
                                            </div>
                                            <div class="form-group">
                                                <label>Mother Employers Name </label>
                                                <input type="text" class="form-control"  name="motherEmployerName" placeholder="Mother Employers Name" >
                                            </div>
                                            <div class="form-group">
                                                <label>Mother's Email </label>
                                                <input type="text" class="form-control"  name="motherEmail" placeholder="Mother's Email" >
                                            </div>
                                            <div class="form-group">
                                                <label>Mothers Phone </label>
                                                <input type="text" class="form-control"  name="motherPhone" placeholder="Mothers Phone Number" >
                                            </div>



                                            {{--end form--}}
                                        </div>
                                    </div>
                                </div>
                                <!-- /# card -->
                            </div>
                            <!-- /# column -->
                        </div>


                        {{--Relatives--}}
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card alert">
                                    <div class="card-header">
                                        <div class="card-header-right-icon">
                                            <ul>
                                                <li class="card-close" data-dismiss="alert"><i class="ti-close"></i></li>
                                                <li class="doc-link"><a href="#"><i class="ti-link"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <div class="menu-upload-form">

                                            {{--<form class="form-horizontal" method="post" action="{{url('post-create-application')}}">--}}
                                                {{--{{csrf_field()}}--}}
                                                <div class="card-body">
                                                    <div class="menu-upload-form">
                                                        <hr>
                                                        <h3> Other People With Parental Responsibility </h3>
                                                        <br>
                                                        <div class="form-group">
                                                            <label>First Name </label>
                                                            <input type="text" class="form-control"  name="fname" placeholder="First Name" >
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Last Name </label>
                                                            <input type="text" class="form-control"  name="sname" placeholder="First Name" >
                                                        </div>
                                                        <div class="form-group">
                                                            <label> Email</label>
                                                            <input type="email" class="form-control"  name="email" placeholder="Relative's Email" >
                                                        </div>
                                                        <div class="form-group">
                                                            <label> Phone </label>
                                                            <input type="tel" class="form-control"  name="Phone" placeholder="Relative's Phone Number" >
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Address </label>
                                                            <input type="text" class="form-control"  name="address" placeholder="Relative's Home Address" >
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Relationship to Child </label>
                                                            <input type="text" class="form-control"  name="relationshipToChild" placeholder="Relationship to child" >
                                                        </div>

                                                    </div>
                                                </div>
                                            {{--</form>--}}




                                        </div>
                                    </div>
                                </div>
                                <!-- /# card -->
                            </div>
                            <!-- /# column -->
                        </div>

                        {{--hear of mamaclara--}}
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card alert">
                                    <div class="card-header">
                                        <div class="card-header-right-icon">
                                            <ul>
                                                <li class="card-close" data-dismiss="alert"><i class="ti-close"></i></li>
                                                <li class="doc-link"><a href="#"><i class="ti-link"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <div class="menu-upload-form">
                                            <h3> How did You Hear of Regent Project System  </h3>
                                            <br>

                                            <div class="form-group phone">
                                                {{--<label for="gender">Accomodation</label>--}}
                                                <div class="row checkbox-group" >
                                                    <div class="col-md-4">
                                                        <input id="" type="checkbox" value="Word of Mouth" name="hearOfHendon">
                                                        <label for="checkbox3">
                                                            Word of Mouth
                                                        </label>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <input id="" type="checkbox" value="Current School" name="hearOfHendon">
                                                        <label for="checkbox3">
                                                            Current School
                                                        </label>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <input id="" type="checkbox" value="Friend" name="hearOfHendon">
                                                        <label for="checkbox3">
                                                            Friend
                                                        </label>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <input id="" type="checkbox" value="Website" name="hearOfHendon">
                                                        <label for="checkbox3">
                                                            Website
                                                        </label>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <input id="" type="checkbox" value="Advertisement" name="hearOfHendon">
                                                        <label for="checkbox3">
                                                            Advertisement
                                                        </label>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <input id="" type="checkbox" value="Others" name="hearOfHendon">
                                                        <label for="checkbox3">
                                                            Others
                                                        </label>
                                                    </div>

                                                </div>
                                            </div><!--//form-group-->



                                            {{--end form--}}
                                        </div>
                                    </div>
                                </div>
                                <!-- /# card -->
                            </div>
                            <!-- /# column -->
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card alert">
                                    <div class="card-header">
                                        <div class="card-header-right-icon">
                                            <ul>
                                                <li class="card-close" data-dismiss="alert"><i class="ti-close"></i></li>
                                                <li class="doc-link"><a href="#"><i class="ti-link"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <div class="menu-upload-form">
                                            {{--<h3> DECLARATION  </h3>--}}

                                            {{--<br>--}}


                                            {{--<div id="declaration">--}}
                                                {{--<p><i>I/We request that the name of our above- named child be registered as a prospective pupil.--}}
                                                        {{--I/We enclose the non-refundable Registration fee of NGN15, 000 made payable to the school--}}
                                                        {{--for the postage of test papers to candidate writing the exam abroad. I/We understand that--}}
                                                        {{--the terms and conditions of the school will undergo reasonable changes from time to time as--}}
                                                        {{--circumstances require and will apply in all our dealings with the school(through the principal,--}}
                                                        {{--as the person responsible)may obtain, process and hold personal information about our child,--}}
                                                        {{--including sensitive information such as medical details, and we consent to this for the purpose of--}}
                                                        {{--assessment and,--}}
                                                        {{--if a place is later offered, in order to safeguard and promote the welfare of the child. </i></p> <br>--}}
                                                {{--<p>I/We also understand that signing this form does not give rise to contract with the school</p>--}}


                                                {{--<label for="dc">--}}
                                                    {{--I Understand--}}
                                                {{--</label>--}}
                                                {{--<input  id="dc" type="checkbox" value="yes" name="declaration">--}}


                                            {{--</div>--}}

                                            <div class="form-group">
                                                <div class=" col-sm-10">
                                                    <button type="submit" class="btn btn-lg btn-primary pull-right">Send</button>
                                                </div>
                                            </div>




                                            {{--end form--}}
                                        </div>
                                    </div>
                                </div>
                                <!-- /# card -->
                            </div>
                            <!-- /# column -->
                        </div>

                    </form>


                    <!-- /# row -->
                </div>
                <!-- /# main content -->
            </div>
            <!-- /# container-fluid -->
        </div>
        <!-- /# main -->
    </div>

@endsection


