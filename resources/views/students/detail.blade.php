@extends('layouts.admin')
@section('content')

    <style>
        .phone-number.link a{
            text-decoration: underline;
            color:#03A9F5;
        }
    </style>

    <div class="content-wrap">
        <div class="main">
            @include('notification')
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-8 p-r-0 title-margin-right">
                        <div class="page-header">
                            <div class="page-title">
                                <h1>Dashboard</h1>
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                    <div class="col-lg-4 p-l-0 title-margin-left">
                        <div class="page-header">
                            <div class="page-title">
                                <ol class="breadcrumb text-right">
                                    <li><a href="#">Dashboard</a></li>
                                    <li>Student</li>
                                    <li class="active">Details</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                </div>
                <!-- /# row -->
                <div id="main-content">
                    <!-- /# column -->

                    <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <div class="card alert">
                                <div class="card-header">
                                    <h4>STUDENT INFO</h4>
                                </div>
                                <div class="card-body">
                                    <div class="default-tab">
                                        <ul class="nav nav-tabs" role="tablist">
                                            <li role="presentation" class="active"><a href="#home1" aria-controls="home"
                                                                                      role="tab" data-toggle="tab">Student</a>
                                            </li>
                                            <li role="presentation"><a href="#subjects" aria-controls="subjects"
                                                                       role="tab" data-toggle="tab">Subjects</a></li>                                            <li role="presentation"><a href="#health" aria-controls="health"
                                                                       role="tab" data-toggle="tab">Health</a></li>
                                            <li role="presentation"><a href="#results" aria-controls="results"
                                                                       role="tab" data-toggle="tab">Results</a></li>
                                            <li role="presentation"><a href="#finance" aria-controls="finance"
                                                                       role="tab" data-toggle="tab"> Finance</a></li>
                                        </ul>


                                        <!-- Tabs -->
                                        <div class="tab-content">

                                            <!-- Main Tab -->
                                            <div role="tabpanel" class="tab-pane active" id="home1">

                                                <div class="card-body">
                                                    <div class="user-profile m-t-15">
                                                        <div class="row">
                                                            <div class="col-lg-4">
                                                                <div class="user-photo m-b-30">

                                                                    @if(!empty($student->image))
                                                                        <img class="img-responsive"
                                                                             src="{{$student->image}}"
                                                                             alt="Student Image"/>
                                                                    @else
                                                                        <img class="img-responsive"
                                                                             src="{{url('admin/assets/images/default-image.jpg')}}"
                                                                             alt="Student Image"/>

                                                                    @endif

                                                                </div>
                                                            </div>
                                                            <div class="col-lg-8">
                                                                <div class="user-profile-name dib">{{$student->fname}} {{$student->sname}}</div>
                                                                <div class="useful-icon dib pull-right">
                                                                    <span><a href="{{url('/student/'.$student->sid.'/edit')}}"
                                                                             class=""><i
                                                                                    class="ti-pencil-alt"></i></a> </span>
                                                                </div>
                                                                <div class="custom-tab user-profile-tab">
                                                                    <ul class="nav nav-tabs" role="tablist">
                                                                        <li role="presentation" class="active"><a
                                                                                    href="#1" aria-controls="1"
                                                                                    role="tab"
                                                                                    data-toggle="tab">About</a></li>
                                                                    </ul>
                                                                    <div class="tab-content">
                                                                        <div role="tabpanel" class="tab-pane active"
                                                                             id="1">
                                                                            <div class="contact-information">
                                                                                <div class="phone-content">
                                                                                    <span class="contact-title">Student ID:</span>
                                                                                    <span class="phone-number">{{$student->studentid}}</span>
                                                                                </div>
                                                                                <div class="phone-content">
                                                                                    <span class="contact-title"> Focus:</span>
                                                                                    <span class="phone-number">

                                                                                        @if(isset($student->Focus))
                                                                                            {{$student->Focus->name}}
                                                                                        @else
                                                                                            Not assigned yet
                                                                                        @endif

                                                                                    </span>
                                                                                </div>
                                                                                <div class="website-content">
                                                                                    <span class="contact-title">Department:</span>
                                                                                    <span class="contact-website">

                                                                                        @if(isset($student->Class))

                                                                                            {{$student->Class->name}}
                                                                                        @else
                                                                                            Not assigned yet
                                                                                        @endif
                                                                                    </span>
                                                                                </div>

                                                                                <div class="gender-content">
                                                                                    <span class="contact-title">Gender:</span>
                                                                                    <span class="gender">{{$student->gender}}</span>
                                                                                </div>
                                                                                <div class="phone-content">
                                                                                    <span class="contact-title">Phone</span>
                                                                                    <span class="phone-number">{{$student->phone}}</span>
                                                                                </div>

                                                                                <div class="phone-content">
                                                                                    <span class="contact-title">Email</span>
                                                                                    <span class="phone-number">{{$student->email}}</span>
                                                                                </div>


                                                                                <div class="birthday-content">
                                                                                    <span class="contact-title">Date of Birth:</span>
                                                                                    <span class="birth-date">{{$student->dob}} </span>
                                                                                </div>

                                                                                <div class="phone-content">
                                                                                    <span class="contact-title">Religion:</span>
                                                                                    <span class="phone-number">{{$student->religion}}</span>
                                                                                </div>
                                                                                <div class="email-content">
                                                                                    <span class="contact-title">Nationality:</span>
                                                                                    <span class="contact-email">{{$student->nationality}}</span>
                                                                                </div>
                                                                                <div class="email-content">
                                                                                    <span class="contact-title">First Language:</span>
                                                                                    <span class="contact-email">{{$student->language}}</span>
                                                                                </div>

                                                                                <div class="address-content">
                                                                                    <span class="contact-title">Home Address:</span>
                                                                                    <span class="mail-address">{{$student->address}}</span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                {{--end of first card--}}
                                            </div>

                                            <!-- Subjects -->
                                            <div role="tabpanel" class="tab-pane" id="subjects">

                                                <p>These are the subjects the student is offering</p>

                                                <table class="table student-data-table m-t-20">
                                                    <thead>
                                                    <tr>
                                                        <th>S/N</th>
                                                        <th>Title</th>
                                                        <th>Project Supervisor</th>
                                                        <th>Created</th>
                                                        <th></th>

                                                    </tr>
                                                    </thead>
                                                    <tbody>

                                                    @if(count($student->Subjects)>0)

			                                            <?php $count = 1; ?>

                                                        @foreach($student->Subjects as $subject)
                                                            <tr>

                                                                <td>
                                                                    #<?php echo $count;?>
                                                                </td>
                                                                <td>
                                                                    {{$subject->name}}
                                                                </td>
                                                                <td>
                                                                    {{$subject->Teacher->fname}}  {{$subject->Teacher->sname}}
                                                                </td>
                                                                <td>
                                                                    {{$subject->created_at->diffForHumans()}}
                                                                </td>

                                                                <td>
                                                                    <span><a href="{{url('subject/'.$subject->subid .'/detail')}}"><i class="ti-eye color-default"></i></a> </span>
                                                                    <span><a title="Un-assign Subject" href="{{url('subject/student/remove/'.$subject->subid .'/' . $student->sid)}}"><i class="ti-close color-danger"></i></a> </span>
                                                                </td>
                                                            </tr>
				                                            <?php $count ++; ?>
                                                        @endforeach
                                                    @else

                                                        <tr>
                                                            <td colspan="4" style="color: silver; text-align: center; margin-top: 30px;"> There are no Project yet </td>
                                                        </tr>

                                                    @endif




                                                    </tbody>
                                                </table>

                                            </div>


                                            <!-- Health Tab -->
                                            <div role="tabpanel" class="tab-pane" id="health">

                                                @if(count($healthRecords) <= 0)
                                                    <p align="center">No records.</p>
                                                @endif
												<?php $count = 1; ?>
                                                @foreach($healthRecords as $health)
                                                    <div class="row">
                                                        <div class="col-lg-10 col-md-10">
                                                            <div class="card alert">
                                                                <div class="card-header">
                                                                    <h4>Diagnosis</h4>
                                                                </div>
                                                                <div class="card-body">
                                                                    <p class="text-muted m-b-15">
                                                                        {{str_limit($health->diagnosis,100,' ....')}}
                                                                    </p>
                                                                    <button class="btn btn-danger m-b-10 pull-right"
                                                                            data-toggle="collapse"
                                                                            data-target="#basic-collapse{{$count}}">More
                                                                        Details
                                                                    </button>

                                                                    <div id="basic-collapse{{$count}}" class="collapse">
                                                                        <h4> Detail </h4>
                                                                        {{$health->details}}
                                                                        <br>
                                                                        <br>
                                                                        <h4> Treatment </h4>
                                                                        {{$health->treatment}}
                                                                    </div>

                                                                </div>
                                                            </div>
                                                            <!-- /# card -->
                                                        </div>
                                                    </div>
                                                    <br>
                                                    <hr>
													<?php $count ++; ?>

                                                @endforeach


                                                {{--<p> This table Holds the Health status of the student and reports of any treatment administered at the school </p>--}}

                                            </div>

                                            <!-- Results Tab -->
                                            <div role="tabpanel" class="tab-pane" id="results">
                                                <p>This holds a report of the academic performance of the
                                                    student,complaints and any suggestion for academics</p>
                                            </div>

                                            <!-- Finance Tab -->

                                            <div role="tabpanel" class="tab-pane" id="finance">
                                                <p>This tab holds the financial record of the students payments and
                                                    pending payments.
                                                </p>
                                            </div>


                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="row">
                        <div class="card-body" style="padding:50px;">
                            <div class="menu-upload-form">
                                <form class="form-horizontal" method="post" action="{{url('assign-student-subject')}}">
                                    {{csrf_field()}}

                                    <input type="hidden" name="sid" value="{{$student->sid}}">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Project</label>
                                        <div class="col-sm-10">
                                            @foreach($subjects as $subject)
                                                <input type="checkbox" value="{{$subject->subid}}" name="subids[]">
                                                <label for="checkbox3">
                                                    {{$subject->name}}
                                                </label>
                                            @endforeach

                                        </div>
                                    </div>





                                    <div class="form-group">
                                        <div class="col-sm-offset-2 col-sm-10">
                                            <button type="submit" class="btn btn-lg btn-primary">Send</button>
                                        </div>
                                    </div>


                                </form>
                            </div>
                        </div>

                    </div>

                    <!-- Parents -->
                    <div class="row">
                        @foreach($student->parents as $parent)
                            <div class="col-lg-6">
                                <div class="card alert">
                                    <div class="card-header">
                                        <h4>{{strtoupper($parent->relationship)}} </h4>

                                    </div>
                                    <div class="card-body">
                                        <div class="user-profile m-t-15">
                                            <div class="row">
                                                <div class="col-lg-12">

                                                    <div class="custom-tab user-profile-tab">
                                                        <ul class="nav nav-tabs"
                                                            role="tablist">
                                                            <li role="presentation"
                                                                class="active"><a href="#1"
                                                                                  aria-controls="1"
                                                                                  role="tab"
                                                                                  data-toggle="tab">About</a>
                                                            </li>
                                                        </ul>
                                                        <div class="tab-content">
                                                            <div role="tabpanel"
                                                                 class="tab-pane active"
                                                                 id="1">
                                                                <div class="contact-information">
                                                                    <div class="phone-content">
                                                                        <span class="contact-title">Name:</span>
                                                                        <span class="phone-number link"><a
                                                                                    href="{{url('/parent/'.$parent->pid.'/detail')}}">{{$parent->fname}} {{$parent->sname}}</a></span>
                                                                    </div>
                                                                    <div class="phone-content">
                                                                        <span class="contact-title"> Phone:</span>
                                                                        <span class="phone-number">{{$parent->phone}}</span>
                                                                    </div>
                                                                    <div class="phone-content">
                                                                        <span class="contact-title"> Email:</span>
                                                                        <span class="phone-number">{{$parent->email}}</span>
                                                                    </div>
                                                                    <div class="phone-content">
                                                                        <span class="contact-title"> Address:</span>
                                                                        <span class="phone-number">{{$parent->address}}</span>
                                                                    </div>


                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        @endforeach
                    </div>








                    {{--<div class="row">--}}
                        {{--<div class="col-lg-12 col-md-12">--}}
                            {{--<div class="card alert">--}}
                                {{--<div class="card-header">--}}
                                    {{--<h4>PARENTS</h4>--}}
                                {{--</div>--}}
                                {{--<div class="card-body">--}}
                                    {{--<div class="default-tab">--}}
                                        {{--<div class="tab-content">--}}
                                            {{--<div role="tabpanel" class="tab-pane active" id="home1">--}}

                                                {{--<hr>--}}

                                                {{--<div class="row">--}}
                                                    {{--@foreach($student->parents as $parent)--}}
                                                        {{--<div class="col-lg-6">--}}
                                                            {{--<div class="card alert">--}}
                                                                {{--<div class="card-header">--}}
                                                                    {{--<h4>{{strtoupper($parent->relationship)}} </h4>--}}

                                                                {{--</div>--}}
                                                                {{--<div class="card-body">--}}
                                                                    {{--<div class="user-profile m-t-15">--}}
                                                                        {{--<div class="row">--}}
                                                                            {{--<div class="col-lg-12">--}}

                                                                                {{--<div class="custom-tab user-profile-tab">--}}
                                                                                    {{--<ul class="nav nav-tabs"--}}
                                                                                        {{--role="tablist">--}}
                                                                                        {{--<li role="presentation"--}}
                                                                                            {{--class="active"><a href="#1"--}}
                                                                                                              {{--aria-controls="1"--}}
                                                                                                              {{--role="tab"--}}
                                                                                                              {{--data-toggle="tab">About</a>--}}
                                                                                        {{--</li>--}}
                                                                                    {{--</ul>--}}
                                                                                    {{--<div class="tab-content">--}}
                                                                                        {{--<div role="tabpanel"--}}
                                                                                             {{--class="tab-pane active"--}}
                                                                                             {{--id="1">--}}
                                                                                            {{--<div class="contact-information">--}}
                                                                                                {{--<div class="phone-content">--}}
                                                                                                    {{--<span class="contact-title">Name:</span>--}}
                                                                                                    {{--<span class="phone-number"><a--}}
                                                                                                                {{--href="{{url('/parent/'.$parent->pid.'/detail')}}">{{$parent->fname}} {{$parent->sname}}</a></span>--}}
                                                                                                {{--</div>--}}
                                                                                                {{--<div class="phone-content">--}}
                                                                                                    {{--<span class="contact-title"> Phone:</span>--}}
                                                                                                    {{--<span class="phone-number">{{$parent->phone}}</span>--}}
                                                                                                {{--</div>--}}
                                                                                                {{--<div class="phone-content">--}}
                                                                                                    {{--<span class="contact-title"> Email:</span>--}}
                                                                                                    {{--<span class="phone-number">{{$parent->email}}</span>--}}
                                                                                                {{--</div>--}}
                                                                                                {{--<div class="phone-content">--}}
                                                                                                    {{--<span class="contact-title"> Address:</span>--}}
                                                                                                    {{--<span class="phone-number">{{$parent->address}}</span>--}}
                                                                                                {{--</div>--}}


                                                                                            {{--</div>--}}
                                                                                        {{--</div>--}}
                                                                                    {{--</div>--}}
                                                                                {{--</div>--}}
                                                                            {{--</div>--}}
                                                                        {{--</div>--}}
                                                                    {{--</div>--}}
                                                                {{--</div>--}}
                                                            {{--</div>--}}
                                                        {{--</div>--}}

                                                {{--@endforeach--}}
                                                {{--</div>--}}
                                                {{--<hr>--}}


                                            {{--</div>--}}


                                            {{--<div role="tabpanel" class="tab-pane" id="profile">--}}
                                                {{--<p> This table Holds the Health status of the student and reports of any--}}
                                                    {{--treatment administered at the school </p>--}}


                                            {{--</div>--}}
                                            {{--<div role="tabpanel" class="tab-pane" id="messages">--}}
                                                {{--<p>This holds a report of the academic performance of the--}}
                                                    {{--student,complaints and any suggestion for academics</p>--}}
                                            {{--</div>--}}
                                            {{--<div role="tabpanel" class="tab-pane" id="settings">--}}
                                                {{--<p>This tab holds the financial record of the students payments and--}}
                                                    {{--pending payments.</p>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<!-- /# column -->--}}

                        {{--<!-- /# column -->--}}
                    {{--</div>--}}


                {{--father--}}





                <!-- /# row -->

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="footer">
                                <p>This dashboard was generated on <span id="date-time"></span> <a href="#"
                                                                                                   class="page-refresh">Refresh
                                        Dashboard</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>







    {{--@foreach($healthRecords as  $health)--}}
        {{--<div class="card-body">--}}
            {{--<div class="user-profile m-t-15">--}}
                {{--<div class="row">--}}
                    {{--<div class="col-lg-2">--}}
                        {{--<div class="user-photo m-b-30">--}}
                            {{--<img class="img-responsive" src="{{$student->image}}" alt="Photo Space"/>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="col-lg-10">--}}
                        {{--<div class="user-profile-name dib">{{$health->diagnosis}} </div>--}}
                        {{--<div class="useful-icon dib pull-right">--}}
                            {{--<span><a href="{{url('/student/'.$student->sid.'/edit')}}" class="btn btn-danger"><i--}}
                                            {{--class="ti-pencil-alt"></i></a> </span>--}}
                            {{--<span><a href="#"><i class="ti-printer"></i></a></span>--}}
                            {{--<span><a href="#"><i class="ti-download"></i></a></span>--}}
                            {{--<span><a href="#"><i class="ti-share"></i></a></span>--}}
                        {{--</div>--}}
                        {{--<div class="custom-tab user-profile-tab">--}}
                            {{--<ul class="nav nav-tabs" role="tablist">--}}
                                {{--<li role="presentation" class="active"><a href="#1" aria-controls="1" role="tab"--}}
                                                                          {{--data-toggle="tab">Treatment Details</a></li>--}}
                            {{--</ul>--}}
                            {{--<div class="tab-content">--}}
                            {{--<div role="tabpanel" class="tab-pane active" id="1">--}}
                            {{--<div class="contact-information">--}}
                            {{--<div class="phone-content">--}}
                            {{--<h3><span  class="contact-title">Details:</span></h3>--}}
                            {{--<p>{{$health->details}}</p>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            {{--</div>--}}

                            {{--<div class="tab-content">--}}
                                {{--<div role="tabpanel" class="tab-pane active" id="1">--}}
                                    {{--<div class="contact-information">--}}
                                        {{--<div class="phone-content">--}}
                                            {{--<h3><span class="contact-title">Treatment:</span></h3>--}}
                                            {{--<p>{{str_limit($health->treatment,10 ,'  ....')}}</p>--}}

                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<br>--}}
        {{--<br>--}}

        {{--<hr>--}}
        {{--<br>--}}
    {{--@endforeach--}}

@endsection









