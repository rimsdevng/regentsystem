@extends('layouts.admin')
@section('content')

    <div class="content-wrap">
        <div class="main">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-8 p-r-0 title-margin-right">
                        <div class="page-header">
                            <div class="page-title">
                                <h1>Dashboard</h1>
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                    <div class="col-lg-4 p-l-0 title-margin-left">
                        <div class="page-header">
                            <div class="page-title">
                                <ol class="breadcrumb text-right">
                                    <li><a href="#">Dashboard</a></li>
                                    <li >Admissions</li>
                                    <li class="active">Project Applications</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                </div>
                <!-- /# row -->
                <div id="main-content">
                    <!-- /# column -->

                    <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <div class="card alert">
                                <div class="card-header">
                                    {{--<h4>Default Tab</h4>--}}
                                </div>
                                <div class="card-body">
                                    <div class="default-tab">
                                        <ul class="nav nav-tabs" role="tablist">
                                            <li role="presentation" class="active"><a href="#home1" aria-controls="home" role="tab" data-toggle="tab">New Student</a></li>
                                            <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Existing Parents</a></li>
                                            {{--<li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Message</a></li>--}}
                                            {{--<li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Setting</a></li>--}}
                                        </ul>
                                        <div class="tab-content">
                                            <div role="tabpanel" class="tab-pane active" id="home1">
                                                <h3>*Note</h3>
                                                <p> Use This tab for fresh Application, If the parent already has kids with use, use next Next </p>

                                                <form class="form-horizontal" method="post" action="{{url('post-create-application')}}">
                                                    {{csrf_field()}}
                                                    <div class="card-body">
                                                        <div class="menu-upload-form">


                                                            <div class="form-group">
                                                                <label>Surname</label>
                                                                <input type="text" class="form-control"  name="surName" placeholder="Enter Surname" required>
                                                            </div>


                                                            <div class="form-group">
                                                                <label>Forenames (full names)</label>
                                                                <input type="text" placeholder="Enter Other Names in full" name="foreNames" class="form-control">

                                                            </div>

                                                            <div class="form-group">
                                                                <label> Preferred Name</label>
                                                                <input type="text" name="preferredName" placeholder="Preferred Name" class="form-control" >

                                                            </div>



                                                            <div class="row">

                                                                <div class="form-group phone col-md-6">
                                                                    <label for="gender">Gender</label>
                                                                    <select class="form-control" name="gender" required>
                                                                        <option value="">Gender</option>
                                                                        <option value="Male">Male</option>
                                                                        <option value="Female">Female</option>
                                                                    </select>
                                                                </div><!--//form-group-->


                                                                <div class="form-group email col-md-6">
                                                                    <label for="email">Date of Birth<span class="required">*</span></label>
                                                                    <input type="text" class="form-control calendar bg-ash" placeholder="dd/mm/yyyy" id="text-calendar">

                                                                </div><!--//form-group-->

                                                            </div>


                                                            <div class="form-group forename">
                                                                <label for="email">Religion Domination<span class="required">*</span></label>
                                                                <input id="religion" name="religionDenomination"  type="text" class="form-control" placeholder="Religion Domination">
                                                            </div><!--//form-group-->

                                                            <div class="form-group forename">
                                                                <label for="email">Nationality<span class="required">*</span></label>
                                                                <input id="religion" name="nationality"  type="text" class="form-control" placeholder="Nationality">
                                                            </div><!--//form-group-->



                                                            <div class="form-group message">
                                                                <label for="message">Home Address of child<span class="required">*</span></label>
                                                                <textarea id="message" class="form-control" rows="3" name="homeAddress"  placeholder="Home Address of child" required></textarea>
                                                            </div><!--//form-group-->

                                                            <div class="form-group forename">
                                                                <label for="email">Home Telephone Number<span class="required">*</span></label>
                                                                <input id="religion" type="text" class="form-control" name="homePhone"  placeholder="Home Phone" required>
                                                            </div><!--//form-group-->


                                                            <div class="form-group forename">
                                                                <label for="email">First Language(language spoken at home)</label>
                                                                <input id="religion" type="text" class="form-control" name="firstLanguage"  placeholder="First Language ">
                                                            </div><!--//form-group-->


                                                            <div class="form-group forename">
                                                                <label for="email">Proposed year of entry<span class="required">*</span></label>
                                                                <input id="religion" type="text" class="form-control" name="proposedYear"  placeholder=" Proposed year Of Entry" required>
                                                            </div><!--//form-group-->

                                                            <div class="form-group phone">
                                                                <label for="gender">Have you a connection at Hendon?</label>
                                                                <select  class="form-control" name="connection" required>
                                                                    <option value="">Connection?</option>
                                                                    <option value="yes">Yes</option>
                                                                    <option value="no">No</option>
                                                                </select>
                                                            </div><!--//form-group-->

                                                            <div class="form-group phone">
                                                                <label for="gender">Accomodation</label>
                                                                <div class="row checkbox-group" >
                                                                    <div class="col-md-4">
                                                                        <input  id="" type="checkbox" value="day" name="accommodation">
                                                                        <label for="checkbox3">
                                                                            Day
                                                                        </label>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <input  id="" type="checkbox" value="boarding" name="accommodation">
                                                                        <label for="checkbox3">
                                                                            Boarding
                                                                        </label>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <input  id="" type="checkbox" value="parent" name="accommodation">
                                                                        <label for="checkbox3">
                                                                            Parent
                                                                        </label>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <input  id="" type="checkbox" value="grandparent" name="accommodation">
                                                                        <label for="checkbox3">
                                                                            GrandParent
                                                                        </label>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <input  id="" type="checkbox" value="sibling" name="accommodation">
                                                                        <label for="checkbox3">
                                                                            Sibling
                                                                        </label>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <input id="" type="checkbox" value="other" name="accommodation">
                                                                        <label for="checkbox3">
                                                                            Other
                                                                        </label>
                                                                    </div>

                                                                </div>
                                                            </div><!--//form-group-->

                                                            //current school
                                                            <hr>

                                                            <h3> Current School </h3>
                                                            <br>
                                                            <div class="form-group">
                                                                <label>Current School</label>
                                                                <input type="text" class="form-control"  name="currentSchool" placeholder="Current School Attended By Student" required>
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Attendance Date</label>
                                                                <input type="text" class="form-control"  name="attendanceDate" placeholder="Attendance Date" required>
                                                            </div>

                                                            <div class="form-group">
                                                                <label>Head teacher</label>
                                                                <input type="text" class="form-control"  name="headTeacher" placeholder="Head Teacher's Name" required>
                                                            </div>

                                                            <div class="form-group">
                                                                <label>Head teacher's Email </label>
                                                                <input type="email" class="form-control"  name="headTeacherEmail" placeholder="Head Teacher's Email" required>
                                                            </div>

                                                            <div class="form-group">
                                                                <label>School Phone</label>
                                                                <input type="number" class="form-control"  name="schoolPhone" placeholder="Phone Number of Someone at the school" required>
                                                            </div>


                                                            <hr>

                                                            <h3> Father's Data </h3>
                                                            <br>


                                                            <div class="row">

                                                                <div class="form-group phone col-md-6">
                                                                    <label for="gender">Title</label>
                                                                    <select class="form-control" name="fatherTitle" required>
                                                                        <option value="">Mr.</option>
                                                                        <option value="Male">Dr.</option>
                                                                        <option value="Female">Eng.</option>
                                                                        <option value="Female">Chief.</option>
                                                                    </select>
                                                                </div><!--//form-group-->


                                                                <div class="form-group email col-md-6">
                                                                    <label for="email">Name<span class="required">*</span></label>
                                                                    <input id="dob" type="text" name="father" class="form-control" placeholder="Fathers Name" required>
                                                                </div><!--//form-group-->

                                                            </div>




                                                            <div class="form-group">
                                                                <label>Fathers Profession </label>
                                                                <input type="text" class="form-control"  name="fatherProfession" placeholder="Father's Profession" required>
                                                            </div>


                                                            <div class="form-group">
                                                                <label>Fathers Industry </label>
                                                                <input type="text" class="form-control"  name="fatherIndustry" placeholder="Father's Industry" required>
                                                            </div>

                                                            <div class="form-group">
                                                                <label>Father Employers Name </label>
                                                                <input type="text" class="form-control"  name="fatherEmployerName" placeholder="Father Employers Name" required>
                                                            </div>

                                                            <div class="form-group">
                                                                <label>Fathers Email </label>
                                                                <input type="text" class="form-control"  name="fatherEmail" placeholder="Father's Email" required>
                                                            </div>


                                                            <div class="form-group">
                                                                <label>Fathers Phone </label>
                                                                <input type="text" class="form-control"  name="fatherPhone" placeholder="Fathers Phone Number" required>
                                                            </div>






                                                            <hr>

                                                            <h3> Mother's Data </h3>
                                                            <br>


                                                            <div class="row">

                                                                <div class="form-group phone col-md-6">
                                                                    <label for="gender">Title</label>
                                                                    <select class="form-control" name="motherTitle" required>
                                                                        <option value="">Mrs.</option>
                                                                        <option value="Male">Dr.</option>
                                                                        <option value="Female">Eng.</option>
                                                                        <option value="Female">Chief.</option>
                                                                    </select>
                                                                </div><!--//form-group-->


                                                                <div class="form-group email col-md-6">
                                                                    <label for="email">Name<span class="required">*</span></label>
                                                                    <input id="dob" type="text" name="mother" class="form-control" placeholder="Mothers Name" required>
                                                                </div><!--//form-group-->

                                                            </div>




                                                            <div class="form-group">
                                                                <label>Mothers Profession </label>
                                                                <input type="text" class="form-control"  name="motherProfession" placeholder="Mother's Profession" required>
                                                            </div>


                                                            <div class="form-group">
                                                                <label>Mothers Industry </label>
                                                                <input type="text" class="form-control"  name="motherIndustry" placeholder="Mother's Industry" required>
                                                            </div>

                                                            <div class="form-group">
                                                                <label>Mother Employers Name </label>
                                                                <input type="text" class="form-control"  name="motherEmployerName" placeholder="Mother Employers Name" required>
                                                            </div>

                                                            <div class="form-group">
                                                                <label>Mother's Email </label>
                                                                <input type="text" class="form-control"  name="motherEmail" placeholder="Mother's Email" required>
                                                            </div>


                                                            <div class="form-group">
                                                                <label>Mothers Phone </label>
                                                                <input type="text" class="form-control"  name="motherPhone" placeholder="Mothers Phone Number" required>
                                                            </div>





                                                            <hr>

                                                            <h3> Relatives's Data </h3>
                                                            <br>



                                                            <div class="form-group">
                                                                <label>First Name </label>
                                                                <input type="text" class="form-control"  name="fname" placeholder="First Name" required>
                                                            </div>


                                                            <div class="form-group">
                                                                <label>Last Name </label>
                                                                <input type="text" class="form-control"  name="sname" placeholder="First Name" required>
                                                            </div>



                                                            <div class="form-group">
                                                                <label> Email</label>
                                                                <input type="email" class="form-control"  name="email" placeholder="Relative's Email" required>
                                                            </div>


                                                            <div class="form-group">
                                                                <label> Phone </label>
                                                                <input type="number" class="form-control"  name="Phone" placeholder="Relative's Phone Number" required>
                                                            </div>

                                                            <div class="form-group">
                                                                <label>Address </label>
                                                                <input type="text" class="form-control"  name="address" placeholder="Relative's Home Address" required>
                                                            </div>

                                                            <div class="form-group">
                                                                <label>Relationship to Child </label>
                                                                <input type="text" class="form-control"  name="relationshipToChild" placeholder="Relationship to child" required>
                                                            </div>


                                                            <div class="form-group">
                                                                <div class=" col-sm-10">
                                                                    <button type="submit" class="btn btn-lg btn-primary">Send</button>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>










                                            </div>
                                            <div role="tabpanel" class="tab-pane" id="profile">
                                                <h3>*Note</h3>
                                                <p> Use This Tab If the parent already have Kids In the School </p>

                                                <div class="card-body">
                                                    <div class="menu-upload-form">

                                                        <form class="form-horizontal" method="post" action="{{url('post-create-application')}}">
                                                            {{csrf_field()}}
                                                            <div class="form-group">
                                                                <label>Surname</label>
                                                                <input type="text" class="form-control"  name="surName" placeholder="Enter Surname" required>
                                                            </div>


                                                            <div class="form-group">
                                                                <label>Forenames (full names)</label>
                                                                <input type="text" placeholder="Enter Other Names in full" name="foreNames" class="form-control">

                                                            </div>

                                                            <div class="form-group">
                                                                <label> Preferred Name</label>
                                                                <input type="text" name="preferredName" placeholder="Preferred Name" class="form-control" >

                                                            </div>



                                                            <div class="row">

                                                                <div class="form-group phone col-md-6">
                                                                    <label for="gender">Gender</label>
                                                                    <select class="form-control" name="gender" required>
                                                                        <option value="">Gender</option>
                                                                        <option value="Male">Male</option>
                                                                        <option value="Female">Female</option>
                                                                    </select>
                                                                </div><!--//form-group-->


                                                                <div class="form-group email col-md-6">
                                                                    <label for="email">Date of Birth<span class="required">*</span></label>
                                                                    <input id="dob" type="datetime-local" name="dob" class="form-control" placeholder="Enter your DoB" required>
                                                                </div><!--//form-group-->

                                                            </div>


                                                            <div class="form-group forename">
                                                                <label for="email">Religion Domination<span class="required">*</span></label>
                                                                <input id="religion" name="religionDenomination"  type="text" class="form-control" placeholder="Religion Domination">
                                                            </div><!--//form-group-->

                                                            <div class="form-group forename">
                                                                <label for="email">Nationality<span class="required">*</span></label>
                                                                <input id="religion" name="nationality"  type="text" class="form-control" placeholder="Nationality">
                                                            </div><!--//form-group-->



                                                            <div class="form-group message">
                                                                <label for="message">Home Address of child<span class="required">*</span></label>
                                                                <textarea id="message" class="form-control" rows="3" name="homeAddress"  placeholder="Home Address of child" required></textarea>
                                                            </div><!--//form-group-->

                                                            <div class="form-group forename">
                                                                <label for="email">Home Telephone Number<span class="required">*</span></label>
                                                                <input id="religion" type="text" class="form-control" name="homePhone"  placeholder="Home Phone" required>
                                                            </div><!--//form-group-->


                                                            <div class="form-group forename">
                                                                <label for="email">First Language(language spoken at home)</label>
                                                                <input id="religion" type="text" class="form-control" name="firstLanguage"  placeholder="First Language ">
                                                            </div><!--//form-group-->


                                                            <div class="form-group forename">
                                                                <label for="email">Proposed year of entry<span class="required">*</span></label>
                                                                <input id="religion" type="text" class="form-control" name="proposedYear"  placeholder=" Proposed year Of Entry" required>
                                                            </div><!--//form-group-->

                                                            <div class="form-group phone">
                                                                <label for="gender">Have you a connection at Hendon?</label>
                                                                <select  class="form-control" name="connection" required>
                                                                    <option value="">Connection?</option>
                                                                    <option value="yes">Yes</option>
                                                                    <option value="no">No</option>
                                                                </select>
                                                            </div><!--//form-group-->

                                                            <div class="form-group phone">
                                                                <label for="gender">Accomodation</label>
                                                                <div class="row checkbox-group" >
                                                                    <div class="col-md-4">
                                                                        <input  id="" type="checkbox" value="day" name="accommodation">
                                                                        <label for="checkbox3">
                                                                            Day
                                                                        </label>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <input  id="" type="checkbox" value="boarding" name="accommodation">
                                                                        <label for="checkbox3">
                                                                            Boarding
                                                                        </label>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <input  id="" type="checkbox" value="parent" name="accommodation">
                                                                        <label for="checkbox3">
                                                                            Parent
                                                                        </label>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <input  id="" type="checkbox" value="grandparent" name="accommodation">
                                                                        <label for="checkbox3">
                                                                            GrandParent
                                                                        </label>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <input  id="" type="checkbox" value="sibling" name="accommodation">
                                                                        <label for="checkbox3">
                                                                            Sibling
                                                                        </label>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <input id="" type="checkbox" value="other" name="accommodation">
                                                                        <label for="checkbox3">
                                                                            Other
                                                                        </label>
                                                                    </div>

                                                                </div>
                                                            </div><!--//form-group-->

                                                            //current school
                                                            <hr>

                                                            <h3> Current School </h3>
                                                            <br>
                                                            <div class="form-group">
                                                                <label>Current School</label>
                                                                <input type="text" class="form-control"  name="currentSchool" placeholder="Current School Attended By Student" required>
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Attendance Date</label>
                                                                <input type="text" class="form-control"  name="attendanceDate" placeholder="Attendance Date" required>
                                                            </div>

                                                            <div class="form-group">
                                                                <label>Head teacher</label>
                                                                <input type="text" class="form-control"  name="headTeacher" placeholder="Head Teacher's Name" required>
                                                            </div>

                                                            <div class="form-group">
                                                                <label>Head teacher's Email </label>
                                                                <input type="email" class="form-control"  name="headTeacherEmail" placeholder="Head Teacher's Email" required>
                                                            </div>

                                                            <div class="form-group">
                                                                <label>School Phone</label>
                                                                <input type="number" class="form-control"  name="schoolPhone" placeholder="Phone Number of Someone at the school" required>
                                                            </div>


                                                            {{--<hr>--}}

                                                            {{--<h3> Father's Data </h3>--}}
                                                            {{--<br>--}}


                                                            {{--<div class="row">--}}

                                                            {{--<div class="form-group phone col-md-6">--}}
                                                            {{--<label for="gender">Title</label>--}}
                                                            {{--<select class="form-control" name="fatherTitle" required>--}}
                                                            {{--<option value="">Mr.</option>--}}
                                                            {{--<option value="Male">Dr.</option>--}}
                                                            {{--<option value="Female">Eng.</option>--}}
                                                            {{--<option value="Female">Chief.</option>--}}
                                                            {{--</select>--}}
                                                            {{--</div><!--//form-group-->--}}


                                                            {{--<div class="form-group email col-md-6">--}}
                                                            {{--<label for="email">Name<span class="required">*</span></label>--}}
                                                            {{--<input id="dob" type="text" name="father" class="form-control" placeholder="Fathers Name" required>--}}
                                                            {{--</div><!--//form-group-->--}}

                                                            {{--</div>--}}




                                                            {{--<div class="form-group">--}}
                                                            {{--<label>Fathers Profession </label>--}}
                                                            {{--<input type="text" class="form-control"  name="fatherProfession" placeholder="Father's Profession" required>--}}
                                                            {{--</div>--}}


                                                            {{--<div class="form-group">--}}
                                                            {{--<label>Fathers Industry </label>--}}
                                                            {{--<input type="text" class="form-control"  name="fatherIndustry" placeholder="Father's Industry" required>--}}
                                                            {{--</div>--}}

                                                            {{--<div class="form-group">--}}
                                                            {{--<label>Father Employers Name </label>--}}
                                                            {{--<input type="text" class="form-control"  name="fatherEmployerName" placeholder="Father Employers Name" required>--}}
                                                            {{--</div>--}}

                                                            {{--<div class="form-group">--}}
                                                            {{--<label>Fathers Email </label>--}}
                                                            {{--<input type="text" class="form-control"  name="fatherEmail" placeholder="Father's Email" required>--}}
                                                            {{--</div>--}}


                                                            {{--<div class="form-group">--}}
                                                            {{--<label>Fathers Phone </label>--}}
                                                            {{--<input type="text" class="form-control"  name="fatherPhone" placeholder="Fathers Phone Number" required>--}}
                                                            {{--</div>--}}






                                                            {{--<hr>--}}

                                                            {{--<h3> Mother's Data </h3>--}}
                                                            {{--<br>--}}


                                                            {{--<div class="row">--}}

                                                            {{--<div class="form-group phone col-md-6">--}}
                                                            {{--<label for="gender">Title</label>--}}
                                                            {{--<select class="form-control" name="motherTitle" required>--}}
                                                            {{--<option value="">Mrs.</option>--}}
                                                            {{--<option value="Male">Dr.</option>--}}
                                                            {{--<option value="Female">Eng.</option>--}}
                                                            {{--<option value="Female">Chief.</option>--}}
                                                            {{--</select>--}}
                                                            {{--</div><!--//form-group-->--}}


                                                            {{--<div class="form-group email col-md-6">--}}
                                                            {{--<label for="email">Name<span class="required">*</span></label>--}}
                                                            {{--<input id="dob" type="text" name="mother" class="form-control" placeholder="Mothers Name" required>--}}
                                                            {{--</div><!--//form-group-->--}}

                                                            {{--</div>--}}




                                                            {{--<div class="form-group">--}}
                                                            {{--<label>Mothers Profession </label>--}}
                                                            {{--<input type="text" class="form-control"  name="motherProfession" placeholder="Mother's Profession" required>--}}
                                                            {{--</div>--}}


                                                            {{--<div class="form-group">--}}
                                                            {{--<label>Mothers Industry </label>--}}
                                                            {{--<input type="text" class="form-control"  name="motherIndustry" placeholder="Mother's Industry" required>--}}
                                                            {{--</div>--}}

                                                            {{--<div class="form-group">--}}
                                                            {{--<label>Mother Employers Name </label>--}}
                                                            {{--<input type="text" class="form-control"  name="motherEmployerName" placeholder="Mother Employers Name" required>--}}
                                                            {{--</div>--}}

                                                            {{--<div class="form-group">--}}
                                                            {{--<label>Mother's Email </label>--}}
                                                            {{--<input type="text" class="form-control"  name="motherEmail" placeholder="Mother's Email" required>--}}
                                                            {{--</div>--}}


                                                            {{--<div class="form-group">--}}
                                                            {{--<label>Mothers Phone </label>--}}
                                                            {{--<input type="text" class="form-control"  name="motherPhone" placeholder="Mothers Phone Number" required>--}}
                                                            {{--</div>--}}





                                                            {{--<hr>--}}

                                                            {{--<h3> Relatives's Data </h3>--}}
                                                            {{--<br>--}}



                                                            {{--<div class="form-group">--}}
                                                            {{--<label>First Name </label>--}}
                                                            {{--<input type="text" class="form-control"  name="fname" placeholder="First Name" required>--}}
                                                            {{--</div>--}}


                                                            {{--<div class="form-group">--}}
                                                            {{--<label>Last Name </label>--}}
                                                            {{--<input type="text" class="form-control"  name="sname" placeholder="First Name" required>--}}
                                                            {{--</div>--}}



                                                            {{--<div class="form-group">--}}
                                                            {{--<label> Email</label>--}}
                                                            {{--<input type="email" class="form-control"  name="email" placeholder="Relative's Email" required>--}}
                                                            {{--</div>--}}


                                                            {{--<div class="form-group">--}}
                                                            {{--<label> Phone </label>--}}
                                                            {{--<input type="number" class="form-control"  name="Phone" placeholder="Relative's Phone Number" required>--}}
                                                            {{--</div>--}}

                                                            {{--<div class="form-group">--}}
                                                            {{--<label>Address </label>--}}
                                                            {{--<input type="text" class="form-control"  name="address" placeholder="Relative's Home Address" required>--}}
                                                            {{--</div>--}}

                                                            {{--<div class="form-group">--}}
                                                            {{--<label>Relationship to Child </label>--}}
                                                            {{--<input type="text" class="form-control"  name="relationshipToChild" placeholder="Relationship to child" required>--}}
                                                            {{--</div>--}}














                                                            <div class="form-group">
                                                                <div class=" col-sm-10">
                                                                    <button type="submit" class="btn btn-lg btn-primary">Send</button>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>

                                            </div>
                                            <div role="tabpanel" class="tab-pane" id="messages">
                                                <p>Homo nostrud organic, assumenda labore aesthetic magna delectus mollit. Keytar helvetica VHS salvia yr, vero magna velit sapiente labore stumptown. Vegan fanny pack odio cillum wes anderson 8-bit, sustainable jean shorts beard ut DIY ethical culpa terry richardson biodiesel. Art party scenester stumptown, tumblr butcher vero sint sapiente accusamus tattooed echo park.</p>
                                            </div>
                                            <div role="tabpanel" class="tab-pane" id="settings">
                                                <p>Nesciunt tofu stumptown aliqua, retro synth master cleanse. Mustache cliche tempor, williamsburg carles vegan helvetica. Reprehenderit butcher retro keffiyeh dreamcatcher synth. Cosby sweater eu banh mi, irure terry richardson ex sd. Alip placeat salvia cillum iphone. Seitan alip s cardigan american apparel, butcher voluptate nisi .</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /# column -->
                    {{--<div class="col-lg-6">--}}
                    {{--<div class="card alert">--}}
                    {{--<div class="card-header">--}}
                    {{--<h4>Custom Tab</h4>--}}
                    {{--</div>--}}
                    {{--<div class="card-body">--}}
                    {{--<div class="custom-tab">--}}
                    {{--<ul class="nav nav-tabs" role="tablist">--}}
                    {{--<li role="presentation" class="active"><a href="#1" aria-controls="1" role="tab" data-toggle="tab">Tab 1</a></li>--}}
                    {{--<li role="presentation"><a href="#2" aria-controls="2" role="tab" data-toggle="tab">Tab 2</a></li>--}}
                    {{--<li role="presentation"><a href="#3" aria-controls="3" role="tab" data-toggle="tab">Tab 3</a></li>--}}
                    {{--<li role="presentation"><a href="#4" aria-controls="4" role="tab" data-toggle="tab">Tab 4</a></li>--}}
                    {{--</ul>--}}
                    {{--<div class="tab-content">--}}
                    {{--<div role="tabpanel" class="tab-pane active" id="1">--}}
                    {{--<p>Raw denim you probably haven't heard of them jean shorts Austin. Nesciunt tofu stumptown aliqua, retro synth master cleanse. Mustache cliche tempor, williamsburg carles vegan helvetica. Reprehenderit butcher retro keffiyeh dreamcatcher synth. Cosby sweater eu banh mi, irure terry richardson ex sd. Alip placeat salvia cillum iphone. Seitan alip s cardigan american apparel, butcher voluptate nisi .</p>--}}
                    {{--</div>--}}
                    {{--<div role="tabpanel" class="tab-pane" id="2">--}}
                    {{--<p>Food truck fixie locavore, accusamus mcsweeney's marfa nulla single-origin coffee sd. Exercitation +1 labore velit, blog sartorial PBR leggings next level wes anderson artisan four loko farm-to-table craft beer twee. photo booth letterpress, commodo enim craft beer mlkshk alip jean shorts ullamco ad vinyl cillum PBR.</p>--}}
                    {{--</div>--}}
                    {{--<div role="tabpanel" class="tab-pane" id="3">--}}
                    {{--<p>Homo nostrud organic, assumenda labore aesthetic magna delectus mollit. Keytar helvetica VHS salvia yr, vero magna velit sapiente labore stumptown. Vegan fanny pack odio cillum wes anderson 8-bit, sustainable jean shorts beard ut DIY ethical culpa terry richardson biodiesel. Art party scenester stumptown, tumblr butcher vero sint sapiente accusamus tattooed echo park.</p>--}}
                    {{--</div>--}}
                    {{--<div role="tabpanel" class="tab-pane" id="4">--}}
                    {{--<p>Nesciunt tofu stumptown aliqua, retro synth master cleanse. Mustache cliche tempor, williamsburg carles vegan helvetica. Reprehenderit butcher retro keffiyeh dreamcatcher synth. Cosby sweater eu banh mi, irure terry richardson ex sd. Alip placeat salvia cillum iphone. Seitan alip s cardigan american apparel, butcher voluptate nisi .</p>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    <!-- /# column -->
                    </div>
                    <!-- /# row -->

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="footer">
                                <p>This dashboard was generated on <span id="date-time"></span> <a href="#" class="page-refresh">Refresh Dashboard</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection