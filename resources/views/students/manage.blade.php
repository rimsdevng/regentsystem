<?php use Illuminate\Support\Facades\Input; ?>
@extends('layouts.admin')
@section('content')
    <div class="content-wrap">
        <div class="main">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-8 p-r-0 title-margin-right">
                        <div class="page-header">
                            <div class="page-title">
                                <h1>Dashboard</h1>
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                    <div class="col-lg-4 p-l-0 title-margin-left">
                        <div class="page-header">
                            <div class="page-title">
                                <ol class="breadcrumb text-right">
                                    <li><a href="#">Dashboard</a></li>
                                    <li class="active">Students</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                </div>
                <!-- /# row -->
                <div id="main-content">
                    @include('notification')
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card alert">
                                <div class="card-header pr">
                                    <h4>Students </h4>
                                    <div class="search-action">
                                        <div class="search-type dib">
                                            <form>
                                                <input class="form-control input-rounded" name="name" value="{{Input::get('name')}}" placeholder="Search" type="text">
                                            </form>

                                        </div>
                                    </div>

                                </div>
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table class="table table-hover student-data-table m-t-20">
                                            <thead>
                                            <tr>
                                                <th>S/N</th>
                                                <th>Names</th>
                                                <th>Gender</th>
                                                <th>Department</th>
                                                <th>Created</th>
                                                <th></th>

                                            </tr>
                                            </thead>
                                            <tbody>

                                            @if(count($students)>0)

												<?php $count = $from; ?>

                                                @foreach($students as $student)
                                                    <tr>

                                                        <td>
                                                            <?php echo $count;?>
                                                        </td>
                                                        <td>
                                                            {{$student->fname}} {{$student->sname}}
                                                        </td>
                                                        <td>
                                                            {{$student->gender}}
                                                        </td>
                                                        <td>
                                                            @if(isset($student->Class))
                                                            {{$student->Class->name}}
                                                                @else
                                                            No Department Assigned
                                                            @endif
                                                        </td>
                                                        <td>
                                                            {{$student->created_at->toDayDateTimeString()}}
                                                        </td>


                                                        <td class="actions">
                                                            <span><a href="{{url('student/'.$student->sid.'/detail')}}"><i class="ti-eye color-default"></i></a> </span>
                                                            <span><a href="{{url('student/'.$student->sid.'/edit')}}"><i class="ti-pencil-alt color-success"></i></a></span>
                                                            <span><a class="btn btn-danger" href="{{url('student/'.$student->sid.'/delete')}}">Delete</a></span>
                                                        </td>
                                                    </tr>
													<?php $count ++; ?>
                                                @endforeach
                                            @else

                                                <tr>
                                                    <td style="color: silver; text-align: center; margin-top: 30px;"> There are no Students yet </td>
                                                </tr>

                                            @endif




                                            </tbody>
                                        </table>

                                        <div class="pull-right">
                                            {{$students->links()}}

                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /# column -->

                    </div>
                    <!-- /# row -->

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="footer">
                                <p>This dashboard was generated on <span id="date-time"></span> <a href="#" class="page-refresh">Refresh Dashboard</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection