<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Regent Project System </title>

    <!-- ================= Favicon ================== -->

    <!-- Standard -->
    <link rel="shortcut icon" href="{{url('assets/images/logo.png')}}">
    <!-- Retina iPad Touch Icon-->
    <link rel="apple-touch-icon" sizes="144x144" href="{{url('admin/assets/images/logo.png')}}">
    <!-- Retina iPhone Touch Icon-->
    <link rel="apple-touch-icon" sizes="114x114" href="{{url('admin/assets/images/logo.png')}}">
    <!-- Standard iPad Touch Icon-->
    <link rel="apple-touch-icon" sizes="72x72" href="{{url('admin/assets/images/logo.png')}}">
    <!-- Standard iPhone Touch Icon-->
    <link rel="apple-touch-icon" sizes="57x57" href="{{url('admin/assets/images/logo.png')}}">
    <!-- ================= End Favicon ================== -->

    <style>
        .logoImage{
            width: 100%;
            padding:40px;
        }

        .select2 {
            max-width:400px;
        }

        .content-wrap {
            min-height: 500px !important;
             
        }

    </style>


    <!-- Styles -->
    <link href=" {{url('admin/assets/css/lib/chartist/chartist.min.css')}}" rel="stylesheet">
    <link href="{{url('admin/assets/css/lib/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{url('admin/assets/css/lib/themify-icons.css')}}" rel="stylesheet">
    <link href="{{url('admin/assets/css/lib/owl.carousel.min.css')}}" rel="stylesheet" />
    <link href="{{url('admin/assets/css/lib/owl.theme.default.min.css')}}" rel="stylesheet" />
    <link href="{{url('admin/assets/css/lib/weather-icons.css')}}" rel="stylesheet" />
    <link href="{{url('admin/assets/css/lib/menubar/sidebar.css')}}" rel="stylesheet">
    <link href="{{url('admin/assets/css/lib/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{url('admin/assets/css/lib/unix.css')}}" rel="stylesheet">
    <link href="{{url('admin/assets/css/lib/calendar2/pignose.calendar.min.css')}}" rel="stylesheet">

    <link href="{{url('admin/assets/css/style.css')}}" rel="stylesheet">

    <script src="{{url('admin/assets/js/jquery.min.js')}}"></script>

    <link rel="stylesheet" href="{{url('admin/assets/css/jquery-ui.min.css')}}">
    <script type="text/javascript" src="{{url('admin/assets/js/jquery-ui.min.js')}}"></script>

    <link href="https://cdnjs.cloudflare.com/ajax/libs/jquery-ui-timepicker-addon/1.6.3/jquery-ui-timepicker-addon.min.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-ui-timepicker-addon/1.6.3/jquery-ui-timepicker-addon.min.js"></script>

    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
</head>

<body>




<div class="sidebar sidebar-hide-to-small sidebar-shrink sidebar-gestures">
    <div class="nano">

        @include('navs.left')
    </div>
</div>
<!-- /# sidebar -->
{{--  change notification color, dob, work on the parents data   --}}
{{--  #6B8DD6 28%, - The former color  --}}


<div class="header" style="background-image: linear-gradient(-225deg, #3c763d 0%, #dc3022 30%);">
    <div class="pull-left">

        <div class="hamburger sidebar-toggle">
            <span class="line" style="color: white"></span>
            <span class="line" style="color: white"></span>
            <span class="line" style="color: white"></span>
        </div>
    </div>
    <div class="pull-right p-r-15">
        <ul>
            {{--<li class="header-icon dib"><a href="#search"><i class="ti-search"></i></a></li>--}}

            <li class="header-icon dib"><img class="avatar-img" src="{{url('assets/images/avatar/1.jpg')}}" alt="" /> <span class="user-avatar">

                    @if(auth()->check())
                        {{auth()->user()->name}}
                    @endif
                        <i class="ti-angle-down f-s-10"></i></span>
                <div class="drop-down dropdown-profile">

                    {{--<div class="dropdown-content-heading">--}}
                        {{--<span class="text-left">Upgrade Now</span>--}}
                        {{--<p class="trial-day">30 Days Trail</p>--}}
                    {{--</div>--}}

                    <div class="dropdown-content-body">
                        <ul>
                            <li><a style="color:black !important;" href="{{url('logout')}}"><i class="ti-power-off"></i> <span>Logout</span></a></li>
                        </ul>
                    </div>
                </div>
            </li>
        </ul>
    </div>
</div>

@yield('content')

<script>
    // In your Javascript (external .js resource or <script> tag)
    $(document).ready(function() {
        $('select').select2();
    });
</script>


<!-- jquery vendor -->
<script src="{{url('admin/assets/js/lib/jquery.nanoscroller.min.js')}}"></script>
<!-- nano scroller -->
<script src="{{url('admin/assets/js/lib/menubar/sidebar.js')}}"></script>
<script src="{{url('admin/assets/js/lib/preloader/pace.min.js')}}"></script>
<!-- sidebar -->
<script src="{{url('admin/assets/js/lib/bootstrap.min.js')}}"></script>
<!-- bootstrap -->
<script src="{{url('admin/assets/js/lib/weather/jquery.simpleWeather.min.js')}}"></script>
<script src="{{url('admin/assets/js/lib/weather/weather-init.js')}}"></script>
<script src="{{url('admin/assets/js/lib/circle-progress/circle-progress.min.js')}}"></script>
<script src="{{url('admin/assets/js/lib/circle-progress/circle-progress-init.js')}}"></script>
<script src="{{url('admin/assets/js/lib/chartist/chartist.min.js')}}"></script>
<script src="{{url('admin/assets/js/lib/chartist/chartist-init.js')}}"></script>
<script src="{{url('admin/assets/js/lib/sparklinechart/jquery.sparkline.min.js')}}"></script>
<script src="{{url('admin/assets/js/lib/sparklinechart/sparkline.init.js')}}"></script>
<script src="{{url('admin/assets/js/lib/owl-carousel/owl.carousel.min.js')}}"></script>
<script src="{{url('admin/assets/js/lib/owl-carousel/owl.carousel-init.js')}}"></script>
<script src="{{url('admin/assets/js/scripts.js')}}"></script>




<script src="{{url('admin/assets/js/lib/calendar-2/moment.latest.min.js')}}"></script>
<!-- scripit init-->
<script src="{{url('admin/assets/js/lib/calendar-2/semantic.ui.min.js')}}"></script>
<!-- scripit init-->
<script src="{{url('admin/assets/js/lib/calendar-2/prism.min.js')}}"></script>
<!-- scripit init-->
<script src="{{url('admin/assets/js/lib/calendar-2/pignose.calendar.min.js')}}"></script>
<!-- scripit init-->
<script src="{{url('admin/assets/js/lib/calendar-2/pignose.init.js')}}"></script>
<!-- scripit init-->




</body>

</html>