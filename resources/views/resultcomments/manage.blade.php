@extends('layouts.admin')
@section('content')




    <div class="content-wrap">
        <div class="main">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-8 p-r-0 title-margin-right">
                        <div class="page-header">
                            <div class="page-title">
                                <h1>Dashboard</h1>
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                    <div class="col-lg-4 p-l-0 title-margin-left">
                        <div class="page-header">
                            <div class="page-title">
                                <ol class="breadcrumb text-right">
                                    <li><a href="#">Dashboard</a></li>
                                    <li class="active">Result Comments</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                </div>
                <!-- /# row -->
                @include('notification')

                <div class="row">
                    <div class="col-md-12">
                        <a href="{{url('admin/result/comment/add')}}" class="btn btn-success padding-overlay pull-right"> Add Comment </a>
                    </div>
                </div>

                <div id="main-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card alert">
                                <div class="card-header pr">
                                    <h4>Comments </h4>
                                    <div class="search-action">
                                        <div class="search-type dib">
                                            <input class="form-control input-rounded" placeholder="Search" type="text">
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table class="table student-data-table m-t-20">
                                            <thead>
                                            <tr>
                                                <th>S/N</th>
                                                <th>Comment</th>
                                                <th>Created</th>
                                                <th></th>

                                            </tr>
                                            </thead>
                                            <tbody>

                                            @if(count($comments)>0)

												<?php $count = 1; ?>

                                                @foreach($comments as $comment)
                                                    <tr>

                                                        <td>
                                                            #<?php echo $count;?>
                                                        </td>
                                                        <td>
                                                            {{$comment->comment}}
                                                        </td>
                                                        <td>
                                                            {{$comment->created_at->toDayDateTimeString()}}
                                                        </td>

                                                        <td>
                                                            {{--<span><a class="btn btn-success" href="{{url('admin/result/comment/'.$comment->rcid)}}"><i class="ti-eye color-default"></i></a> </span>--}}
                                                            <span><a class="btn btn-danger" href="{{url('admin/result/comment/'.$comment->rcid.'/delete')}}">DELETE</a></span>
                                                        </td>
                                                    </tr>
													<?php $count ++; ?>
                                                @endforeach
                                            @else

                                                <tr>
                                                    <td colspan="5" style="color: silver; text-align: center; margin-top: 30px;"> There are no comments </td>
                                                </tr>

                                            @endif


                                            </tbody>
                                        </table>

                                        <div class="pull-right">
                                            {{$comments->links()}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /# column -->

                    </div>
                    <!-- /# row -->

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="footer">
                                <p>This dashboard was generated on <span id="date-time"></span> <a href="#" class="page-refresh">Refresh Dashboard</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection