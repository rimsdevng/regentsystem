<?php use Carbon\Carbon; ?>
@extends('dashboard.staff.layouts.app')
@section('content')




    <div class="content-wrap">
        <div class="main">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-8 p-r-0 title-margin-right">
                        <div class="page-header">
                            <div class="page-title">
                                <h1>Dashboard</h1>
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                    <div class="col-lg-4 p-l-0 title-margin-left">
                        <div class="page-header">
                            <div class="page-title">
                                <ol class="breadcrumb text-right">
                                    <li><a href="#">Dashboard</a></li>
                                    <li class="active">Project Assignments</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                </div>
                <!-- /# row -->
                @include('notification')

                <div id="main-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card alert">
                                <div class="card-body">

                                    {{--@if(empty($test->deadline) || ( !empty($test->deadline) && Carbon::now() < $test->deadline ) )--}}
                                        {{--<a href="{{url('upload-questions/' . $test->testid)}}" class="btn btn-primary pull-right">Upload Questions</a>--}}
                                    {{--@endif--}}

                                    <a href="{{url('staff/assignment/' . $assignment->aid . '/delete')}}" class="btn btn-danger pull-right" style="margin-right: 10px;">Delete Project Assignment</a>


                                    <p class="page-title">Assignment - {{$assignment->name}}</p>
                                    <p>

                                        Description - {{$assignment->description}} <br>
                                        Subject - {{$assignment->Subject->name}} <br>
                                        Due Date - <b>{{Carbon::createFromFormat("Y-m-d H:i:s",$assignment->due)->toDayDateTimeString()}}</b><br> <br>

                                        <a href="{{$assignment->url}}" class="btn btn-success">Download Attached File</a>

                                    </p>
                                </div>
                            </div>

                            {{--<div class="card alert">--}}
                            {{--<div class="card-body" align="center">--}}

                            {{--<form class="form-inline" method="post" action="{{url('add-focus-subject')}}">--}}
                            {{--@csrf--}}
                            {{--<input type="hidden" name="fid" value="{{$focus->fid}}" >--}}
                            {{--<div class="form-group">--}}
                            {{--<label >Add Subject</label>--}}
                            {{--<select class="form-control" name="subid" style="width: 200px;">--}}
                            {{--@foreach($subjects as $subject)--}}
                            {{--<option value="{{$subject->subid}}">{{$subject->name}}</option>--}}
                            {{--@endforeach--}}
                            {{--</select>--}}
                            {{--</div>--}}

                            {{--<button class="btn btn-success">Add</button>--}}
                            {{--</form>--}}

                            {{--</div>--}}
                            {{--</div>--}}


                            <div class="card alert">
                                <div class="card-body">

                                    <h6>Submissions</h6>

                                    <div class="table-responsive">
                                        <table class="table student-data-table m-t-20">
                                            <thead>
                                            <tr>
                                                <th>S/N</th>
                                                <th>Student</th>
                                                <th>Date Submitted</th>
                                                <th></th>

                                            </tr>
                                            </thead>
                                            <tbody>


                                            <?php $count = 1; ?>
                                            @foreach($assignment->Submission as $submission)
                                                <tr>
                                                    <td>{{$count}}</td>
                                                    <td>
                                                        {{$submission->Student->fname}} {{$submission->Student->sname}}
                                                    </td>
                                                    <td>
                                                        {{$submission->created_at->toDayDateTimeString()}}
                                                    </td>
                                                    <td>
                                                        <a href="{{url($submission->url)}}" class="btn btn-success">DOWNLOAD</a>
                                                    </td>
                                                </tr>
                                                <?php $count++; ?>
                                            @endforeach

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /# column -->

                    </div>
                    <!-- /# row -->

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="footer">
                                <p>This dashboard was generated on <span id="date-time"></span> <a href="#" class="page-refresh">Refresh Dashboard</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection
