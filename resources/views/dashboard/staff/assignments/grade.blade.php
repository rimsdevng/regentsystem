<?php use Carbon\Carbon; ?>
@extends('dashboard.student.layouts.app')
@section('content')


    <div class="content-wrap">
        <div class="main">
            @include('notification')
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-8 p-r-0 title-margin-right">
                        <div class="page-header">
                            <div class="page-title">
                                <h1>Dashboard</h1>
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                    <div class="col-lg-4 p-l-0 title-margin-left">
                        <div class="page-header">
                            <div class="page-title">
                                <ol class="breadcrumb text-right">
                                    <li><a href="#">Dashboard</a></li>
                                    <li class="active">Grade Project</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                </div>
                <!-- /# row -->



               
                <!-- /# row -->



                <div id="main-content">
                   





                    <div class="row">
                        
                        <!-- /# column -->
                        <div class="col-lg-12">
                            <div class="card alert">
                                <div class="card-header">
                                    <h4>Grade Project</h4>
                                    <div class="card-header-right-icon">
                                        <ul>
                                            <li class="card-close" data-dismiss="alert"><i class="ti-close"></i></li>
                                            <li class="doc-link"><a href="#"><i class="ti-link"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="horizontal-form-elements">
                                        <form class="form-horizontal" enctype="multipart/form-data" method="POST" action="{{ url('staff/assignment/postGrade/'. $submission->asid) }}">
                                                {{csrf_field()}}
                                            <div class="row">
                                                <div class="col-lg-8">
                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label">Project Topic:</label>
                                                        <div class="col-sm-10">
                                                            <input type="text" class="form-control" name="name" value="{{ $submission->assignment->name }}" >
                                                        </div>
                                                    </div>
                                               
                                                    <div class="form-group">
                                                            <label class="col-sm-2 control-label">Project Description:</label>
                                                            <div class="col-sm-10">
                                                                <input type="text" class="form-control" name="description" value="{{ $submission->description }}" >
                                                            </div>
                                                        </div>

                                                    <div class="form-group">
                                                            <label class="col-sm-2 control-label">Grade:</label>
                                                            <div class="col-sm-10">
                                                                <input type="text" class="form-control" name="grade" placeholder="Grade" maxlength="3">
                                                            </div>
                                                        </div>
                                                       
                                                        <div class="form-group">
                                                                <label class="col-sm-2 control-label">Comment</label>
                                                                <div class="col-sm-10">
                                                                    <textarea class="form-control" rows="2" name="comment" placeholder="Comment"></textarea>
                                                                </div>
                                                            </div>
                                                
                                                    {{--  
                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label">Readonly</label>
                                                        <div class="col-sm-10">
                                                            <input class="form-control" type="text" value="Readonly value" readonly="">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label">Disabled</label>
                                                        <div class="col-sm-10">
                                                            <input class="form-control" type="text" value="Disabled value" disabled="">
                                                        </div>
                                                    </div>  --}}
                                                </div>
                                    
                                                <!-- /# column -->
                                                <button class="btn btn-info pull-right" type="submit">Submit Synopsis</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!-- /# card -->
                        </div>
                        <!-- /# column -->
                    </div>


                </div>
            </div>
        </div>
    </div>



@endsection