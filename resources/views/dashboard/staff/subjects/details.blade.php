<?php use Carbon\Carbon; ?>
@extends('dashboard.staff.layouts.app')
@section('content')


    <div class="content-wrap">
        <div class="main">
            @include('notification')
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-8 p-r-0 title-margin-right">
                        <div class="page-header">
                            <div class="page-title">
                                <h1>Dashboard</h1>
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                    <div class="col-lg-4 p-l-0 title-margin-left">
                        <div class="page-header">
                            <div class="page-title">
                                <ol class="breadcrumb text-right">
                                    <li><a href="#">Dashboard</a></li>
                                    <li class="active">Project Detail</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                </div>
                <!-- /# row -->



                <div class="row">
                    <div class="col-md-8 col-md-offset-1">


                        <div class="col-md-3 pull-left">
                            {{--                            <a href="{{url('student/'.$student->sid.'/edit')}}" class="btn btn-info padding-overlay"> Update record </a>--}}
                        </div>

                        <div class="col-md-3">
                            {{--<a href="{{url('admit-student/'.$application->apid)}}" class="btn btn-success padding-overlay"> Admit Student</a>--}}
                        </div>

                        <div class="col-md-3">
                            {{--                            <a href="{{url('make-student/'.$application->apid)}}" class="btn btn-danger padding-overlay pull-right"> Admit </a>--}}
                        </div>

                    </div>

                </div>
                <!-- /# row -->



                <div id="main-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card alert">
                                <div class="card-header">
                                    <h4>Project Details - {{$subject->name}}</h4>

                                </div>
                                <div class="card-body">
                                    <div class="user-profile m-t-15">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="user-profile-name dib">Supervisor </div>
                                                <div class="useful-icon dib pull-right">
                                                    <span><a href="#" class="btn btn-danger"><i class="ti-pencil-alt"></i></a> </span>
                                                </div>


                                                            <div class="contact-information col-12">
                                                                <div class="col-md-6">

                                                                    <div class="phone-content">
                                                                        <span class="contact-title"> Name:</span>
                                                                        <span class="phone-number">   {{$subject->Teacher->fname}}  {{$subject->Teacher->sname}}

			                                                                <?php

			                                                                if(!empty($subject->cids)){
				                                                                $cids = json_decode($subject->cids);

				                                                                $assignedClasses = "(";
				                                                                $countCids = count($cids);

				                                                                $count = 1;
				                                                                foreach ($cids as $cid){
					                                                                $assignedClasses .= \App\classes::find($cid)->name ;

					                                                                if($countCids != $count) $assignedClasses .= ",";

					                                                                $count++;
				                                                                }

				                                                                $assignedClasses .= ")";

				                                                                echo $assignedClasses;
			                                                                }

			                                                                ?>
                                                                    </span>

                                                                    </div>

                                                                    <div class="phone-content">
                                                                        <span class="contact-title"> Phone:</span>
                                                                        <span class="phone-number">{{$subject->Teacher->phone}} </span>
                                                                    </div>
                                                                    <div class="website-content">
                                                                        <span class="contact-title">Email:</span>
                                                                        <span class="contact-website">{{$subject->Teacher->email}}</span>
                                                                    </div>

                                                                </div>


                                                                <!-- show extra subject teachers -->
                                                                @foreach($subject->Teachers as $teacher)
                                                                    <div class="col-md-6">

                                                                        <div class="phone-content">
                                                                            <span class="contact-title"> Name:</span>
                                                                            <span class="phone-number">   {{$teacher->fname}}  {{$teacher->sname}}

			                                                                    <?php


			                                                                    $st = \App\subjectTeacher::where('stid',$teacher->stid)->where('subid',$subject->subid)->first();
			                                                                    if(!empty($st->cids)){
				                                                                    $cids = json_decode($st->cids);

				                                                                    $assignedClasses = "(";
				                                                                    $countCids = count($cids);

				                                                                    $count = 1;
				                                                                    foreach ($cids as $cid){
					                                                                    $assignedClasses .= \App\classes::find($cid)->name ;

					                                                                    if($countCids != $count) $assignedClasses .= ",";

					                                                                    $count++;
				                                                                    }

				                                                                    $assignedClasses .= ")";

				                                                                    echo $assignedClasses;
			                                                                    }

			                                                                    ?>
                                                                        </span>
                                                                        </div>

                                                                        <div class="phone-content">
                                                                            <span class="contact-title"> Phone:</span>
                                                                            <span class="phone-number">{{$subject->Teacher->phone}} </span>
                                                                        </div>
                                                                        <div class="website-content">
                                                                            <span class="contact-title">Email:</span>
                                                                            <span class="contact-website">{{$subject->Teacher->email}}</span>
                                                                        </div>



                                                                    </div>
                                                                @endforeach

                                                            </div>


                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /# column -->
                    </div>


                    <div class="card">

                        <div class="default-tab">

                            <ul class="nav nav-tabs" role="tablist">
                                {{--  <li role="presentation" class="active"><a href="#updates" aria-controls="updates"
                                                                          role="tab" data-toggle="tab">Updates</a>
                                </li>
                                <li role="presentation">
                                    <a href="#tests" aria-controls="tests" role="tab" data-toggle="tab">Tests</a>
                                </li>  --}}
                                <li role="presentation" class="active">
                                    <a href="#assignments" aria-controls="assignments" role="tab" data-toggle="tab">Projects</a>
                                </li>
                                <li role="presentation">
                                    <a href="#files" aria-controls="files" role="tab" data-toggle="tab"> Files</a>
                                </li>
                                <li role="presentation">
                                    <a href="#students" aria-controls="students" role="tab" data-toggle="tab">Students</a>
                                </li>
                            </ul>

                            <!-- Tabs -->
                            <div class="tab-content">

                                <!-- Update Tab -->
                             
                                <!-- Tests Tab -->
                             

                                <!-- Assignments Tab -->
                                <div role="tabpanel" class="tab-pane" id="assignments">

                                    <table class="table student-data-table m-t-20">
                                        <thead>
                                        <tr>
                                            <th>S/N</th>
                                            <th>Name</th>
                                            <th>Description</th>
                                            <th>Department</th>
                                            <th>Due</th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        @if(count($assignments)>0)
						                    <?php $count = 1; ?>
                                            @foreach($assignments as $assignment)
                                                <tr>

                                                    <td>
                                                        <?php echo $count; ?>
                                                    </td>
                                                    <td>
                                                        {{$assignment->name}}
                                                    </td>
                                                    <td>
                                                        {{str_limit($assignment->description,300,'...')}}
                                                    </td>
                                                    <td>
                                                        {{$assignment->Class->name}}
                                                    </td>
                                                    <td>
                                                        {{$assignment->due->toDayDateTimeString()}}
                                                    </td>

                                                    <td>
                                                        <span><a href="{{url('staff/assignment/'. $assignment->aid )}}">View</a> </span>
                                                    </td>
                                                </tr>
							                    <?php $count ++; ?>
                                            @endforeach
                                        @else

                                            <tr>
                                                <td colspan="5" style="color: silver; text-align: center; margin-top: 30px;"> There are no tests </td>
                                            </tr>

                                        @endif




                                        </tbody>
                                    </table>

                                </div>

                                <!-- Files Tab -->
                               
                                <!-- end files tab -->

                                <!-- Students -->
                                <div role="tabpanel" class="tab-pane" id="students">

                                    <table class="table table-hover student-data-table m-t-20">
                                        <thead>
                                        <tr>
                                            <th>S/N</th>
                                            <th>Name</th>
                                            <th>Gender</th>
                                            <th>Department</th>
                                            {{--<th>Created</th>--}}
                                            <th></th>

                                        </tr>
                                        </thead>
                                        <tbody>

                                        @if(count($subject->Students)>0)

			                                <?php $count = 1; ?>

                                            @foreach($subject->Students as $student)
                                                <tr>
                                                    <td>
                                                        <?php echo $count;?>
                                                    </td>
                                                    <td>
                                                        {{strtoupper($student->fname)}} {{strtoupper($student->sname)}}
                                                    </td>
                                                    <td>
                                                        {{$student->gender}}
                                                    </td>
                                                    <td>
                                                        @if(isset($student->Class))
                                                            {{$student->Class->name}}
                                                        @else
                                                            No Class Assigned
                                                        @endif
                                                    </td>
                                                    {{--<td>--}}
{{--                                                        {{$student->created_at->toDayDateTimeString()}}--}}
                                                    {{--</td>--}}


                                                    {{--<td class="actions">--}}
                                                        {{--<span><a href="{{url('student/'.$student->sid.'/detail')}}"><i class="ti-eye color-default"></i></a> </span>--}}
                                                    {{--</td>--}}
                                                </tr>
				                                <?php $count ++; ?>
                                            @endforeach
                                        @else

                                            <tr>
                                                <td style="color: silver; text-align: center; margin-top: 30px;"> There are no Students yet </td>
                                            </tr>

                                        @endif




                                        </tbody>
                                    </table>


                                </div> <!-- end students tab -->

                            </div>
                        </div>
                    </div>


                    <!-- /# row -->
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="footer">
                                <p>This dashboard was generated on <span id="date-time"></span> <a href="#" class="page-refresh">Refresh Dashboard</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection
