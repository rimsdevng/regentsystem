@extends('dashboard.staff.layouts.app')
@section('content')
    <div class="content-wrap">
        <div class="main">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-8 p-r-0 title-margin-right">
                        <div class="page-header">
                            <div class="page-title">
                                <h1>Dashboard</h1>
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                    <div class="col-lg-4 p-l-0 title-margin-left">
                        <div class="page-header">
                            <div class="page-title">
                                <ol class="breadcrumb text-right">
                                    <li><a href="#">Dashboard</a></li>
                                    <li class="active">Bills</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                </div>
                <!-- /# row -->
                @include('notification')

                <div id="main-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card alert">
                                <div class="card-header pr">
                                    <h4>Bills </h4>
                                    <div class="search-action">
                                        <div class="search-type dib">
                                            <input class="form-control input-rounded" placeholder="search" type="text">
                                        </div>
                                    </div>
                                    <div class="card-header-right-icon">
                                        <ul>
                                            {{--<li class="card-close" data-dismiss="alert"><i class="ti-close"></i></li>--}}
                                            <li class="card-option drop-menu"><i class="ti-settings" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" role="link"></i>
                                                <ul class="card-option-dropdown dropdown-menu">
                                                    <li><a href="#"><i class="ti-loop"></i> Update data</a></li>
                                                    <li><a href="#"><i class="ti-menu-alt"></i> Detail log</a></li>
                                                    <li><a href="#"><i class="ti-pulse"></i> Statistics</a></li>
                                                    <li><a href="#"><i class="ti-power-off"></i> Clear ist</a></li>
                                                </ul>
                                            </li>
                                            {{--<li class="doc-link"><a href="#"><i class="ti-link"></i></a></li>--}}
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table class="table student-data-table m-t-20">
                                            <thead>
                                            <tr>
                                                <th>S/n</th>
                                                <th>Title</th>
                                                <th>Content</th>
                                                <th>Created</th>
                                                <th>Action</th>

                                            </tr>
                                            </thead>
                                            <tbody>

                                            @if(count($bills)>0)

												<?php $count = 1; ?>

                                                @foreach($bills as $bill)
                                                    <tr>
                                                        {{--<td><label><input type="checkbox" value=""></label>#2901</td>--}}
                                                        {{--<td>--}}
                                                        {{--<div class="round-img">--}}
                                                        {{--<a href="#"><img src="assets/images/avatar/1.jpg" alt=""></a>--}}
                                                        {{--</div>--}}
                                                        {{--</td>--}}
                                                        <td>
                                                            #<?php echo $count;?>
                                                        </td>
                                                        <td>
                                                            {{$bill->name}}
                                                        </td>
                                                        <td>
                                                            {{$bill->due}}
                                                        </td>
                                                        <td>
                                                            {{str_limit($bill->description,100)}}
                                                        </td>
                                                        <td>
                                                            &#8358 {{number_format($bill->amount)}}
                                                        </td>
                                                        <td>
                                                            {{$bill->created_at->diffForHumans()}}
                                                        </td>
                                                        <td>
                                                            {{--<span><a href="{{url('notification/'.$notification->nid.'/detail')}}"><i class="ti-eye color-default"></i></a> </span>--}}
                                                            {{--<span><a href="{{url('notification/'.$subjects->subid.'/edit')}}"><i class="ti-pencil-alt color-success"></i></a></span>--}}
                                                            {{--<span><a href="{{url('notification/'.$subjects->subid.'/delete')}}"><i class="ti-trash color-danger"></i> </a></span>--}}
                                                        </td>
                                                    </tr>
													<?php $count ++; ?>
                                                @endforeach
                                            @else
                                                <h3 style="color: silver; text-align: center; margin-top: 30px;"> There Are No Bills Yet</h3>
                                            @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /# column -->
                    </div>
                    <!-- /# row -->
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="footer">
                                <p>This dashboard was generated on <span id="date-time"></span> <a href="#" class="page-refresh">Refresh Dashboard</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection