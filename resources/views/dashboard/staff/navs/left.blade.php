<div class="nano-content">
    <ul>
        <li>
            <img class="img logoImage" src="{{url('admin/assets/images/logo.png')}}"><br>
        </li>

        <li class="label">Home</li>
        <li><a href="{{url('/staff/dashboard')}}"><i class="ti-view-list-alt"></i> DashBoard</a></li>


        {{--  @if(session()->get('staff')->category == "Teaching" || session()->get('staff')->category == "Academic")  --}}


        <li class="label">Projects and Departments</li>

        <li><a href="{{url('staff/subjects')}}"><i class="ti-view-list-alt"></i> Students Projects</a></li>

        <li><a href="{{url('staff/classes')}}"><i class="ti-view-list-alt"></i> Departments</a></li>


        <li class="label">Project Submission</li>

        {{--  <li>
            <a class="sidebar-sub-toggle"><i class="ti-pencil-alt"></i> Results <span class="sidebar-collapse-icon ti-angle-down"></span></a>
            <ul>
                <li><a href="{{url('/add-result/')}}">Add</a></li>
                <li><a href="{{url('/upload-result')}}">Upload</a></li>
            </ul>
        </li>  --}}

        {{--  <li class="label">Tests and Assignments</li>
        <li>
            <a class="sidebar-sub-toggle"><i class="ti-pencil-alt"></i> Tests <span class="sidebar-collapse-icon ti-angle-down"></span></a>
            <ul>
                <li><a href="{{url('staff/add-test')}}">Add</a></li>
                <li><a href="{{url('staff/tests')}}">Manage</a></li>
            </ul>
        </li>  --}}

        <li>
            <a class="sidebar-sub-toggle"><i class="ti-pencil-alt"></i>Project <span class="sidebar-collapse-icon ti-angle-down"></span></a>
            <ul>
                <li><a href="{{url('/staff/assignments/add')}}">Allow Submission/Set Formart</a></li>
                <li><a href="{{url('/staff/assignments')}}">Manage</a></li>
            </ul>
        </li>

        {{--  @endif  --}}

        <li class="label">Cloud Files</li>
        <li>
            <a class="sidebar-sub-toggle"><i class="ti-pencil-alt"></i> Files <span class="sidebar-collapse-icon ti-angle-down"></span></a>
            <ul>
                <li><a href="{{url('staff/files/add')}}">Add</a></li>
                <li><a href="{{url('staff/files')}}">Manage</a></li>
            </ul>
        </li>


    @if(session()->get('staff')->designation->name == "Admin. Front Desk")
        <li class="label">COMMUNICATIONS</li>

        <li>
            <a class="sidebar-sub-toggle"><i class="ti-pencil-alt"></i> News <span class="sidebar-collapse-icon ti-angle-down"></span></a>
            <ul>
                <li><a href="{{url('staff/news/add')}}">Add</a></li>
                <li><a href="{{url('staff/news')}}">Manage</a></li>
            </ul>
        </li>

        @endif

        @if(session()->get('staff')->designation->name == "Nurse")
        <li class="label">Health and SickBay</li>

        <li>
            <a class="sidebar-sub-toggle"><i class="ti-pencil-alt"></i> Records <span class="sidebar-collapse-icon ti-angle-down"></span></a>
            <ul>
                <li><a href="{{url('sickbay/students')}}">SickBay</a></li>
                <li><a href="{{url('/upload-result')}}">Upload Result sheets</a></li>
            </ul>
        </li>

        @endif

        @if(session()->get('staff')->designation->name == "Bursar")

        <li class="label">Bills and Payment</li>
        <li>
            <a class="sidebar-sub-toggle"><i class="ti-pencil-alt"></i> Bills <span class="sidebar-collapse-icon ti-angle-down"></span></a>
            <ul>
                <li><a href="{{url('add-bill')}}">Add A Bill</a></li>
                <li><a href="{{url('get-bills')}}">Manage</a></li>
            </ul>
        </li>
        @endif

        @if(session()->has('staff'))
            @php(  $user = session()->get('staff'))
            <li><a href="{{url('logout-user/'.$user->role)}}"><i class="ti-close"></i> Logout</a></li>
        @endif

    </ul>


</div>
