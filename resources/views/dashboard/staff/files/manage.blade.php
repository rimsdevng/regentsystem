<?php use Carbon\Carbon; use Illuminate\Support\Facades\Input; ?>
@extends('dashboard.staff.layouts.app')
@section('content')
    <div class="content-wrap">
        <div class="main">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-8 p-r-0 title-margin-right">
                        <div class="page-header">
                            <div class="page-title">

                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                    <div class="col-lg-4 p-l-0 title-margin-left">
                        <div class="page-header">
                            <div class="page-title">
                                <ol class="breadcrumb text-right">
                                    <li><a href="#">Dashboard</a></li>
                                    <li class="active">Files</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                </div>

                @include('notification')

                <div id="main-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card alert">
                                <div class="card-header pr">
                                    <h4>Manage Files</h4>
                                    <div class="search-action">
                                        <form id="searchForm">

                                        @if(isset($type))
                                            <span class="badge badge-pill badge-success">Searching {{$type}} files</span>
                                        @endif

                                        <div class="search-type dib">

                                            <div class="col-md-4">
                                                <select onchange="document.getElementById('searchForm').submit()" class="form-control" name="type">
                                                    <option value="private">Private</option>
                                                    <option value="all">All</option>
                                                    <option value="staff">Staff</option>
                                                    <option value="students">Students</option>
                                                </select>
                                            </div>
                                            <div class="col-md-8">
                                                <input class="form-control input-rounded" name="term" value="{{Input::get('term')}}" placeholder="Search" type="text">
                                            </div>


                                        </div>
                                        </form>
                                    </div>

                                </div>
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table class="table student-data-table m-t-20">
                                            <thead>
                                            <tr>
                                                <th>S/N</th>
                                                <th>Name</th>
                                                <th>Type</th>
                                                <th>Created</th>
                                                <th>Uploaded By</th>
                                                <th></th>
                                            </tr>
                                            </thead>
                                            <tbody>

                                            @if(count($files)>0)
												<?php $count = 1; ?>
                                                @foreach($files as $file)
                                                    <tr>

                                                        <td>
                                                            # <?php echo $count; ?>
                                                        </td>
                                                        <td>
                                                            {{$file->name}}
                                                        </td>
                                                        <td>
                                                            @if($file->type == 'private')
                                                            <span class="badge badge-pill">
                                                            {{$file->type}}
                                                            </span>
                                                            @endif

                                                            @if($file->type == 'public')
                                                            <span class="badge badge-success">
                                                            {{$file->type}}
                                                            </span>
                                                            @endif

                                                            @if($file->type == 'staff')
                                                            <span class="badge badge-primary">
                                                            {{$file->type}}
                                                            </span>
                                                            @endif

                                                            @if($file->type == 'students')
                                                            <span class="badge badge-warning">
                                                            {{$file->type}}
                                                            </span>
                                                            @endif

                                                        </td>
                                                        <td>
                                                            {{$file->created_at->toDayDateTimeString()}}
                                                        </td>
                                                        <td>
                                                            {{$file->Staff->fname}} {{$file->Staff->sname}}
                                                        </td>
                                                        <td>
                                                            <span><a class="btn btn-success" href="{{$file->url}}"><i class="ti-download color-white"></i> Download </a> </span>
                                                        </td>
                                                    </tr>
													<?php $count ++; ?>
                                                @endforeach
                                            @else

                                                <tr>
                                                    <td colspan="5" style="color: silver; text-align: center; margin-top: 30px;"> There are no files </td>
                                                </tr>
                                            @endif




                                            </tbody>
                                        </table>
                                        <div align="center">
                                            {{$files->links()}}
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /# column -->

                    </div>
                    <!-- /# row -->

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="footer">
                                <p>This dashboard was generated on <span id="date-time"></span> <a href="#" class="page-refresh">Refresh Dashboard</a></p>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- /# main content -->
            </div>
            <!-- /# container-fluid -->
        </div>
        <!-- /# main -->
    </div>


@endsection







