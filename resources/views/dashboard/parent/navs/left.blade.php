<div class="nano-content">
    <ul>

        <li>
            <img class="img logoImage" src="{{url('admin/assets/images/logo.png')}}"><br>
        </li>

        <li class="label">Home</li>
        <li><a href="{{url('/parent/dashboard')}}"><i class="ti-view-list-alt"></i> DashBoard</a></li>



        <li class="label">NEWS & UPDATES</li>
        <li><a href="{{url('/parent/news')}}"><i class="ti-view-list-alt"></i> News</a></li>
        <li><a href="{{url('/parent/children')}}"><i class="ti-view-list-alt"></i> Children </a></li>


        <li class="label">COMMUNICATIONS</li>
        <li><a class="sidebar-sub-toggle"><i class="ti-pencil-alt"></i> Complaints <span class="sidebar-collapse-icon ti-angle-down"></span></a>
            <ul>
                <li><a href="{{url('/parent/complaint/add')}}">Add</a></li>
                <li><a href="{{url('/parent/complaints')}}">View</a></li>
            </ul>
        </li>



        @if(session()->has('parent'))
            @php(  $user = session()->get('parent'))
            <li><a href="{{url('logout-user/'.$user->role)}}"><i class="ti-close"></i> Logout</a></li>
        @endif


    </ul>
</div>
