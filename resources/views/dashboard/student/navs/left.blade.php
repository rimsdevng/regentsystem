
<div class="nano-content">
    <ul>

        <li>
            <img class="img logoImage" src="{{url('admin/assets/images/logo.png')}}"><br>
        </li>


        <li class="label">Home</li>
        <li><a href="{{url('student/dashboard')}}"><i class="ti-view-list-alt"></i> Dashboard</a></li>


        <li class="label">Project</li>

        <li><a href="{{url('add-project')}}"><i class="ti-view-list-alt"></i>Add synopsis</a></li>
        <li><a href="{{url('student/subjects')}}"><i class="ti-view-list-alt"></i>Manage</a></li>

        <li class="label">Project Chapters</li>

        {{--  <li><a href="{{url('student/tests')}}"><i class="ti-view-list-alt"></i> My Synopsis</a></li>  --}}
        <li><a href="{{url('student/assignments ')}}" title=" Chapters and open day forms are uploaded here"><i class="ti-view-list-alt"></i> Manage</a></li>

        {{--  <li class="label">Assignments</li>
        <li><a href="{{url('student/assignments')}}"><i class="ti-view-list-alt"></i> My OpenDay</a></li>  --}}



        @if(session()->has('student'))
            @php(  $user = session()->get('student'))
            <li><a href="{{url('logout-user/'.$user->role)}}"><i class="ti-close"></i> Logout</a></li>
        @endif


    </ul>
</div>