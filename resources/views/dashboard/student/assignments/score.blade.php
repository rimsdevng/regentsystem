<?php use Carbon\Carbon; ?>
@extends('dashboard.student.layouts.app')
@section('content')


    <div class="content-wrap">
        <div class="main">
            @include('notification')
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-8 p-r-0 title-margin-right">
                        <div class="page-header">
                            <div class="page-title">
                                <h1>Dashboard</h1>
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                    <div class="col-lg-4 p-l-0 title-margin-left">
                        <div class="page-header">
                            <div class="page-title">
                                <ol class="breadcrumb text-right">
                                    <li><a href="#">Dashboard</a></li>
                                    <li class="active">Result</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                </div>
                <!-- /# row -->



               
                <!-- /# row -->



                <div id="main-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card alert">
                                <div class="card-header">
                                    <h4>Projectn Chapter Score</h4>

                                </div>
                                <div class="card-body">
                                    <div class="user-profile m-t-15">
                                        <div class="row">
                                           
                                            <div class="col-lg-8">
                                                <div class="user-profile-name dib">{{$assignment->name}} </div>
                                              
                                                <div class="custom-tab user-profile-tab">
                                                    <ul class="nav nav-tabs" role="tablist">
                                                        <li role="presentation" class="active"><a href="#1" aria-controls="1" role="tab" data-toggle="tab">About</a></li>
                                                    </ul>
                                                    <div class="tab-content">
                                                        <div role="tabpanel" class="tab-pane active" id="1">
                                                            <div class="contact-information">
                                                                
                                                                <div class="phone-content">
                                                                    <span class="contact-title"> Project Topic:</span>
                                                                    <span class="phone-number">   {{$assignment->name}}</span>
                                                                </div>

                                                                <div class="phone-content">
                                                                        <span class="contact-title"> Project Grade:</span>
    
                                                                        @if(isset($assignment->Submission[0]->grade))
                                                                            <span class="phone-number">   
                                                                                {{ $assignment->Submission[0]->grade }} 
                                                                                </span>
                                                                        @else
                                                                            <h5 style="color:silver; text-align: center"> Project has not been graded </h5>
                                                                        @endif
                                                                    </div>
                                                                
                                                                <div class="phone-content">
                                                                        <span class="contact-title"> Comment:</span>
                                                                        <span class="phone-number">   
                                                                            {{$assignment->Submission[0]->comment}}</span>
                                                                </div>
        
                                                                <div class="website-content">
                                                                    <span class="contact-title">Date Submitted:</span>
                                                                    <span class="contact-website">{{$assignment->Submission[0]->created_at->toDayDateTimeString()}}  </span>
                                                                </div>


                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /# column -->
                    </div>





                    <!-- /# row -->
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="footer">
                                <p>This dashboard was generated on <span id="date-time"></span> <a href="#" class="page-refresh">Refresh Dashboard</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection