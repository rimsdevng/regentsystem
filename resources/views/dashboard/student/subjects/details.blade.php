<?php use Carbon\Carbon; ?>
@extends('dashboard.student.layouts.app')
@section('content')


    <div class="content-wrap">
        <div class="main">
            @include('notification')
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-8 p-r-0 title-margin-right">
                        <div class="page-header">
                            <div class="page-title">
                                <h1>Dashboard</h1>
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                    <div class="col-lg-4 p-l-0 title-margin-left">
                        <div class="page-header">
                            <div class="page-title">
                                <ol class="breadcrumb text-right">
                                    <li><a href="#">Dashboard</a></li>
                                    <li class="active">Project Detail</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                </div>
                <!-- /# row -->



               
                <!-- /# row -->



                <div id="main-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card alert">
                                <div class="card-header">
                                    <h4>Project Details</h4>

                                </div>
                                <div class="card-body">
                                    <div class="user-profile m-t-15">
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <div class="user-photo m-b-30">
                                                    {{--                                                    <img class="img-responsive" src="{{$subject->image}}" alt="Photo Space" />--}}
                                                </div>
                                            </div>
                                            <div class="col-lg-8">
                                                <div class="user-profile-name dib">{{$subject->name}} </div>
                                                <div class="useful-icon dib pull-right">
                                                        {{--  subject/'.$subject->subid.'/edit  --}}
                                                    <span><a href="{{url('add-project')}}" class="btn btn-danger"><i class="ti-pencil-alt"></i></a> </span>
                                                    {{--<span><a href="#"><i class="ti-printer"></i></a></span>--}}
                                                    {{--<span><a href="#"><i class="ti-download"></i></a></span>--}}
                                                    <span><a href="#"><i class="ti-share"></i></a></span>
                                                </div>
                                                <div class="custom-tab user-profile-tab">
                                                    <ul class="nav nav-tabs" role="tablist">
                                                        <li role="presentation" class="active"><a href="#1" aria-controls="1" role="tab" data-toggle="tab">About</a></li>
                                                    </ul>
                                                    <div class="tab-content">
                                                        <div role="tabpanel" class="tab-pane active" id="1">
                                                            <div class="contact-information">
                                                                <div class="phone-content">
                                                                    <span class="contact-title"> Project Supervisor:</span>

                                                                    @if(isset($subject->Teacher->fname))
                                                                        <span class="phone-number">   {{$subject->Teacher->fname}}  {{$subject->Teacher->sname}} </span>
                                                                    @else
                                                                        <h5 style="color:silver; text-align: center"> There is no suprvisor assigned yet </h5>
                                                                    @endif
                                                                </div>
                                                                <div class="phone-content">
                                                                    <span class="contact-title"> Phone:</span>
                                                                    <span class="phone-number">{{$subject->Teacher->phone}} </span>
                                                                </div>
                                                                <div class="website-content">
                                                                    <span class="contact-title">Email:</span>
                                                                    <span class="contact-website">{{$subject->Teacher->email}}</span>
                                                                </div>


                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /# column -->
                    </div>




                    <!-- <div class="row">
                        <div class="col-lg-12">
                            <div class="card alert">
                                <div class="card-header pr">
                                    <h4>Manage Tests </h4>
                                </div>
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table class="table student-data-table m-t-20">
                                            <thead>
                                            <tr>
                                                <th>S/N</th>
                                                <th>Name</th>
                                                <th>Available Between</th>
                                                <th>Classes</th>
                                                <th>Max Attempts</th>
                                                <th>Attempt</th>
                                                <th>Score</th>
                                                <th></th>
                                            </tr>
                                            </thead>
                                            <tbody>

                                            @if(count($tests)>0)
								                <?php $count = 1; ?>
                                                @foreach($tests as $test)
                                                    <tr>

                                                        <td>
                                                            # <?php echo $count; ?>
                                                        </td>
                                                        <td>
                                                            {{$test->name}}
                                                        </td>
                                                        <td>
                                                            {{Carbon::createFromFormat("Y-m-d H:i:s",$test->startTime)->toDayDateTimeString()}} and
                                                            {{Carbon::createFromFormat("Y-m-d H:i:s",$test->endTime)->toDayDateTimeString()}}
                                                        </td>
                                                        <td>
                                                            @foreach($test->Classes as $item)
                                                                {{$item->name}},
                                                            @endforeach
                                                        </td>

                                                        <td>
                                                            {{$test->attempts}}
                                                        </td>

                                                    @if($modifiedTests[$count - 1]['attempt'] > 0)
                                                        <td>
                                                            {{$modifiedTests[$count - 1]['attempt']}}
                                                        </td>
                                                        <td>
                                                            {{$modifiedTests[$count - 1]['score']}} / {{$modifiedTests[$count - 1]['total']}}
                                                        </td>

                                                            @else
                                                            <td colspan="2"> Not attempted Yet</td>
                                                        @endif

                                                        <td>
                                                            @if($test->endTime > Carbon::now())
                                                                @if($modifiedTests[$count - 1]['attempt'] < $test->attempts )
                                                                <span><a class="btn btn-success " href="{{url('student/take-test/' . $test->testid)}}">Start Test</a> </span>
                                                                @endif
                                                            @endif
                                                        </td>
                                                    </tr>
									                <?php $count ++; ?>
                                                @endforeach
                                            @else

                                                <tr>
                                                    <td colspan="5" style="color: silver; text-align: center; margin-top: 30px;"> There are no tests </td>
                                                </tr>

                                            @endif




                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        

                    </div>  -->


                    <!-- /# row -->
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="footer">
                                <p>This dashboard was generated on <span id="date-time"></span> <a href="#" class="page-refresh">Refresh Dashboard</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection