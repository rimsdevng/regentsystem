<?php use Carbon\Carbon; ?>
@extends('dashboard.student.layouts.app')
@section('content')


    <div class="content-wrap">
        <div class="main">
            @include('notification')
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-8 p-r-0 title-margin-right">
                        <div class="page-header">
                            <div class="page-title">
                                <h1>Dashboard</h1>
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                    <div class="col-lg-4 p-l-0 title-margin-left">
                        <div class="page-header">
                            <div class="page-title">
                                <ol class="breadcrumb text-right">
                                    <li><a href="#">Dashboard</a></li>
                                    <li class="active">Project Synopsis</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                </div>
                <!-- /# row -->



                <div class="row">
                    <div class="col-md-8 col-md-offset-1">


                        <div class="col-md-3 pull-left">
                            {{--                            <a href="{{url('student/'.$student->sid.'/edit')}}" class="btn btn-info padding-overlay"> Update record </a>--}}
                        </div>

                        <div class="col-md-3">
                            {{--<a href="{{url('admit-student/'.$application->apid)}}" class="btn btn-success padding-overlay"> Admit Student</a>--}}
                        </div>

                        <div class="col-md-3">
                            {{--                            <a href="{{url('make-student/'.$application->apid)}}" class="btn btn-danger padding-overlay pull-right"> Admit </a>--}}
                        </div>

                    </div>

                </div>
                <!-- /# row -->



                <div id="main-content">
                   





                    <div class="row">
                        
                        <!-- /# column -->
                        <div class="col-lg-12">
                            <div class="card alert">
                                <div class="card-header">
                                    <h4>Fill in Synopsis Details</h4>
                                    <div class="card-header-right-icon">
                                        <ul>
                                            <li class="card-close" data-dismiss="alert"><i class="ti-close"></i></li>
                                            <li class="doc-link"><a href="#"><i class="ti-link"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="horizontal-form-elements">
                                        <form class="form-horizontal" enctype="multipart/form-data" method="POST" action="{{ url('post-project') }}">
                                                {{csrf_field()}}
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label">Project Topic:</label>
                                                        <div class="col-sm-10">
                                                            <input type="text" class="form-control" name="name" placeholder="Project Title">
                                                        </div>
                                                    </div>
                                                   
                                                    <div class="form-group">
                                                            <label class="col-sm-2 control-label">Project Topic:</label>
                                                            <div class="col-sm-10">
                                                                <input type="text" class="form-control" name="stid" placeholder="Student ID" maxlength="8">
                                                            </div>
                                                        </div>
                                                       

                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label">Upload File Instead (Optional)</label>
                                                        <div class="col-sm-10">
                                                            <input class="form-control" type="file" name="file" placeholder="Upload Synopsis if available">
                                                        </div>
                                                    </div>
                                                    {{--  <div class="form-group">
                                                        <label class="col-sm-2 control-label">Text Area</label>
                                                        <div class="col-sm-10">
                                                            <textarea class="form-control" rows="3" placeholder="Text input"></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label">Readonly</label>
                                                        <div class="col-sm-10">
                                                            <input class="form-control" type="text" value="Readonly value" readonly="">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label">Disabled</label>
                                                        <div class="col-sm-10">
                                                            <input class="form-control" type="text" value="Disabled value" disabled="">
                                                        </div>
                                                    </div>  --}}
                                                </div>
                                                <!-- /# column -->
                                                <div class="col-lg-6">
                                                    {{--  <div class="form-group">
                                                        <label class="col-sm-2 control-label">Static Control</label>
                                                        <div class="col-sm-10">
                                                            <p class="form-control-static">email@example.com</p>
                                                        </div>
                                                    </div>  --}}
                                                    {{--  <div class="form-group">
                                                        <label class="col-sm-2 control-label">Helping text</label>
                                                        <div class="col-sm-10">
                                                            <input class="form-control" type="text" placeholder="Helping text">
                                                            <span class="help-block">
																<small>A block of help text that breaks onto a new line and may extend beyond one line.</small>
															</span>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label">Input Select</label>
                                                        <div class="col-sm-10">
                                                            <select class="form-control">
																<option>1</option>
																<option>2</option>
																<option>3</option>
																<option>4</option>
																<option>5</option>
															</select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label">Multiple Select</label>
                                                        <div class="col-sm-10">
                                                            <select multiple="" class="form-control">
																<option>1</option>
																<option>2</option>
																<option>3</option>
																<option>4</option>
																<option>5</option>
															</select>
                                                        </div>
                                                    </div>  --}}

                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label">Description:</label>
                                                        <div class="col-sm-10">
                                                            <textarea class="form-control" rows="20" name="description" placeholder="Project Description"></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- /# column -->
                                                <button class="btn btn-info pull-right" type="submit">Submit Synopsis</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!-- /# card -->
                        </div>
                        <!-- /# column -->
                    </div>


                </div>
            </div>
        </div>
    </div>



@endsection