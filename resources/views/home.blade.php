@extends('layouts.admin')

@section('content')



    <div class="content-wrap">
        <div class="main">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-8 p-r-0 title-margin-right">
                        <div class="page-header">
                            <div class="page-title">
                                <h1>Hello, <span>{{auth()->user()->name}}</span></h1>
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                    <div class="col-lg-4 p-l-0 title-margin-left">
                        <div class="page-header">
                            <div class="page-title">
                                <ol class="breadcrumb text-right">
                                    <li><a href="#">Dashboard</a></li>
                                    <li class="active">Home</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                </div>
                <!-- /# row -->
                <div id="main-content">
                   
                   
                    <div class="row">
                        <div class="col-md-3">
                            <div class="card">
                                <div class="media">
                                    <div class="media-body media-text-left">
                                        <h4>{{ count($applications) }}</h4>
                                        <p class="color-white">Synopsis Submitted</p>
                                    </div>
                                    <div class="media-right meida media-middle">
                                        <div class="sparkline-unix">
                                            <div id="sparklinedash17"><canvas width="130" height="30" style="display: inline-block; width: 130px; height: 30px; vertical-align: top;"></canvas></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="card">
                                <div class="media">
                                    <div class="media-body media-text-left">
                                        <h4>{{ count($staff) }}</h4>
                                        <p class="color-white">Projects Supervisors/Staff</p>
                                    </div>
                                    <div class="media-right meida media-middle">
                                        <div class="sparkline-unix">
                                            <div id="sparklinedash18"><canvas width="130" height="30" style="display: inline-block; width: 130px; height: 30px; vertical-align: top;"></canvas></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="card">
                                <div class="media">
                                    <div class="media-body media-text-left">
                                        <h4>{{ count($students) }}</h4>
                                        <p class="color-white">Final Year Students</p>
                                    </div>
                                    <div class="media-right meida media-middle">
                                        <div class="sparkline-unix">
                                            <div id="sparklinedash19"><canvas width="130" height="30" style="display: inline-block; width: 130px; height: 30px; vertical-align: top;"></canvas></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="card">
                                <div class="media">
                                    <div class="media-body media-text-left">
                                        <h4>{{ count($subjects) }}</h4>
                                        <p class="color-white">Projects</p>
                                    </div>
                                    <div class="media-right meida media-middle">
                                        <div class="sparkline-unix">
                                            <div id="sparklinedash24"><canvas width="130" height="30" style="display: inline-block; width: 130px; height: 30px; vertical-align: top;"></canvas></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                   
                   
                    {{--  the oldupdate  --}}
                  




                    {{--  latest contents  --}}
                    <div class="row">
                        <div class="col-lg-9">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Recent Activities</h4>
                                    <div class="card-header-right-icon">
                                        <ul>
                                            <li class="card-close" data-dismiss="alert"><i class="ti-close"></i></li>
                                            <li class="card-option drop-menu"><i class="ti-settings" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" role="link"></i>
                                                <ul class="card-option-dropdown dropdown-menu">
                                                    <li><a href="#"><i class="ti-loop"></i> Update data</a></li>
                                                    <li><a href="#"><i class="ti-menu-alt"></i> Detail log</a></li>
                                                    <li><a href="#"><i class="ti-pulse"></i> Statistics</a></li>
                                                    <li><a href="#"><i class="ti-power-off"></i> Clear ist</a></li>
                                                </ul>
                                            </li>
                                            
                                        </ul>
                                    </div>
                                </div>
                                <div class="bg-white">
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table class="table">
                                                <thead>
                                                <tr>
                                                    <th>Student Name</th>
                                                    <th>Department</th>
                                                    <th>Topic</th>
                                                    <th>Date</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <div class="round-img">
                                                                <a href="#">J Rimamchirika</a>
                                                            </div>
                                                        </td>
                                                        <td>Information System Science</td>
                                                        <td>
                                                            <span>Car Servicing System</span>
                                                        </td>
                                                        <td class="color-primary">Jun 04 2019 </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="card">
                                <div class="card-header m-b-20">
                                    <h4>Latest Students Added</h4>
                                </div>
                                <ul class="list-group">
                                    <li class="list-group-item">
                                        <div class="media">
                                            <div class="media-left media-middle round-img">
                                                <img src="assets/images/avatar/1.jpg" alt="">
                                            </div>
                                            <div class="media-body">
                                                <h5>Jackson Matthew</h5>
                                                <p>Computer Science</p>
                                            </div>
                                        </div>
                                    </li>
                                   
                                </ul>
                            </div>
                        </div>
                    </div>
                    {{--  end of latest contents  --}}





                        <!-- /# row -->
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="footer">
                                <p>This dashboard was generated on <span id="date-time"></span> <a href="#" class="page-refresh">Refresh Dashboard</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection