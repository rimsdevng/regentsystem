@extends('layouts.admin')
@section('content')


    <div class="content-wrap">
        <div class="main">
            @include('notification')
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-8 p-r-0 title-margin-right">
                        <div class="page-header">
                            <div class="page-title">
                                <h1>{{$subject->name}}</h1>
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                    <div class="col-lg-4 p-l-0 title-margin-left">
                        <div class="page-header">
                            <div class="page-title">
                                <ol class="breadcrumb text-right">
                                    <li><a href="#">Dashboard</a></li>
                                    <li class="active">Project Details</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                </div>
                <!-- /# row -->



                <div class="row">
                    <div class="col-md-8 col-md-offset-1">


                        <div class="col-md-3 pull-left">
                            {{--                            <a href="{{url('student/'.$student->sid.'/edit')}}" class="btn btn-info padding-overlay"> Update record </a>--}}
                        </div>

                        <div class="col-md-3">
                            {{--<a href="{{url('admit-student/'.$application->apid)}}" class="btn btn-success padding-overlay"> Admit Student</a>--}}
                        </div>

                        <div class="col-md-3">
                            {{--                            <a href="{{url('make-student/'.$application->apid)}}" class="btn btn-danger padding-overlay pull-right"> Admit </a>--}}
                        </div>

                    </div>

                </div>
                <!-- /# row -->



                <div id="main-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card alert">
                                <div class="card-header">
                                    <h4>Project Details</h4>

                                </div>
                                <div class="card-body">
                                    <div class="user-profile m-t-15">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="user-profile-name dib">Supervisor </div>

                                                <div class="contact-information col-12">
                                                    <div class="col-md-6">

                                                        <div class="phone-content">
                                                            <span class="contact-title"> Name:</span>
                                                            <span class="phone-number">   {{$subject->Teacher->fname}}  {{$subject->Teacher->sname}}

							                                    <?php

							                                    if(!empty($subject->cids)){
								                                    $cids = json_decode($subject->cids);

								                                    $assignedClasses = "(";
								                                    $countCids = count($cids);

								                                    $count = 1;
								                                    foreach ($cids as $cid){
									                                    $assignedClasses .= \App\classes::find($cid)->name ;

									                                    if($countCids != $count) $assignedClasses .= ",";

									                                    $count++;
								                                    }

								                                    $assignedClasses .= ")";

								                                    echo $assignedClasses;
							                                    }

							                                    ?>
                                                                    </span>

                                                        </div>

                                                        <div class="phone-content">
                                                            <span class="contact-title"> Phone:</span>
                                                            <span class="phone-number">{{$subject->Teacher->phone}} </span>
                                                        </div>
                                                        <div class="website-content">
                                                            <span class="contact-title">Email:</span>
                                                            <span class="contact-website">{{$subject->Teacher->email}}</span>
                                                        </div>

                                                    </div>


                                                    <!-- show extra subject teachers -->
                                                    @foreach($subject->Teachers as $teacher)
                                                        <div class="col-md-6">

                                                            <div class="phone-content">
                                                                <span class="contact-title"> Name:</span>
                                                                <span class="phone-number">   {{$teacher->fname}}  {{$teacher->sname}}

								                                    <?php


								                                    $st = \App\subjectTeacher::where('stid',$teacher->stid)->where('subid',$subject->subid)->first();
								                                    if(!empty($st->cids)){
									                                    $cids = json_decode($st->cids);

									                                    $assignedClasses = "(";
									                                    $countCids = count($cids);

									                                    $count = 1;
									                                    foreach ($cids as $cid){
										                                    $assignedClasses .= \App\classes::find($cid)->name ;

										                                    if($countCids != $count) $assignedClasses .= ",";

										                                    $count++;
									                                    }

									                                    $assignedClasses .= ")";

									                                    echo $assignedClasses;
								                                    }

								                                    ?>
                                                                        </span>
                                                            </div>

                                                            <div class="phone-content">
                                                                <span class="contact-title"> Phone:</span>
                                                                <span class="phone-number">{{$teacher->phone}} </span>
                                                            </div>
                                                            <div class="website-content">
                                                                <span class="contact-title">Email:</span>
                                                                <span class="contact-website">{{$teacher->email}}</span>
                                                            </div>



                                                        </div>
                                                    @endforeach

                                                </div>


                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /# column -->
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="card alert" align="center">

                                <form class="form-inline" method="post" action="{{url('change-subject-teacher')}}">
                                    @csrf
                                    <input type="hidden" name="subid" value="{{$subject->subid}}" >
                                    <div class="form-group">
                                        <label >Change Supervisor</label>
                                        <select class="form-control" name="stid" style="width: 200px;">
                                            @foreach($teachers as $teacher)
                                                <option value="{{$teacher->stid}}">{{$teacher->fname}} {{$teacher->sname}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <button class="btn btn-success">Change</button>
                                </form>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="card alert" align="center">

                                <form class="form-inline" method="post" action="{{url('remove-subject-teacher')}}">
                                    @csrf
                                    <input type="hidden" name="subid" value="{{$subject->subid}}" >
                                    <div class="form-group">
                                        <label >Remove Supervisor</label>
                                        <select class="form-control" required name="stid" style="width: 200px;">
                                            @foreach($subject->Teachers as $teacher)
                                                <option value="{{$teacher->stid}}">{{$teacher->fname}} {{$teacher->sname}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <button class="btn btn-danger">Remove</button>
                                </form>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="card alert" align="center">

                                <form class="form" method="post" action="{{url('add-subject-teacher')}}">
                                    @csrf
                                    <input type="hidden" name="subid" value="{{$subject->subid}}" >

                                    <div class="form-group">
                                        <label >Add Supervisor</label><br>

                                        <select class="form-control col-sm-6" name="stid" >
                                            @foreach($teachers as $teacher)
                                                <option value="{{$teacher->stid}}">{{$teacher->fname}} {{$teacher->sname}}</option>
                                            @endforeach
                                        </select>

                                        {{--  multiple  --}}
                                        <select name="cids[]"  class="col-sm-12"  >
                                            <option disabled >Pick a Department</option>
                                            @foreach($classes as $class)
                                                <option value="{{$class->cid}}">{{$class->name}}</option>
                                            @endforeach
                                        </select>

                                    </div>

                                    <button class="btn btn-success">Add</button>
                                </form>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="card alert" align="center">

                            <form method="post" action="{{url('add-subject-student')}}">
                                @csrf
                                <input type="hidden" name="subid" value="{{$subject->subid}}">


                                <div class="form-group">
                                    <label >Add Student</label>
                                    <select class="form-control" name="sid" style="width: 200px;">
                                        @foreach($students as $student)
                                            <option value="{{$student->sid}}">{{$student->fname}} {{$student->sname}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <button class="btn btn-success">Add</button>

                            </form>
                            </div>
                        </div>
                    </div>



                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card alert">
                                <div class="card-header">
                                    <h4>Students </h4>

                                </div>
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table class="table student-data-table m-t-20">
                                            <thead>
                                            <tr>
                                                <th>S/n</th>
                                                <th>Names</th>
                                                <th>Gender</th>
                                                <th>ID No</th>
                                                <th>Created</th>
                                                <th></th>

                                            </tr>
                                            </thead>
                                            <tbody>

                                            @if(count($subject->Students)>0)

				                                <?php $count = 1; ?>

                                                @foreach($subject->Students as $student)
                                                    <tr>

                                                        <td>
                                                            #<?php echo $count;?>
                                                        </td>
                                                        <td>
                                                            {{$student->fname}} {{$student->sname}}
                                                        </td>
                                                        <td>
                                                            {{$student->gender}}
                                                        </td>
                                                        <td>
                                                            {{$student->studentid}}
                                                        </td>
                                                        <td>
                                                            {{$student->created_at}}
                                                        </td>


                                                        <td class="actions">
                                                            <span><a href="{{url('student/'.$student->sid.'/detail')}}"><i class="ti-eye color-default"></i></a> </span>
                                                            {{--<span><a href="#"><i class="ti-pencil-alt color-success"></i></a></span>--}}
                                                            {{--<span><a href="#"><i class="ti-trash color-danger"></i> </a></span>--}}
                                                        </td>
                                                    </tr>
					                                <?php $count ++; ?>
                                                @endforeach
                                            @else

                                                <tr>
                                                    <td colspan="6" style="color: silver; text-align: center; margin-top: 30px;"> There are no Students yet </td>
                                                </tr>

                                            @endif




                                            </tbody>
                                        </table>

                                        <div class="pull-right">
                                            {{--{{$applications->links()}}--}}

                                        </div>

                                    </div>

                                </div>
                            </div>
                        </div>
                        <!-- /# column -->
                    </div>




                    <!-- /# row -->
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="footer">
                                <p>This dashboard was generated on <span id="date-time"></span> <a href="#" class="page-refresh">Refresh Dashboard</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection