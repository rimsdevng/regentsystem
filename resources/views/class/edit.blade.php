@extends('layouts.admin')
@section('content')
    <div class="content-wrap">
        <div class="main">
            {{--@include('notification')--}}
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-8 p-r-0 title-margin-right">
                        <div class="page-header">
                            <div class="page-title">
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                    <div class="col-lg-4 p-l-0 title-margin-left">
                        <div class="page-header">
                            <div class="page-title">
                                <ol class="breadcrumb text-right">
                                    <li><a href="#">Dashboard</a></li>
                                    <li class="active">Department</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                </div>
                <!-- /# row -->
                @include('notification')
                <div class="main-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card alert">
                                <div class="card-header">
                                    <h4>Edit Class</h4>
                                </div>
                                <div class="card-body">
                                    <div class="menu-upload-form">
                                        <form class="form-horizontal" method="post" action="{{url('class/' . $class->cid . '/edit')}}">
                                            {{csrf_field()}}
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Name</label>
                                                <div class="col-sm-10">
                                                    <input type="text" class="form-control" name="name" value="{{$class->name}}" placeholder="Enter a Class Name" required>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Departmental Head</label>
                                                <select name="stid"  class="col-sm-10" >
                                                    <option  disabled selected >Pick a HOD</option>
                                                @foreach($staffs as $staff)
                                                        <option value="{{$staff->stid}}"
                                                                @if($staff->stid == $class->stid)
                                                                        selected
                                                                @endif
                                                                class="form-control">{{$staff->fname}} {{$staff->sname}}</option>
                                                @endforeach

                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-offset-2 col-sm-10">
                                                    <button type="submit" class="btn btn-lg btn-primary">Send</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!-- /# card -->
                        </div>
                        <!-- /# column -->
                    </div>
                    <!-- /# row -->
                </div>
                <!-- /# main content -->
            </div>
            <!-- /# container-fluid -->
        </div>
        <!-- /# main -->
    </div>





@endsection