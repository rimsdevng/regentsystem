<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});


Route::get('rename','PublicController@rename');

Auth::routes();

Route::get('/home', 'AdminController@index')->name('home');


Route::get('notification','HomeController@getNotification');
Route::post('post-notification','HomeController@postNotification');
Route::get('view-notifications','HomeController@viewNotifications');

Route::get('notification/{nid}/delete','HomeController@delete');
Route::get('notification/{nid}/detail','HomeController@detail');


Route::get('create-application','AdmissionController@addApplication');
Route::get('upload-application','AdmissionController@uploadApplication');

Route::post('post-create-application','AdmissionController@postApplication');
Route::post('upload-applications','AdmissionController@postUploadApplications');


Route::get('manage-applications','AdmissionController@getApplications');
Route::get('manage-applications/detail/{apid}','AdmissionController@applicationDetail');
Route::get('application/{apid}/edit','AdmissionController@editApplication');
Route::get('make-student/{apid}','AdmissionController@makeStudent');

Route::get('applications/admit/all','AdmissionController@admitAll');
Route::get('application/{apid}/delete','AdmissionController@deleteApplication');

Route::post('application/{apid}/edit','AdmissionController@postEditApplication');




//ADMIN ROLE

Route::get('logout','HomeController@logout');

Route::get('enroll-all-students','AdminController@enrollAllStudents');



Route::get('tests','AdminController@tests');
Route::get('admin/test/{testid}','AdminController@testDetails');
Route::get('admin/add-test','AdminController@AddTest');
Route::post('admin/add-test','AdminController@postAddTest');


Route::get('admin/result/comment/add','AdminController@addResultComment');
Route::get('admin/result/comments','AdminController@resultComments');
Route::get('admin/result/comment/{rcid}','AdminController@resultCommentDetails');
Route::get('admin/result/comment/{rcid}/delete','AdminController@deleteResultComment');

Route::post('admin/result/comment/add','AdminController@postAddResultComment');

Route::get('student-email-list','AdminController@studentEmailList');
Route::get('parent-email-list','AdminController@parentEmailList');


//admin result generation related
Route::get('admin/setup-midterm-result/{cid}','StaffController@setupMidtermResult');
Route::post('admin/setup-midterm-result','StaffController@postSetupMidtermResult');

Route::get('admin/verify-midterm-result','StaffController@verifyMidtermResult');
Route::get('admin/verify-midterm-result/{sid}','StaffController@verifyMidtermResultDetails');

Route::get('admin/submit-midterm-result','StaffController@submitMidtermResult');
Route::get('admin/generate-midterm-result','StaffController@generateMidtermResult');

Route::get('admin/generate-endofterm-result','StaffController@generateEndOfTermResult');



//students
Route::get('students','AdminController@students');
Route::get('student/{sid}/detail','AdminController@studentDetail');
Route::get('student/{sid}/edit','AdminController@editStudent');
Route::get('student/{sid}/delete','AdminController@deleteStudent');

Route::post('student/{sid}/edit','AdminController@postEditStudent');


//parents
Route::get('parents','AdminController@parents');
Route::get('parent/{pid}/detail','AdminController@parentDetails');


Route::get('parent/{pid}/edit','AdminController@editParent');

Route::post('parent/{pid}/edit','AdminController@postEditParent');
Route::get('parent/{pid}/delete', 'AdminController@deleteParent');





//academics
Route::get('add-focus','ClassController@focus');
Route::get('focus','ClassController@getFocus');
Route::get('focus/{fid}','ClassController@focusDetails');
Route::get('remove-focus-subject/{fid}/{subid}','ClassController@removeFocusSubject');

Route::post('add-focus-subject','ClassController@postAddFocusSubject');
Route::post('post-focus','ClassController@postFocus');



//subjects
Route::get('add-subjects','AdminController@addSubject');
// student aspect
Route::get('add-project','StudentController@addProject');

Route::get('subjects','AdminController@subjects');
Route::get('upload-subjects','AdminController@uploadSubjects');
Route::get('assign-project/{subid}','AdminController@getAssignProjectToSupervisor');
Route::post('post-assign-project/{subid}','AdminController@postAssignProjectToSupervisor');

Route::get('subject/{subid}/detail','AdminController@subjectDetails');
Route::get('remove-subject-student','AdminController@removeSubjectStudent');
Route::get('subject/student/remove/{subid}/{sid}','AdminController@removeSubjectStudent2');

Route::post('post-subject','AdminController@postAddSubject');
// student aspect
Route::post('post-project','StudentController@postAddProject');

Route::post('upload-subjects','AdminController@uploadSubjects');
Route::post('change-subject-teacher','AdminController@changeSubjectTeacher');
Route::post('add-subject-teacher','AdminController@addSubjectTeacher');
Route::post('remove-subject-teacher','AdminController@removeSubjectTeacher');
Route::post('add-subject-student','AdminController@postAddSubjectStudent');
Route::post('assign-student-subject','AdminController@postAssignStudentSubject');






Route::get('add-class','ClassController@addClass');
Route::get('classes','ClassController@classes');
Route::get('class/{cid}/edit','ClassController@editClass');
Route::post('post-class','ClassController@postClass');

Route::post('class/{cid}/edit','ClassController@postEditClass');

Route::get('add-staff','AdminController@addStaff');
Route::post('add-staff','AdminController@postAddStaff');
Route::get('manage-staff','AdminController@manageStaff');
Route::get('staff/upload','AdminController@uploadStaff');
Route::get('staff/{stid}/detail','AdminController@staffDetail');
Route::get('staff/{stid}/edit','AdminController@editStaff');


Route::post('staff/upload','AdminController@postUploadStaff');
Route::post('staff/{stid}/edit','AdminController@postEditStaff');


//designations
Route::get('add-designation','AdminController@addDesignation');
Route::post('post-designation','AdminController@postDesignation');
Route::get('designations','AdminController@designations');

Route::get('designation/{did}/edit','AdminController@editDesignation');
Route::get('designation/{did}/delete','AdminController@deleteDesignation');

Route::post('designation/{did}/edit','AdminController@postEditDesignation');






// STUDENT ROLE

Route::get('student/subjects','StudentController@subjects');
Route::get('student/subject/{subid}','StudentController@subjectDetails');
Route::get('student/tests','StudentController@tests');
Route::get('student/take-test/{testid}','StudentController@takeTest');
Route::get('student/assignments','StudentController@assignments');
Route::get('student/assignment/{aid}','StudentController@assignmentDetails');

Route::get('student/end-of-term-result','StudentController@endOfTermResult');

Route::get('student/end-of-term-result-ss3','StudentController@endOfTermResultSS3');

Route::post('student/assignment/{aid}/submit','StudentController@submitAssignment');

Route::get('view-project-score/{aid}', 'StudentController@getProjectScore');



//Authentication for other users.



//Authentication for other parents.
Route::get('parent/login','AuthenticationController@loginParent');
Route::post('parent/login','AuthenticationController@postLoginParent');
Route::get('parent/dashboard','ParentController@dashboard');

//Authentication for other staff.
Route::get('staff/login','AuthenticationController@loginStaff');
Route::post('staff/login','AuthenticationController@postLoginStaff');
Route::get('staff/dashboard','StaffController@dashboard');

//Authentication for other students.
Route::get('student/login','AuthenticationController@loginStudent');
Route::post('student/login','AuthenticationController@postLoginStudent');
Route::get('student/dashboard','StudentController@dashboard');

Route::get('logout-user/{user}','AuthenticationController@logoutUser');



//forgot passwords
Route::get('staff/forgot-password','AuthenticationController@staffForgotPassword');
Route::post('staff/forgot-password','AuthenticationController@postStaffForgotPassword');

Route::get('student/forgot-password','AuthenticationController@studentForgotPassword');
Route::post('student/forgot-password','AuthenticationController@postStudentForgotPassword');

Route::get('parent/forgot-password','AuthenticationController@parentForgotPassword');
Route::post('parent/forgot-password','AuthenticationController@postParentForgotPassword');

Route::get('admin/forgot-password','AuthenticationController@adminForgotPassword');
Route::post('admin/forgot-password','AuthenticationController@postAdminForgotPassword');




Route::get('upload-result','StaffController@getResultUpload');
//Route::post('upload-result/{cid}','StaffController@uploadResult');
Route::post('post-upload-result','StaffController@uploadResults');



//sickBay
Route::get('sickbay/students','StaffController@getStudents');
Route::get('sickbay/student/{sid}/detail','StaffController@sickbayStudentDetail');
Route::post('post-sickbay-treatment/{sid}','StaffController@postSickbayVisit');
Route::get('sickbay-visit/{sbid}','StaffController@sickbayVisitDetail');




//STAFF ROLE
Route::get('staff/classes/','StaffController@staffClasses');
Route::get('staff/class/{cid}','StaffController@classDetails');
Route::get('staff/subjects','StaffController@staffSubjects');
Route::get('staff/subject/{subid}','StaffController@subjectDetails');


Route::get('staff/assignments','StaffController@assignments');
Route::get('staff/assignments/add','StaffController@addAssignment');
Route::get('staff/assignment/{aid}','StaffController@assignmentDetails');
Route::get('staff/assignment/{aid}/delete','StaffController@deleteAssignment');
Route::get('staff/assignment/{aid}/submissions','StaffController@assignmentSubmissions');

Route::get('staff/assignment/{asid}/grade','StaffController@getGradeAssignmentSubmissions');
Route::post('staff/assignment/postGrade/{asid}','StaffController@postGradeAssignmentSubmissions');



Route::get('staff/news/add','StaffController@addNews');
Route::post('staff/news/add','StaffController@postAddNews');

Route::get('staff/news','StaffController@news');
Route::get('staff/news/{newsid}','StaffController@newsDetails');

Route::get('staff/news/{newsid}/delete','StaffController@deleteNews');

Route::post('staff/assignments/add','StaffController@postAddAssignment');


Route::get('staff/files/add','StaffController@addFile');
Route::get('staff/files','StaffController@files');

Route::post('staff/files/add','StaffController@postUploadFiles');

Route::get('staff/test/{testid}/questions/delete','StaffController@deleteAllQuestions');
Route::get('staff/test-results/{testid}','StaffController@testResults');

Route::get('staff/subject/update/{suid}/delete','StaffController@deleteSubjectUpdate');
Route::get('staff/subject/file/{sfid}/delete','StaffController@deleteSubjectFile');

Route::post('staff/subject/file/add','StaffController@postAddSubjectFile');
Route::post('staff/subject/update/add','StaffController@postAddSubjectUpdate');

//staff result related
Route::get('setup-midterm-result/{cid}','StaffController@setupMidtermResult');
Route::post('setup-midterm-result','StaffController@postSetupMidtermResult');

Route::get('verify-midterm-result','StaffController@verifyMidtermResult');
Route::get('verify-midterm-result/{sid}','StaffController@verifyMidtermResultDetails');

Route::get('submit-midterm-result','StaffController@submitMidtermResult');
Route::get('generate-midterm-result','StaffController@generateMidtermResult');

//Route::get('midterm-result-pdf/{sid}','StaffController@midtermResultPDf');
Route::get('midterm-result-pdf/{sid}','StaffController@generateMidtermResultPDF');

Route::get('generate-endofterm-result','StaffController@generateEndOfTermResult');

Route::get('print-class-midterm-result/{cid}','StaffController@printClassMidtermResult');


//end of term result


Route::get('setup-endofterm-result/{cid}','StaffController@setupEndOfTermResult');
Route::post('setup-endofterm-result','StaffController@postSetupEndOfTermResult');

Route::get('verify-endofterm-result','StaffController@verifyEndOfTermResult');
Route::get('verify-endofterm-result/{sid}','StaffController@verifyEndOfTermResultDetails');

Route::get('endofterm-result-pdf/{sid}','StaffController@generateEndOfTermResultPDF');

//ss3

Route::get('setup-endofterm-result-ss3/{cid}','StaffController@setupEndOfTermResultSS3');
Route::post('setup-endofterm-result-ss3','StaffController@postSetupEndOfTermResultSS3');

Route::get('verify-endofterm-result-ss3','StaffController@verifyEndOfTermResultSS3');
Route::get('verify-endofterm-result-ss3/{sid}','StaffController@verifyEndOfTermResultDetailsSS3');

Route::get('endofterm-result-pdf-ss3/{sid}','StaffController@generateEndOfTermResultPDFSS3');

//end end of term result


Route::get('staff/add-test','StaffController@addTest');
Route::get('staff/tests','StaffController@manageTests');
Route::get('test/{testid}','StaffController@testDetails');

Route::get('staff/question/{qid}','StaffController@questionDetails');
Route::get('staff/question/{qid}/delete','StaffController@deleteQuestion');

Route::get('staff/add-question/{testid}','StaffController@addQuestion');
Route::get('staff/upload-questions/{testid}','StaffController@uploadQuestions');

Route::post('staff/upload-questions','StaffController@postUploadQuestions');
Route::post('staff/edit-test','StaffController@postEditTest');

Route::get('add-result/','StaffController@addResult');
Route::get('add-result/subject','StaffController@result');
Route::get('add-result/subject/{subid}/{cid}','StaffController@addSubjectResult');
Route::get('results/{subid}/{cid}/{sid}/add','StaffController@addResultEntry');

Route::get('staff/score-sheet/{subid}/{cid}','StaffController@scoreSheet');
Route::get('staff/broad-sheet/{cid}','StaffController@generateBroadsheet');
Route::get('staff/broad-sheet-ss3/{cid}','StaffController@generateBroadsheetSS3');
Route::get('result','StaffController@viewResult');



//Route::get('results/add','StaffController@addResultEntry');

Route::post('add-subject-results','StaffController@postAddSubjectResults');
Route::post('add-result/subject','StaffController@result');
Route::post('post-result-entry','StaffController@postResultEntry');


Route::post('staff/add-question','StaffController@postAddQuestion');
Route::post('staff/add-test','StaffController@postAddTest');

Route::get('add-bill','StaffController@addBill');
Route::post('post-bill','StaffController@postBill');
Route::get('get-bills','StaffController@getBills');
Route::get('get-bills/{bid}/details','StaffController@getBillDetails');

Route::get('student-bill','StaffController@studentBill');
Route::post('post-student-bill','StaffController@postStudentBill');




//tests api

Route::post('add-attempt','WebApiController@addAttempt');
Route::post('test','WebApiController@test');
Route::post('add-test-result','WebApiController@addTestResult');




// GENERAL

Route::get('change-password','AuthenticationController@changePassword');

Route::get('forgot-password','AuthenticationController@forgotPassword');
Route::post('forgot-password','AuthenticationController@postForgotPassword');

Route::post('change-password','AuthenticationController@postChangePassword');

Route::get('reset-password','AuthenticationController@resetPassword');
Route::post('reset-password','AuthenticationController@postResetPassword');



//PARENT ROLE

Route::get('parent/children','ParentController@children');
Route::get('parent/child/{sid}','ParentController@childDetails');
Route::get('parent/child/result/{sid}','ParentController@childResult');
Route::get('parent/child/end-of-term-result/{sid}','ParentController@childEndOfTermResult');
Route::get('parent/child/end-of-term-result-ss3/{sid}','ParentController@childEndOfTermResultSS3');

Route::get('parent/news','ParentController@news');
Route::get('parent/news/{newsid}','ParentController@newsDetails');


Route::get('parent/complaint/add','ParentController@addComplaint');
Route::post('parent/complaint/add','ParentController@postAddComplaint');

Route::get('parent/complaints','ParentController@complaints');
Route::get('parent/complaint/{comid}','ParentController@complaint');
